/*
Author: Yakup Ipek
Jacobi Iteration Kernel for OpenCL 1.X
*/
#pragma OPENCL EXTENSION cl_khr_fp64 : enable

VALUE init_func(int x, int y) {
	return 40 * sin((VALUE)(16 * (2 * x - 1) * y));
}

__kernel void initialize_values(__global VALUE* f){

	int idx = get_global_id(0);
	int idy = get_global_id(1);

	f[N*idx+idy]=init_func(idx,idy);
}


__kernel void jacobi_iteration(__global VALUE* f, __global VALUE* u, __global VALUE* tmp, VALUE factor){

	int i = get_global_id(0);
	int j = get_global_id(1);
  
	if(i>0 && j>0 && i<N-1 && j<N-1){
		tmp[i*N+j]=(VALUE)1/4 * (u[(i-1)*N+j] + u[i*N+(j+1)] + u[i*N+(j-1)] + u[(i+1)*N+j] - factor * f[i*N+j]);
	}

}