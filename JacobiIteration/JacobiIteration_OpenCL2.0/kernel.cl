/*
Author: Yakup Ipek
Jacobi Iteration Kernel for OpenCL 2.0
*/

VALUE init_func(int x, int y) {
	return 40 * sin((VALUE)(16 * (2 * x - 1) * y));
}

__kernel void jacobi_iteration_steps(__global VALUE* f, __global VALUE* u, __global VALUE* tmp, VALUE factor){
	int i = get_global_id(0);
	int j = get_global_id(1);
	if(i>0 && j>0 && i<N-1 && j<N-1){
		tmp[i*N+j]=(VALUE)1/4 * (u[(i-1)*N+j] + u[i*N+(j+1)] + u[i*N+(j-1)] + u[(i+1)*N+j] - factor * f[i*N+j]);
	}
}

__kernel void jacobi_iteration(__global VALUE* f, __global VALUE* u, __global VALUE* tmp,__global VALUE* factor){

	int idx = get_global_id(0);
	int idy = get_global_id(1);

	f[N*idx+idy]=init_func(idx,idy);

	if(idx ==0 && idy ==0){
		clk_event_t evt[IT];
		int index=0;
		size_t globalworksize[2] = {N,N};
		ndrange_t ndrange = ndrange_2D(globalworksize);
		for(int i=0; i<IT; i++){
			if(i==0){
				void (^my_block) (void)= ^{jacobi_iteration_steps(f,u,tmp,*factor);};	
				enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL, ndrange,0,NULL,evt+index, my_block);
			}else if(i % 2 == 0){
				void (^my_block) (void)= ^{jacobi_iteration_steps(f,u,tmp,*factor);};	
				enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL, ndrange,1,evt+index,evt+(++index), my_block);	
			}else{
				void (^my_block) (void)= ^{jacobi_iteration_steps(f,tmp,u,*factor);};
				enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL, ndrange,1,evt+index,evt+(++index), my_block);
			}
			release_event(evt[index]);
		}
	}
}


