/*
Author: Yakup Ipek
Jacobi Iteration Kernel for OpenCL 2.0 Alternative Version Without Events
*/

__kernel void jacobi_iteration_steps(__global VALUE* f, __global VALUE* u, __global VALUE* tmp,__global VALUE* factor,__global int* counter);

VALUE init_func(int x, int y) {
	return 40 * sin((VALUE)(16 * (2 * x - 1) * y));
}

__kernel void copy_buffer(__global VALUE* f,__global VALUE* u, __global VALUE* tmp, __global VALUE* factor,__global int* counter){
	int i = get_global_id(0);
	int j = get_global_id(1); 
	u[i*N+j] = tmp[i*N+j];

	if(i == 0 && j == 0){
		counter[0]++;
		if(counter[0] < IT){
			size_t globalworksize[2] = {N,N};
			ndrange_t ndrange = ndrange_2D(globalworksize);
			void (^my_block) (void)= ^{jacobi_iteration_steps(f,u,tmp,factor, counter);};
			enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL, ndrange,my_block);
		}
	}
}

__kernel void jacobi_iteration_steps(__global VALUE* f, __global VALUE* u, __global VALUE* tmp,__global VALUE* factor,__global int* counter){
	int i = get_global_id(0);
	int j = get_global_id(1);

	if(i>0 && j>0 && i<N-1 && j<N-1){
		tmp[i*N+j]=(VALUE)1/4 * (u[(i-1)*N+j] + u[i*N+(j+1)] + u[i*N+(j-1)] + u[(i+1)*N+j] - factor[0] * f[i*N+j]);
	}

	if(i==0 && j == 0){
		size_t globalworksize[2] = {N,N};
		ndrange_t ndrange = ndrange_2D(globalworksize);
		void (^my_block) (void)= ^{copy_buffer(f,u,tmp,factor, counter);};	
		enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL, ndrange,my_block);
	}
}

__kernel void jacobi_iteration(__global VALUE* f, __global VALUE* u, __global VALUE* tmp,__global VALUE* factor, __global int* counter){

	int idx = get_global_id(0);
	int idy = get_global_id(1);

	f[N*idx+idy]=init_func(idx,idy);

	if(idx ==0 && idy ==0){
		counter[0]=0;
		size_t globalworksize[2] = {N,N};
		ndrange_t ndrange = ndrange_2D(globalworksize);
		void (^my_block) (void)= ^{jacobi_iteration_steps(f,u,tmp,factor,counter);};	
		enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL, ndrange, my_block);
	}
}


