/*
Author: Yakup Ipek
Jacobi Iteration in Parallel for OpenCL 2.0
*/
#include "matrixLib.h"
#include "myOpenCL_Lib.h"
#include "MyTime.h"

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

#ifndef N
#define N 5
#endif
#ifndef IT
#define IT 100
#endif

#define VALUE double

#define KERNEL_FILE_NAME "./kernel.cl"

VALUE init_func(int x, int y);
void sequential_computation(VALUE* u, VALUE* tmp, VALUE* f, VALUE factor);

int main(void) {

	cl_context context;
	cl_command_queue command_queue;
	cl_command_queue device_queue;
	//cl_device_id device_id = clInitWithDeviceQueue(&context, &command_queue, &device_queue);
	cl_device_id device_id = clInitWithDeviceQueueByParam(&context, &command_queue, &device_queue, CL_PLATFORM_ID, CL_DEVICE_ID);
	cl_program clProgram;
	cl_kernel clKernel;
	cl_int status;
	cl_event event;
	cl_ulong total_time = 0;
	MyTimeData time;

	VALUE *u_seq = (VALUE*)malloc(sizeof(VALUE)*N*N);
	VALUE *f_seq = (VALUE*)malloc(sizeof(VALUE)*N*N);
	VALUE *tmp_seq = (VALUE*)malloc(sizeof(VALUE)*N*N);
	VALUE factor = pow((VALUE)1 / N, 2);

	// init matrix
	memset(u_seq, 0, N*N*sizeof(VALUE));
	memset(tmp_seq, 0, N*N*sizeof(VALUE));

	/*---------------Initialize CL_DEVICE---------------*/
	char option[128];
	sprintf(option, "-DIT=%i -DN=%i -DVALUE=%s", IT, N, EXPAND_QUOTE(VALUE));
	myCLBuildProgram(context, &clProgram, device_id, KERNEL_FILE_NAME, option);
	clKernel = clCreateKernel(clProgram, "jacobi_iteration", &status);
	CLU_ERRCHECK(status, "Error creating kernel for initializing values");

	/*---------------Create Buffers---------------*/

	VALUE* u = (VALUE*)clSVMAlloc(context, CL_MEM_READ_WRITE, N * N * sizeof(VALUE), 0);
	VALUE* f = (VALUE*)clSVMAlloc(context, CL_MEM_READ_WRITE, N * N * sizeof(VALUE), 0);
	VALUE* tmp = (VALUE*)clSVMAlloc(context, CL_MEM_READ_WRITE, N * N * sizeof(VALUE), 0);
	VALUE* mem_factor = (VALUE*)clSVMAlloc(context, CL_MEM_READ_ONLY, sizeof(VALUE), 0);
	VALUE* counter = (VALUE*)clSVMAlloc(context, CL_MEM_READ_WRITE, sizeof(int), 0);

	start_timer(&time);
	memcpy(mem_factor, &factor, sizeof(VALUE));
	end_timer(&time);
	total_time += time.Result;

	/*---------------Set Arguments---------------*/

	clSetKernelArgSVMPointer(clKernel, 0, f);
	clSetKernelArgSVMPointer(clKernel, 1, u);
	clSetKernelArgSVMPointer(clKernel, 2, tmp);
	clSetKernelArgSVMPointer(clKernel, 3, mem_factor);
	clSetKernelArgSVMPointer(clKernel, 4, counter);


	/*--------------- Kernel---------------*/

	size_t  globalWorkSize[2] = { N,N };

	CLU_ERRCHECK(clEnqueueNDRangeKernel(command_queue, clKernel, 2, NULL, globalWorkSize, NULL, 0, NULL, &event), "Failed to enqueue 2D kernel");

	total_time += retrieveTotalExecutionTimeFromEvent(&event);

	/*-----------------Print Result---------------*/
	printMatrix(u, N*N, EXPAND_QUOTE(VALUE), N, "Parallel Computation Output");

	printf("Total time in nanoseconds = %llu\n\n", total_time);

	/*------------------Seqential Execution----------------*/
	sequential_computation(u_seq, tmp_seq, f_seq, factor);
	printMatrix(u_seq, N*N, EXPAND_QUOTE(VALUE), N, "Sequential Computation Output");


	/*free and release*/
	free(u_seq);
	free(f_seq);
	free(tmp_seq);
	clSVMFree(context, f);
	clSVMFree(context, u);
	clSVMFree(context, tmp);
	clSVMFree(context, mem_factor);
	status = clReleaseContext(context);
	status |= clReleaseCommandQueue(command_queue);
	status |= clReleaseProgram(clProgram);
	status |= clReleaseKernel(clKernel);
	CLU_ERRCHECK(status, "Error realeasing ocl data");
}

VALUE init_func(int x, int y) {
	return 40 * sin((VALUE)(16 * (2 * x - 1) * y));
}

void sequential_computation(VALUE* u, VALUE* tmp, VALUE* f, VALUE factor) {
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			f[N*i + j] = init_func(i, j);
		}
	}

	for (int it = 0; it<IT; it++) {
		// main Jacobi loop
		#ifdef OMP
		#pragma omp parallel for
		#endif
		for (int i = 1; i < N - 1; i++) {
			for (int j = 1; j < N - 1; j++) {
				tmp[N*i + j] = (VALUE)1 / 4 * (u[N*(i - 1) + j] + u[N*i + (j + 1)] + u[N*i + (j - 1)] + u[N*(i + 1) + j] - factor * f[N*i + j]);
			}
		}
		memcpy(u, tmp, N*N*sizeof(VALUE));
	}
}
