/*
Author: Yakup Ipek
Bitonic Sort with Chunks (utilizing Local Memory) for OpenCL 1.2
A merge map which is precomputed is used to pair chunks
*/


#include "myOpenCL_Lib.h"
#include "matrixLib.h"
#define SIZE 8192
#define LOCALWORKSIZE 512
#define TYPE int

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

/////////////////////////////////////////////////////////
// Program main
/////////////////////////////////////////////////////////

#define ISPOWEROFTWO(a)\
if(!((a != 0) && ((a & (~a + 1)) == a))) { \
	fprintf(stderr, "%s is not a power of 2\n",QUOTE(a)); \
	exit(-1); \
}

typedef struct{
	cl_context context;
	cl_command_queue cmd_queue;
	cl_kernel cl_my_kernel;
	cl_program cl_my_program;
	cl_device_id devices;
} _ocl_management_;

void create_merge_map(int* merge_map, int* count, int start, int end);
void merge_chunks(int* merge_map, int*count, int startpoint, int endpoint);
void merge(int* merge_map, int* count, int startpoint, int mid, int endpoint);
void print_merge_map(int* merge_map);
void merge_kernel(_ocl_management_* cl_data,TYPE* input_output, int* merge_map, cl_ulong* time);

int main(void) {

	//Verify correct inputs (SIZE->Number of elements to sort, LOCALWORKSIZE->Number of work items within workgroup)
	ISPOWEROFTWO(SIZE);
	ISPOWEROFTWO(LOCALWORKSIZE);
	
	if (LOCALWORKSIZE > SIZE) {
		fprintf(stderr,"LOCALWORKSIZE needs to be smaller than SIZE\n");
		return -1;
	}

	_ocl_management_ cl_data;
	size_t dataBytes;
	cl_int errcode;
	cl_event profiling_event;
	cl_ulong total_time = 0;
	cl_mem d_A;
	cl_int status;
	
	
	//Initialize cl device
	//cl_data.devices = clInit(&cl_data.context, &cl_data.cmd_queue);
	cl_data.devices = clInitByParam(&cl_data.context, &cl_data.cmd_queue,CL_PLATFORM_ID,CL_DEVICE_ID);

	//Load and build OpenCL kernel
	const char fileName[] = "bitonic_sort.cl";
	char option[128];
	sprintf(option, "-DTYPE=%s", EXPAND_QUOTE(TYPE));
	myCLBuildProgram(cl_data.context, &cl_data.cl_my_program, cl_data.devices, fileName, option);
	cl_data.cl_my_kernel = clCreateKernel(cl_data.cl_my_program, "bitonic", &errcode);
	CLU_ERRCHECK(errcode, "Error creating bitonic kernel");
	
	//Prepare problem-domain
	printf("Preparing data for sorting...\n");
	unsigned int mem_size_A = sizeof(TYPE)* SIZE;
	size_t size_merge_map = sizeof(int) * (SIZE/LOCALWORKSIZE) * ((SIZE/LOCALWORKSIZE)-1);
	TYPE* vector = (TYPE*)malloc(mem_size_A);
	TYPE* vector_sequential = (TYPE*)malloc(mem_size_A);
	int* merge_map = (int*)malloc(size_merge_map);
	int* count = (int*)calloc(SIZE/LOCALWORKSIZE, sizeof(int));
	create_merge_map(merge_map,count,0,SIZE/LOCALWORKSIZE);
	fillMatrixRandomly((void*)vector, SIZE, EXPAND_QUOTE(TYPE));
	memcpy(vector_sequential, vector, mem_size_A);

	//printMatrix(merge_map, SIZE / LOCALWORKSIZE * ((SIZE / LOCALWORKSIZE) - 1), "int", SIZE / LOCALWORKSIZE, "Test");
	//print_merge_map(merge_map);
	//printMatrix(vector, SIZE, EXPAND_QUOTE(TYPE), LOCALWORKSIZE, "Vector which needs to be sorted");
	
	printf("_________________Start Sorting_______________\n");
	//Start merging chunks
	merge_kernel(&cl_data,vector,merge_map,&total_time);
	printf("_________________Finished Sorting_______________\n");

	//printMatrix(vector, SIZE, EXPAND_QUOTE(TYPE), LOCALWORKSIZE, "Sorted");

	//Sequential Bubblesort in order to compare the results
	sortMatrix(vector_sequential, SIZE, EXPAND_QUOTE(TYPE));

	printf("\nTotal time in nanoseconds = %llu\n", total_time);
	verifyTwoMatrices(vector, vector_sequential, SIZE, EXPAND_QUOTE(TYPE));

	free(vector);
	free(vector_sequential);
	free(merge_map);
	free(count);
	errcode = clReleaseContext(cl_data.context);
	errcode |= clReleaseKernel(cl_data.cl_my_kernel);
	errcode |=  clReleaseProgram(cl_data.cl_my_program);
	errcode |= clReleaseCommandQueue(cl_data.cmd_queue);
	CLU_ERRCHECK(errcode, "Error releasing ocl objects");

	return EXIT_SUCCESS;
}

//Merge map creates n/2 times 2-tuples which can be sorted at one go (each tuples are distinct)
void create_merge_map(int* merge_map, int* count, int start, int end) {
	merge_chunks(merge_map, count,start,end);
}

void merge_chunks(int* merge_map, int*count , int startpoint, int endpoint) {
	
	if (endpoint - startpoint < 2) {
		return;
	}
	
	int mid = (startpoint + endpoint) / 2;
	
	merge_chunks(merge_map, count, startpoint,mid);
	merge_chunks(merge_map, count, mid, endpoint);
	merge(merge_map, count, startpoint,mid, endpoint);
}

void merge(int* merge_map,int* count, int startpoint,int mid, int endpoint) {
	int dif = endpoint - mid;
	int jump = SIZE/LOCALWORKSIZE;
	int width = endpoint - startpoint;
	int shift;
	if(width == 2){
		for (int i = startpoint; i < endpoint; i++) {
			if (i < mid) {
				merge_map[count[i]*jump+i] = i + dif;
				merge_map[count[i + dif]*jump+(i + dif)] = -1;
				count[i]++;
				count[i + dif]++;
			}
			else break;
		}
	}
	else {
		for (int i = startpoint; i < mid; i++) {
			for (int j = width / 2; j < width; j++) {
				if ((i + j) >= endpoint) {
					shift = (i + j) % endpoint;
					merge_map[count[i]*jump+i] = mid + shift;
					merge_map[count[mid+shift]*jump+(mid+shift)] = -1;
					count[i]++;
					count[mid + shift]++;
				}
				else {
					merge_map[count[i]*jump+i] = i+j;
					merge_map[count[i+j]*jump+(i+j)] = -1;
					count[i]++;
					count[i+j]++;
				}
			}
		}
	}
}

void print_merge_map(int* merge_map) {
	int end = (SIZE/LOCALWORKSIZE)-1;	//number of iterations
	int number_work_groups = SIZE / LOCALWORKSIZE;	//number of work groups to merge
	for (int i = 0; i < number_work_groups; i++) {
		for (int j = 0; j < end; j++) {
			if (merge_map[j*number_work_groups +i] == -1) {
				printf("\t %d.- \t", j*number_work_groups + i);
			}
			else {
				printf("\t %d.%d \t", j*number_work_groups + i, merge_map[j*number_work_groups + i]);
			}
		}
		printf("\n");
	}
}

//chunks are merged
void merge_kernel(_ocl_management_* cl_data, TYPE* input_output,int* merge_map, cl_ulong* time) {
	//printf("Start: %d\tEnd: %d\n",startpoint, endpoint);
	int number_work_groups = SIZE / LOCALWORKSIZE;
	size_t size = SIZE*sizeof(TYPE);
	cl_int errcode;
	cl_event profiling_event;
	cl_mem memory_vector = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, size, NULL, &errcode);
	CLU_ERRCHECK(errcode, "Failed creating Buffer");
	cl_mem memory_merge_map = clCreateBuffer(cl_data->context, CL_MEM_READ_ONLY, number_work_groups*sizeof(int), NULL, &errcode);
	CLU_ERRCHECK(errcode, "Failed creating Buffer");

	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->cmd_queue, memory_vector, CL_TRUE, 0, size, input_output, 0, NULL, &profiling_event), "Error writing buffer");
	*time+=retrieveTotalExecutionTimeFromEvent(&profiling_event);
	
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_my_kernel, 0, sizeof(cl_mem), (void *)&memory_vector), "Error while setting first argument");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_my_kernel, 1, sizeof(TYPE)*LOCALWORKSIZE * 2, NULL), "Error while setting second argument");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_my_kernel, 2, sizeof(cl_mem), (void *)&memory_merge_map), "Error while setting third argument");

	size_t  globalWorkSize[1] = { SIZE * 2 };
	size_t  localWorkSize[1] = { LOCALWORKSIZE * 2 };
	int end = (SIZE/LOCALWORKSIZE) - 1;
	
	//Times variable determines how many times the Merge map has to be applied to sort the vector
	int times = (int)log2(SIZE / LOCALWORKSIZE) == 1 ? (int)log2(SIZE / LOCALWORKSIZE) : (int)log2(SIZE / LOCALWORKSIZE) - 1;
	for (int j = 0; j < times; j++){
		for (int i = 0; i < end; i++) {

			CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->cmd_queue, memory_merge_map, CL_TRUE,0, sizeof(int)*number_work_groups, merge_map+i*number_work_groups, 0, NULL, &profiling_event), "Error writing buffer");
			*time += retrieveTotalExecutionTimeFromEvent(&profiling_event);
		
			CLU_ERRCHECK(clEnqueueNDRangeKernel(cl_data->cmd_queue, cl_data->cl_my_kernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, &profiling_event),"Error enqueueing kernel");
			*time += retrieveTotalExecutionTimeFromEvent(&profiling_event);

		}
	}
	
	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->cmd_queue, memory_vector, CL_TRUE, 0, size, input_output, 0, NULL, &profiling_event), "Error reading buffer");
	*time += retrieveTotalExecutionTimeFromEvent(&profiling_event);

	//Release device objects
	clReleaseMemObject(memory_merge_map);
	clReleaseMemObject(memory_vector);
}
