/*
Author: Yakup Ipek
Bitonic sort with local_memory kernel
A merge map is used to pair chunks
*/

__kernel void bitonic(__global TYPE* vector, __local TYPE* local_vector, __global int* merge_map){

	int size = get_local_size(0)/2;
	int log = (int)log2((float)get_local_size(0));
	int g_id = get_global_id(0);
	int l_id = get_local_id(0);
	int index = l_id % (get_local_size(0)/2);
	int d;
	bool up;
	int result = get_group_id(0)*size+index; 
	TYPE temp;
	//printf("%d.%d \n",get_group_id(0),merge_map[get_group_id(0)]);
	
	if(merge_map[get_group_id(0)]!=-1){
		//printf("%d\n",merge_map[get_group_id(0)]);
		
		if(l_id < get_local_size(0)/2){
			local_vector[l_id] = vector[get_group_id(0)*size+index];
			//printf("%d, %d, %d\n",l_id, index, result);	
		}else{
			local_vector[l_id] = vector[merge_map[get_group_id(0)]*size+index];
			//printf("%d\n",merge_map[get_group_id(0)]*size+index);
			//printf("%d, %d\n",l_id, result);	
		}
	}
	
	
	barrier(CLK_LOCAL_MEM_FENCE);

	for(int i = 0; i<log; i++){
		for(int j = 0; j<=i; j++){

			d = 1 << (i-j);
			up = ((l_id>>i) & 2) == 0;
		
			if((l_id & d)==0 && (local_vector[l_id]>local_vector[l_id|d])==up){
				TYPE temp = local_vector[l_id];
				local_vector[l_id] = local_vector[l_id|d];
				local_vector[l_id|d]=temp;
			}		

			barrier(CLK_LOCAL_MEM_FENCE);				
		}
	}
	if(merge_map[get_group_id(0)]!=-1){
		if(l_id < get_local_size(0)/2){
			vector[get_group_id(0)*size+index] = local_vector[l_id];
		}else{
			vector[merge_map[get_group_id(0)]*size+index] = local_vector[l_id];
		}
	}
}