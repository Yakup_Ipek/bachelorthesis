/*
Author: Yakup Ipek
Bitonic sort kernel
*/


__kernel void bitonic(__global TYPE* A, int p, int q){

	int id = get_global_id(0);
	int d = 1 << (p-q);

	bool up = ((id>>p) & 2) == 0;
	
	if((id & d)==0 && (A[id]>A[id|d])==up){
		TYPE temp = A[id];
		A[id] = A[id|d];
		A[id|d]=temp;
	}
	//printf("END i: %d\tj: %d\n",p,q);
}

__kernel void init(__global TYPE* A){

	if(get_global_id(0)==0){
		
		float logn = log2(get_global_size(0)*1.0);
		ndrange_t child_ndrange = ndrange_1D(get_global_size(0));

		
		clk_event_t evt[(int)KERNELAMOUNTOFEVENTS];
		int index=0;
		
		for(int i=0; i<logn; i++){
			for(int j=0; j<=i; j++){
				if(i==0){
					enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,child_ndrange,0, NULL, evt+index, ^{bitonic(A,i,j);});
					release_event(evt[index]);
				}
				else{ 	
					enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,child_ndrange,1, evt+index, evt+(++index), ^{bitonic(A,i,j);});
					release_event(evt[index]);
				}
			}
		}
	}
}