/*
Author: Yakup Ipek
Bitonic sort for OpenCL 2.0
*/

#include "myOpenCL_Lib.h"
#include "matrixLib.h"
#include "MyTime.h"

#define SIZE 8192

#define TYPE int

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

#define LOG (log2(SIZE))

#define KERNELAMOUNTOFEVENTS (calcAmountOfEvents())

#define ISPOWEROFTWO(a)\
if(!((a != 0) && ((a & (~a + 1)) == a))) { \
	fprintf(stderr, "The Size Variable needs to be a power of 2\n"); \
	exit(-1); \
}

/*Since kernel does not support variable length for arrays, we need to precompute the array size and pass this info with kompile options*/
int calcAmountOfEvents();

/////////////////////////////////////////////////////////
// Program main
/////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
	ISPOWEROFTWO(SIZE);
	
	//Allocate and initialize host memory for matrices A and B
	unsigned int mem_size_A = sizeof(TYPE)* SIZE;
	TYPE* h_A = (TYPE*)malloc(mem_size_A);
	TYPE* h_A_Seq = (TYPE*)malloc(mem_size_A);

	fillMatrixRandomly((void*)h_A, SIZE, EXPAND_QUOTE(TYPE));
	memcpy(h_A_Seq, h_A, mem_size_A);

	//Initialize OpenCL specific variables
	cl_program clProgram;
	cl_kernel clKernel;
	cl_kernel clKernel_init;
	size_t dataBytes;
	size_t kernelLength;
	cl_int errcode;
	cl_event profiling_event;
	cl_ulong total_time = 0;
	MyTimeData time;

	//--------------retrieving platforms-----------------------//	

	cl_int status;
	cl_device_id devices = NULL;
	cl_command_queue cmdQueue;
	cl_command_queue deviceQueue;
	cl_context context = NULL;

	//devices = clInitWithDeviceQueue(&context, &cmdQueue, &deviceQueue);
	devices = clInitWithDeviceQueueByParam(&context, &cmdQueue, &deviceQueue,CL_PLATFORM_ID,CL_DEVICE_ID);
	
	// OpenCL device memory for matrices, making use of Shared Virtual Memory
	TYPE* d_A = (TYPE*)clSVMAlloc(context, CL_MEM_READ_WRITE, mem_size_A, 0);

	/*copying data to shared virtual memory is included into execution time*/
	start_timer(&time);
	memcpy(d_A, h_A, mem_size_A);
	end_timer(&time);
	total_time += time.Result;

	//Load and build OpenCL kernel
	const char fileName[] = "bitonic_sort.cl";
	char option[128];
	sprintf(option, "-DTYPE=%s -DKERNELAMOUNTOFEVENTS=%i", EXPAND_QUOTE(TYPE), KERNELAMOUNTOFEVENTS);
	myCLBuildProgram(context, &clProgram, devices, fileName, option);

	clKernel = clCreateKernel(clProgram, "init", &errcode);
	CLU_ERRCHECK(errcode, "Error creating bitonic kernel");

	size_t  globalWorkSize[1];
	globalWorkSize[0] = SIZE;

	clSetKernelArgSVMPointer(clKernel, 0, d_A);
	CLU_ERRCHECK(clEnqueueNDRangeKernel(cmdQueue, clKernel, 1, NULL, globalWorkSize, NULL, 0, NULL, &profiling_event), "Error while enqueueing ndrangekernel");
	total_time += retrieveTotalExecutionTimeFromEvent(&profiling_event);

	//Sequential Bubblesort in order to compare the results
	sortMatrix(h_A_Seq, SIZE, EXPAND_QUOTE(TYPE));

	//Print Results
	//printMatrix(h_A,SIZE, EXPAND_QUOTE(TYPE), 4, "SORTED A (Parallel: Bitonic Sort)");
	//printMatrix(h_A_Seq, SIZE, EXPAND_QUOTE(TYPE), 4, "Sorted A (Sequential: BubbleSort)");

	printf("\nTotal time in nanoseconds = %llu\n", total_time);
	verifyTwoMatrices(d_A, h_A_Seq, SIZE, EXPAND_QUOTE(TYPE));

	free(h_A);
	free(h_A_Seq);
	clSVMFree(context, d_A);
	errcode = clReleaseKernel(clKernel);
	errcode |= clReleaseProgram(clProgram);
	errcode |= clReleaseCommandQueue(cmdQueue);
	CLU_ERRCHECK(errcode, "Error while releasing ocl objects");
	return EXIT_SUCCESS;
}

int calcAmountOfEvents() {
	int count = 0;
	for (int i = 0; i < (int)LOG; i++) {
		for (int j = 0; j <= i; j++) {
			count++;
		}
	}
	return count;
}
