/*
Author: Yakup Ipek
Bitonic Sort with Chunks (utilizing Local Memory) for OpenCL 2.0
Applying BitonicSort on Chunks which sort its elements by applying again BitonicSort
*/

#include "myOpenCL_Lib.h"
#include "matrixLib.h"
#define SIZE 8192
#define LOCALWORKSIZE 512
#define TYPE int

#ifndef CL_PLATFORM_ID
#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
#define CL_DEVICE_ID 0
#endif

/////////////////////////////////////////////////////////
// Program main
/////////////////////////////////////////////////////////

#define ISPOWEROFTWO(a)\
if(!((a != 0) && ((a & (~a + 1)) == a))) { \
	fprintf(stderr, "%s is not a power of 2\n",QUOTE(a)); \
	exit(-1); \
}

typedef struct {
	cl_context context;
	cl_command_queue cmd_queue,device_queue;
	cl_kernel cl_my_kernel;
	cl_program cl_my_program;
	cl_device_id devices;
} _ocl_management_;

int get_number_events(int size) { int a = 0; if (size < 1) return 0;  a += (int)log2(size) + get_number_events(size/2); return a; }

int main(void) {

	//Verify correct inputs (SIZE->Number of elements to sort, LOCALWORKSIZE->Number of work items within workgroup)
	ISPOWEROFTWO(SIZE);
	ISPOWEROFTWO(LOCALWORKSIZE);

	if (LOCALWORKSIZE > SIZE ) {
		fprintf(stderr, "[Invocation Fault] LOCALWORKSIZE needs to be smaller than SIZE\n");
		return -1;
	}
	else if (LOCALWORKSIZE == SIZE) {
		fprintf(stderr, "[Invocation Fault] LOCALWORKSIZE can't be the same as SIZE\n");
		fprintf(stderr, "[Invocation Fault] Algorithm works for number of chunks greater 1\n");
		fprintf(stderr, "[Invocation Fault] E.g. LOCALWORKSIZE=128 SIZE=256\n");
		return -1;
	}

	_ocl_management_ cl_data;
	size_t dataBytes;
	cl_int errcode;
	cl_event profiling_event;
	cl_ulong total_time = 0;
	cl_int status;


	//Initialize cl device
	//cl_data.devices = clInitWithDeviceQueue(&cl_data.context, &cl_data.cmd_queue,&cl_data.device_queue);
	cl_data.devices = clInitWithDeviceQueueByParam(&cl_data.context, &cl_data.cmd_queue,&cl_data.device_queue, CL_PLATFORM_ID, CL_DEVICE_ID);

	//Load and build OpenCL kernel
	const char fileName[] = "bitonic_sort.cl";
	char option[128];
	sprintf(option, "-DTYPE=%s -DGLOBALSIZE=%i -DLOCALWORKSIZE=%i -DNUMBEREVENTS=%i", EXPAND_QUOTE(TYPE),SIZE,LOCALWORKSIZE,get_number_events(SIZE));
	myCLBuildProgram(cl_data.context, &cl_data.cl_my_program, cl_data.devices, fileName, option);
	cl_data.cl_my_kernel = clCreateKernel(cl_data.cl_my_program, "bitonic", &errcode);
	CLU_ERRCHECK(errcode, "Error creating bitonic kernel");
	
	//Prepare problem-domain
	printf("Preparing data for sorting...\n");
	size_t mem_size_A = sizeof(TYPE)* SIZE;
	TYPE* vector = (TYPE*)malloc(mem_size_A);
	TYPE* vector_sequential = (TYPE*)malloc(mem_size_A);
	fillMatrixRandomly((void*)vector, SIZE, EXPAND_QUOTE(TYPE));
	memcpy(vector_sequential, vector, mem_size_A);

	//printMatrix(vector, SIZE, EXPAND_QUOTE(TYPE), LOCALWORKSIZE, "Vector which needs to be sorted");

	printf("_________________Start Sorting_______________\n");

	cl_mem memory_vector = clCreateBuffer(cl_data.context, CL_MEM_READ_WRITE, mem_size_A, NULL, &errcode);
	CLU_ERRCHECK(errcode, "Failed creating Buffer");

	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data.cmd_queue, memory_vector, CL_TRUE, 0, mem_size_A, vector, 0, NULL, &profiling_event), "Error writing buffer");
	total_time += retrieveTotalExecutionTimeFromEvent(&profiling_event);

	CLU_ERRCHECK(clSetKernelArg(cl_data.cl_my_kernel, 0, sizeof(cl_mem), (void *)&memory_vector), "Error while setting argument: vector");
	
	size_t  globalWorkSize[1] = {1};
	
	CLU_ERRCHECK(clEnqueueNDRangeKernel(cl_data.cmd_queue, cl_data.cl_my_kernel, 1, NULL, globalWorkSize, NULL, 0, NULL, &profiling_event), "Error enqueueing kernel");
	total_time += retrieveTotalExecutionTimeFromEvent(&profiling_event);

	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data.cmd_queue, memory_vector, CL_TRUE, 0, mem_size_A, vector, 0, NULL, &profiling_event), "Error reading buffer");
	total_time += retrieveTotalExecutionTimeFromEvent(&profiling_event);

	printf("_________________Finished Sorting_______________\n");

	//printMatrix(vector, SIZE, EXPAND_QUOTE(TYPE), LOCALWORKSIZE, "Sorted");

	//Sequential Bubblesort in order to compare the results
	sortMatrix(vector_sequential, SIZE, EXPAND_QUOTE(TYPE));

	printf("\nTotal time in nanoseconds = %llu\n", total_time);
	verifyTwoMatrices(vector, vector_sequential, SIZE, EXPAND_QUOTE(TYPE));

	free(vector);
	free(vector_sequential);
	errcode = clReleaseContext(cl_data.context);
	errcode |= clReleaseKernel(cl_data.cl_my_kernel);
	errcode |= clReleaseProgram(cl_data.cl_my_program);
	errcode |= clReleaseMemObject(memory_vector);
	errcode |= clReleaseCommandQueue(cl_data.cmd_queue);
	errcode |= clReleaseCommandQueue(cl_data.device_queue);

	CLU_ERRCHECK(errcode, "Error while releasing ocl objects");
	return EXIT_SUCCESS;
}
