/*
Author: Yakup Ipek
Bitonic sort with local_memory kernel
*/

__kernel void bitonic_step(__global TYPE* vector, __local TYPE* local_vector, int p, int q){

	int size = get_local_size(0)/2;
	int log = (int)log2((float)get_local_size(0));
	int g_id = get_global_id(0);
	int l_id = get_local_id(0);
	int index = l_id % (get_local_size(0)/2);
	int group_id = get_group_id(0);
	int d;
	bool up;
	int group = get_group_id(0);
	TYPE temp;
	
	int group_d = 1<<(p-q);
	
	if((group_id & group_d) ==0){

		bool group_up = ((group_id>>p) & 2) == 0;
	
		if(l_id < get_local_size(0)/2){
			local_vector[l_id] = vector[group_id*size+index];
		}else{
			local_vector[l_id] = vector[(group_id|group_d)*size+index];
		}
	
		barrier(CLK_LOCAL_MEM_FENCE);

		for(int i = 0; i<log; i++){
			for(int j = 0; j<=i; j++){

				d = 1 << (i-j);

				if(i==log-1){	//chunks need to go through BitonicBuild + BitonicSplit
					up = group_up;
				}else{
					up = ((l_id>>i) & 2) == 0;
				}
		
				if((l_id & d)==0 && (local_vector[l_id]>local_vector[l_id|d])==up){
					TYPE temp = local_vector[l_id];
					local_vector[l_id] = local_vector[l_id|d];
					local_vector[l_id|d]=temp;
				}		

				barrier(CLK_LOCAL_MEM_FENCE);				
			}
		}
		
		if(l_id < get_local_size(0)/2){
			vector[group_id*size+index] = local_vector[l_id];
		}else{
			vector[(group_id|group_d)*size+index] = local_vector[l_id];
		}
	}
}

__kernel void bitonic(__global TYPE* vector){

	clk_event_t evt[NUMBEREVENTS];
	uint local_mem_size = sizeof(TYPE)*LOCALWORKSIZE*2;
	ndrange_t child_ndrange = ndrange_1D(LOCALWORKSIZE*2*(GLOBALSIZE/LOCALWORKSIZE),LOCALWORKSIZE*2);
	int end = (int)log2((double)(GLOBALSIZE/LOCALWORKSIZE));
	int index=0;
	for(int i=0; i<end; i++){
		for(int j=0; j<=i; j++){
			void(^my_blk)(local void*)=^(local void* local_vector){
							bitonic_step(vector, local_vector,i,j);
			};
			if(i==0 && j==0){
				enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,child_ndrange,0,NULL,evt+(index),my_blk,local_mem_size);		
			}else{
				enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,child_ndrange,1,evt+(index-1),evt+(index),my_blk,local_mem_size);		
				release_event(evt[index-1]);
			}
			index++;
		}
	}
}