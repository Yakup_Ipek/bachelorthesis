/*
Author: Yakup Ipek
Bitonic sort with local_memory kernel
*/

__kernel void bitonic(__global TYPE* vector, __local TYPE* local_vector, int p, int q){

	int size = get_local_size(0)/2;
	int log = (int)log2((float)get_local_size(0));
	int g_id = get_global_id(0);
	int l_id = get_local_id(0);
	int index = l_id % (get_local_size(0)/2);
	int group_id = get_group_id(0);
	int d;
	bool up;
	int group = get_group_id(0);
	TYPE temp;
	
	int group_d = 1<<(p-q);
	
	if((group_id & group_d) ==0){

		bool group_up = ((group_id>>p) & 2) == 0;
	
		if(l_id < get_local_size(0)/2){
			local_vector[l_id] = vector[group_id*size+index];
		}else{
			local_vector[l_id] = vector[(group_id|group_d)*size+index];
		}
	
		barrier(CLK_LOCAL_MEM_FENCE);

		for(int i = 0; i<log; i++){
			for(int j = 0; j<=i; j++){

				d = 1 << (i-j);

				if(i==log-1){	//chunks need to go through BitonicBuild + BitonicSplit
					up = group_up;
				}else{
					up = ((l_id>>i) & 2) == 0;
				}
		
				if((l_id & d)==0 && (local_vector[l_id]>local_vector[l_id|d])==up){
					TYPE temp = local_vector[l_id];
					local_vector[l_id] = local_vector[l_id|d];
					local_vector[l_id|d]=temp;
				}		

				barrier(CLK_LOCAL_MEM_FENCE);				
			}
		}
		
		if(l_id < get_local_size(0)/2){
			vector[group_id*size+index] = local_vector[l_id];
		}else{
			vector[(group_id|group_d)*size+index] = local_vector[l_id];
		}
	}
}