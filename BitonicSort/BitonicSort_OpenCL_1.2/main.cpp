/*
Author: Yakup Ipek
Bitonic sort for OpenCL 1.X
*/

#include "myOpenCL_Lib.h"
#include "matrixLib.h"
#define SIZE 8192
#define TYPE float

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

/////////////////////////////////////////////////////////
// Program main
/////////////////////////////////////////////////////////

#define ISPOWEROFTWO(a)\
if(!((a != 0) && ((a & (~a + 1)) == a))) { \
	fprintf(stderr, "The Size Variable needs to be a power of 2\n"); \
	exit(-1); \
}

int main(int argc, char** argv)
{
	ISPOWEROFTWO(SIZE);

	// allocate and initialize host memory for matrices A and B
	unsigned int mem_size_A = sizeof(TYPE)* SIZE;
	TYPE* h_A = (TYPE*)malloc(mem_size_A);
	TYPE* h_A_Seq = (TYPE*)malloc(mem_size_A);

	fillMatrixRandomly((void*)h_A, SIZE, EXPAND_QUOTE(TYPE));
	memcpy(h_A_Seq, h_A, mem_size_A);


	// 5. Initialize OpenCL specific variables
	cl_program clProgram;
	cl_kernel clKernel;

	size_t dataBytes;
	size_t kernelLength;
	cl_int errcode;
	cl_event profiling_event;
	cl_ulong total_time = 0;

	// OpenCL device memory for matrices
	cl_mem d_A;

	//--------------retrieving platforms-----------------------//	
	cl_int status;
	cl_device_id devices = NULL;
	cl_command_queue cmdQueue;
	cl_context context = NULL;
	//devices = clInit(&context, &cmdQueue);
	devices = clInitByParam(&context, &cmdQueue, CL_PLATFORM_ID, CL_DEVICE_ID);
	
	// Setup device memory
	d_A = clCreateBuffer(context, CL_MEM_READ_WRITE, mem_size_A, NULL, &errcode);
	CLU_ERRCHECK(clEnqueueWriteBuffer(cmdQueue, d_A, CL_TRUE, 0, mem_size_A, h_A, 0, NULL, &profiling_event), "Error writing buffer A");
	total_time += retrieveTotalExecutionTimeFromEvent(&profiling_event);

	// 6. Load and build OpenCL kernel
	const char fileName[] = "bitonic_sort.cl";
	char option[128];
	sprintf(option, "-DTYPE=%s", EXPAND_QUOTE(TYPE));
	myCLBuildProgram(context, &clProgram, devices, fileName, option);
	clKernel = clCreateKernel(clProgram, "bitonic", &errcode);
	CLU_ERRCHECK(errcode, "Error creating bitonic kernel");


	size_t  globalWorkSize[1] = { SIZE };

	int log = log2(SIZE);
	//printf("Log: %d of %d\n", log, SIZE);
	CLU_ERRCHECK(clSetKernelArg(clKernel, 0, sizeof(cl_mem), (void *)&d_A), "Error while setting first argument");
	int count = 0;
	for (int i = 0; i < log; i++) {
		for (int j = 0; j <= i; j++) {
			//printf("Number of Iteration: %d\n", ++count);
			CLU_ERRCHECK(clSetKernelArg(clKernel, 1, sizeof(int), (void*)&i), "Error while setting second argument");
			CLU_ERRCHECK(clSetKernelArg(clKernel, 2, sizeof(int), (void*)&j), "Error while setting third argument");

			errcode = clEnqueueNDRangeKernel(cmdQueue, clKernel, 1, NULL, globalWorkSize, NULL, 0, NULL, &profiling_event);
			if (errcode != 0) {
				printf("Error: %d\n", errcode);
				exit(errcode);
			}

			total_time += retrieveTotalExecutionTimeFromEvent(&profiling_event);
		}
	}

	// 8. Retrieve result from device
	CLU_ERRCHECK(clEnqueueReadBuffer(cmdQueue, d_A, CL_TRUE, 0, mem_size_A, h_A, 0, NULL, &profiling_event), "Error reading buffer A");
	total_time += retrieveTotalExecutionTimeFromEvent(&profiling_event);

	//Sequential Bubblesort in order to compare the results
	sortMatrix(h_A_Seq, SIZE, EXPAND_QUOTE(TYPE));

	// 9. print out the results
	//printMatrix(h_A,SIZE,1,5, "SORTED A (Parallel: Bitonic Sort)");
	//printMatrix(h_A_Seq, SIZE, 1, 5, "Sorted A (Sequential: BubbleSort)");

	printf("\nTotal time in nanoseconds = %llu\n", total_time);
	verifyTwoMatrices(h_A, h_A_Seq, SIZE, EXPAND_QUOTE(TYPE));

	free(h_A);
	free(h_A_Seq);
	errcode = clReleaseMemObject(d_A);
	errcode |= clReleaseKernel(clKernel);
	errcode |= clReleaseProgram(clProgram);
	errcode |= clReleaseCommandQueue(cmdQueue);
	CLU_ERRCHECK(errcode,"Error while releasing ocl objects");
	return EXIT_SUCCESS;
}