/*
Author: Yakup Ipek
Bitonic sort kernel
*/

// OpenCL Kernel
__kernel void bitonic(__global TYPE* A, int p, int q){

	int id = get_global_id(0);
	int d = 1 << (p-q);

	bool up = ((id>>p) & 2) == 0;

	if((id & d)==0 && (A[id]>A[id|d])==up){
		TYPE temp = A[id];
		A[id] = A[id|d];
		A[id|d]=temp;
	}
}
