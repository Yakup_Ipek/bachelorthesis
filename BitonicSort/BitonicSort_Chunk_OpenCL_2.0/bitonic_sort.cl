/*
Author: Yakup Ipek
Bitonic sort with local_memory kernel
A merge map is used to pair chunks
*/


__kernel void bitonic_merge_chunks(__global TYPE* vector, __local TYPE* local_vector, __global int* merge_map){
	
	int size = get_local_size(0)/2;
	int log = (int)log2((double)get_local_size(0));
	int g_id = get_global_id(0);
	int l_id = get_local_id(0);
	int index = l_id % (get_local_size(0)/2);
	int d;
	bool up;
	int result = get_group_id(0)*size+index; 
	TYPE temp;
	
	if(merge_map[get_group_id(0)]!=-1){
		if(l_id < get_local_size(0)/2){
			local_vector[l_id] = vector[get_group_id(0)*size+index];
		}else{
			local_vector[l_id] = vector[merge_map[get_group_id(0)]*size+index];
		}
	}
	
	
	barrier(CLK_LOCAL_MEM_FENCE);

	for(int i = 0; i<log; i++){
		for(int j = 0; j<=i; j++){

			d = 1 << (i-j);
			up = ((l_id>>i) & 2) == 0;
		
			if((l_id & d)==0 && (local_vector[l_id]>local_vector[l_id|d])==up){
				TYPE temp = local_vector[l_id];
				local_vector[l_id] = local_vector[l_id|d];
				local_vector[l_id|d]=temp;
			}		

			barrier(CLK_LOCAL_MEM_FENCE);				
		}
	}
	if(merge_map[get_group_id(0)]!=-1){
		if(l_id < get_local_size(0)/2){
			vector[get_group_id(0)*size+index] = local_vector[l_id];
		}else{
			vector[merge_map[get_group_id(0)]*size+index] = local_vector[l_id];
		}
	}
}

__kernel void bitonic(__global TYPE* vector, __local TYPE* local_vector, __global int* merge_map){

		clk_event_t evt[TIMES*BOUNDARY];
		ndrange_t child_ndrange = ndrange_1D(GLOBALSIZE*2,LOCALWORKSIZE*2);
		uint local_mem_size = sizeof(TYPE)*LOCALWORKSIZE*2;

		int shift = GLOBALSIZE/LOCALWORKSIZE;
		for(int i=0; i<TIMES; i++){
			for(int j=0; j<BOUNDARY;j++){
				
				void(^my_blk)(local void*)=^(local void* local_vector){
							bitonic_merge_chunks(vector, local_vector,merge_map + j * shift);
				};
				
				if(i==0 && j==0){
					enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,child_ndrange,0,NULL,evt+(i*BOUNDARY+j),my_blk,local_mem_size);		
				}else{
					enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,child_ndrange,1,evt+(i*BOUNDARY+j-1),evt+(i*BOUNDARY+j),my_blk,local_mem_size);	
					release_event(evt[i*BOUNDARY+j-1]);
				}
			}
		}
	
}
