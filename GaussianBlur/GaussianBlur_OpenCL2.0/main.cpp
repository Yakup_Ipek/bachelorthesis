/*
Author: Yakup Ipek
Gaussian Blur in Parallel for OpenCL 2.0
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stb_image.h"	//external lib for loading image
#include "stb_image_write.h" //external lib for loading image
#include "myOpenCL_Lib.h"
#include "MyTime.h"

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

#define IMAGE_NAME "SourceImages/ffXV.jpg"	//image to load
#define SEQ_SAVE_IMAGE_AS "Images/ffXV_seq.jpg"	//image name for sequentially computed image
#define OCL_SAVE_IMAGE_AS "Images/ffXV_ocl_2.0.jpg" //image name for parallely computed image
#define KERNEL_FILE_NAME "kernel.cl"
#define PI 3.141592653589793

typedef struct {
	int width, height, components;
	unsigned char*data;
}ImageData;

typedef struct {
	int r, rs;
}EffectParameters;

void gaussian_blur_sequential(ImageData image, EffectParameters effect, unsigned char* bufferToSafeInto);

int main(void) {
	ImageData image;
	EffectParameters effect;
	image.data = stbi_load(IMAGE_NAME, &image.width, &image.height, &image.components, 0);
	unsigned char *g_data = (unsigned char*)malloc(sizeof(unsigned char)*image.width*image.height*image.components);
	size_t size_data = sizeof(unsigned char) * image.width*image.height*image.components;
	cl_context context;
	cl_command_queue command_queue, device_queue;
	//cl_device_id device_id = clInitWithDeviceQueue(&context, &command_queue, &device_queue);
	cl_device_id device_id = clInitWithDeviceQueueByParam(&context, &command_queue, &device_queue, CL_PLATFORM_ID, CL_DEVICE_ID);
	cl_program clProgram;
	cl_kernel clKernel;
	cl_int status;
	cl_event event;
	cl_ulong total_time = 0;
	MyTimeData time;
	effect.r = 4;
	effect.rs = ceil(effect.r*2.57);

	/*--------------print some details---------------*/
	printf("Image resolution: %d x %d\n", image.width, image.height);
	printf("Image #components: %d\n", image.components);
	printf("Width: %d\t Height: %d\n", image.width, image.height);

	/*---------------Initialize CL_DEVICE---------------*/
	char option[128];
	sprintf(option, "-DCOMPONENTS=%i -DM_PI=%f", image.components, PI);
	myCLBuildProgram(context, &clProgram, device_id, KERNEL_FILE_NAME, option);
	clKernel = clCreateKernel(clProgram, "gaussian_blur", &status);
	CLU_ERRCHECK(status, "Error creating kernel for gaussian_blur");

	/*---------------Create Buffers---------------*/
	unsigned char* mem_input =(unsigned char*) clSVMAlloc(context, CL_MEM_READ_ONLY, size_data, 0);
	unsigned char* mem_output = (unsigned char*)clSVMAlloc(context, CL_MEM_WRITE_ONLY, size_data, 0);

	start_timer(&time);
	memcpy(mem_input, image.data, size_data);
	end_timer(&time);
	total_time += time.Result;

	/*---------------Set Arguments---------------*/
	clSetKernelArgSVMPointer(clKernel, 0, mem_input);
	clSetKernelArgSVMPointer(clKernel, 1, mem_output);
	clSetKernelArg(clKernel, 2, sizeof(int), &effect.r);
	clSetKernelArg(clKernel, 3, sizeof(int), &effect.rs);

	/*--------------- Kernel---------------*/

	printf("\nParallel Computation starts...\n");
	size_t  globalWorkSize[2] = { (size_t) image.height, (size_t) image.width };

	CLU_ERRCHECK(clEnqueueNDRangeKernel(command_queue, clKernel, 2, NULL, globalWorkSize, NULL, 0, NULL, &event), "Failed to enqueue 2D kernel");
	total_time += retrieveTotalExecutionTimeFromEvent(&event);

	printf("\nParallel Computation finished\n");
	printf("Total time in nanoseconds = %llu\n\n", total_time);

	/*--------------Sequential Gaussian Blur Computation-----------*/
	gaussian_blur_sequential(image, effect, g_data);

	/*Writing image files*/
	printf("Writing files...\n");
	stbi_write_png(OCL_SAVE_IMAGE_AS, image.width, image.height, image.components, mem_output, image.width*image.components);
	stbi_write_png(SEQ_SAVE_IMAGE_AS, image.width, image.height, image.components, g_data, image.width*image.components);
	printf("Finished writing to files...\n");
	/*free and release*/
	stbi_image_free(image.data);
	free(g_data);
	clSVMFree(context,mem_input);
	clSVMFree(context, mem_output);
	status = clReleaseContext(context);
	status |= clReleaseCommandQueue(command_queue);
	status |= clReleaseProgram(clProgram);
	status |= clReleaseKernel(clKernel);
	CLU_ERRCHECK(status, "Error realeasing ocl data");
}

void gaussian_blur_sequential(ImageData image, EffectParameters effect, unsigned char* bufferToSafeInto) {
	int r = effect.r;
	int rs = effect.rs;
	printf("Start sequential  Gaussian Blur\n");
	int counter = 0;
	for (int i = 0; i < image.height; i++) {
		for (int j = 0; j < image.width; j++) {
			long double *val = (long double*)malloc(sizeof(long double)*image.components);
			memset(val, 0, sizeof(long double)*image.components);
			double wsum = 0;
			for (int iy = i - rs; iy < i + rs + 1; iy++) {
				for (int ix = j - rs; ix < j + rs + 1; ix++) {
					//printf("iy: %d\t ix: %d\n", iy, ix);
					int x = fmin(image.width - 1, fmax(0, ix));
					int y = fmin(image.height - 1, fmax(0, iy));
					double dsq = (ix - j)*(ix - j) + (iy - i) * (iy - i);
					double wght = exp(-dsq / (2 * r*r)) / (PI * 2 * r*r);
					wsum += wght;
					for (int k = 0; k < image.components; k++) {
						unsigned char v = image.data[k + y*image.width*image.components + x*image.components];
						*(val + k) += v * wght;
					}
				}
			}
			for (int k = 0; k < image.components; k++) {
				bufferToSafeInto[k + j*image.components + i*image.width*image.components] = roundf(*(val + k) / wsum);
			}
			free(val);
			counter++;
		}
		printf("Progress: %0.2f %%\r", (double)(counter * 100) / (double)(image.height*image.width));
	}
	printf("Sequential Computation finished\n");
}
