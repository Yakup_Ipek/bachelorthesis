/*
Author: Yakup Ipek
Gaussian Blur Kernel for OpenCL 1.X
*/
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
__kernel void gaussian_blur(__global unsigned char* input, __global unsigned char* output, int r, int rs){

	int idx = get_global_id(0);
	int idy = get_global_id(1);
	int height = get_global_size(0);
	int width = get_global_size(1);
	double val[COMPONENTS];
	double wsum=0;
	
	for(int k=0; k<COMPONENTS; k++){
		val[k]=0.0;
	}
	
	for(int iy= idx -rs; iy<idx+rs+1;iy++){
		for(int ix = idy -rs; ix < idy+rs+1; ix++){
			int x = min(width-1, max(0,ix));
			int y = min(height-1, max(0,iy));
			float dsq = (ix-idy)*(ix-idy)+(iy-idx)*(iy-idx);
			float wght = exp2(-dsq/(2*r*r)) / (M_PI * 2*r*r);
			wsum += wght;

			for(int k = 0; k<COMPONENTS; k++){
				unsigned char v = input[k+y*width*COMPONENTS+x*COMPONENTS];
				*(val+k)+= v*wght;

				//printf("Input Data: %d\n",v);
				//printf("Wght: %f\n",wght);
				//printf("Res: %f\n",v*wght);
				//printf("Val: %d\n", *(val+k));
			}
			
		}
	}

	for(int k = 0; k<COMPONENTS; k++){
		output[k+idy*COMPONENTS + idx*COMPONENTS*width]= round(*(val + k) / wsum);
		//printf("Val: %f, Wsum: %f\n",*(val+k),wsum);
		//printf("Value to safe: %f\n",round(*(val + k) / wsum));
		//printf("%d \n",output[k+idy*COMPONENTS + idx*COMPONENTS*width]);
	}

	/*
	if(idx == 0 && idy == 0){
		printf("Width: %d\n", width);
		printf("Height: %d\n", height);
		printf("COMPONENTS: %d\n",COMPONENTS);
		printf("R: %d\n",r);
		printf("Rs: %d\n",rs);
	}
	*/
}