/*
Author: Yakup Ipek
Dijkstra Sequential
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "Dijkstra.h"

int main(void) {
	srand(time(NULL));
	Graph graph;
	Dijkstra data;
	int num_of_nodes = 5;
	int number_of_tests = num_of_nodes > 100 ? 100 : num_of_nodes;
	int source = rand() % num_of_nodes;
	int find;
	int min = 2;
	int max = 4;

	generate_random_directed_graph(&graph, num_of_nodes, max, min, 1024);
	std::cout << "Source Node: " << source << "\n";
	//print_adjacency_list(graph);
	
	/*Compute the dijkstra algorithm*/
	std::cout << "Compute dijkstra ..\n";
	initialize_dijkstra(&data, num_of_nodes);
	compute_dijkstra(&graph, &data, source);
	
	/*Tests*/
	printf("Print Some Tests...\n");
	for (int i = 0; i < number_of_tests; i++){
		find= rand() % num_of_nodes;
		if (has_path_to_source(data, source, find)) {
			print_dijkstra_path(data, find);
		}
		else {
			std::cout << "There is no path from " << find << " to source " << source << "\n";
		}
	}

	/*Free*/
	free_dijkstra(&data);
	clean_graph(&graph);
	return EXIT_SUCCESS;
}
