/*
Author: Yakup Ipek
Dijkstra in parallel for OpenCL 2.0
*/
#include <time.h>
#include "MyTime.h"
#include "myOpenCL_Lib.h"
#include "Dijkstra.h"
#include "Graph.h"
#include "matrixLib.h"
#include "GraphReader.h"

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

#define KERNEL_FILE_NAME "./kernel.cl"
#define NODES 75
#define INT_MAX 2147483647

typedef struct {
	cl_context context;
	cl_command_queue command_queue, device_queue;
	cl_device_id device_id;
	cl_program cl_my_program;
	cl_kernel cl_kernel_init;
} OclData;

typedef struct {
	int source_node;
	int size_neighbours;
	int* starting;
	int* neighbours;
	int* weights;
	int* mark_next;
	int number_of_nodes;
	Dijkstra dijkstra;
}KernelArguments;

void verify(Dijkstra dijkstra_seq, Dijkstra dijkstra_par, int number_of_nodes);
void dijkstra_init(OclData* cl_data, KernelArguments* arguments, cl_ulong* time);

int main(void) {

	srand(time(NULL));
	/*-------------Initialization-----------------*/
	OclData cl_data;
	Graph graph;
	cl_int status;
	Dijkstra dijkstra_seq;
	KernelArguments kernel_arguments;
	cl_ulong total_time = 0;
	/*----------------initialize OpenCL device--------------*/
	//cl_data.device_id = clInitWithDeviceQueue(&cl_data.context, &cl_data.command_queue, &cl_data.device_queue);
	cl_data.device_id = clInitWithDeviceQueueByParam(&cl_data.context, &cl_data.command_queue, &cl_data.device_queue, CL_PLATFORM_ID, CL_DEVICE_ID);
	
	/*________________Generate graph randomly in runtime__________________*/
	
	int number_of_nodes = NODES;
	int min_number_of_nodes = 1;
	int max_number_of_nodes = NODES / 2;
	int source_node = rand() % NODES;
	generate_random_directed_graph(&graph, number_of_nodes, max_number_of_nodes, min_number_of_nodes, 100);
	
	/*____________________________________________________________________*/

	/*_____________________Read graph from file_____________________*/
	/*
	read_weighted_graph_into_file(&graph, "../../Library/GraphWriterReader/GeneratedGraphs/Nodes5K_Neighbours2.5K.txt");
	int number_of_nodes = graph.nodes_count;
	int source_node = 790;
	*/
	/*______________________________________________________________*/

	/*-----------initialize kernel arguments---------------------*/
	kernel_arguments.source_node = source_node;
	kernel_arguments.starting = (int*)malloc(sizeof(int)*number_of_nodes);	//starting indices for neighbours
	kernel_arguments.mark_next = (int*)calloc(number_of_nodes, sizeof(int));
	kernel_arguments.size_neighbours = size_to_wrap_up(graph);
	kernel_arguments.neighbours = (int*)malloc(sizeof(int)*kernel_arguments.size_neighbours);
	kernel_arguments.weights = (int*)malloc(sizeof(int)*kernel_arguments.size_neighbours);
	kernel_arguments.number_of_nodes = number_of_nodes;
	/*----------wrap up neighbors and weights - in order to transfer them at once to the kernel----------*/
	wrap_neighbours_up(graph, &kernel_arguments.neighbours);
	wrap_weights_up(graph, &kernel_arguments.weights);
	determine_starting_points(graph, &kernel_arguments.starting);

	/*----------------initialize data for shortest path-------*/
	initialize_dijkstra(&kernel_arguments.dijkstra, number_of_nodes);
	initialize_dijkstra(&dijkstra_seq, number_of_nodes);

	/*------------Build program and define kernel--------------*/
	char option[128];
	sprintf(option, "-DINT_MAX=%i -DBOUNDARY=%i", INT_MAX, kernel_arguments.size_neighbours);	//boundary is necessary for the last node to process correctly its neighbours
	myCLBuildProgram(cl_data.context, &cl_data.cl_my_program, cl_data.device_id, KERNEL_FILE_NAME, option);
	cl_data.cl_kernel_init = clCreateKernel(cl_data.cl_my_program, "init_kernel", &status);
	CLU_ERRCHECK(status, "Error creating init_kernel");

	/*
	print_adjacency_list(graph);
	printf("---------------------\n");
	printMatrix(kernel_arguments.starting, number_of_nodes, "int", NODES, "Starting points");
	printMatrix(kernel_arguments.neighbours, kernel_arguments.size_neighbours, "int", NODES, "Neighbours");
	*/

	printf("[Source Node]: %d\n\n", source_node);

	/*----------------compute dijkstra in parallel------------*/
	printf("------------------------------------------------\n");
	printf("[Dijkstra]Started parallel computation ...\n\n");

	dijkstra_init(&cl_data, &kernel_arguments, &total_time);

	printf("Total time in nanoseconds = %llu\n\n", total_time);
	printf("[Dijkstra]Finished parallel computation ...\n\n");
	printf("------------------------------------------------\n");

	/*----------------compute dijkstra sequentially------------*/
	
	printf("[Dijkstra]Started sequential computation ...\n\n");

	compute_dijkstra(&graph, &dijkstra_seq, kernel_arguments.source_node);

	printf("[Dijkstra]Finished sequential computation ...\n\n");
	printf("------------------------------------------------\n");

	verify(dijkstra_seq, kernel_arguments.dijkstra,kernel_arguments.number_of_nodes);

	
	/*
	for (int i = 0; i < NODES; i++) {
		printf("[PAR]Distance[%d]: %d\t Parent[%d]: %d\n", i, kernel_arguments.dijkstra.dist_to[i], i, kernel_arguments.dijkstra.edge_to[i]);
		printf("[SEQ]Distance[%d]: %d\t Parent[%d]: %d\n", i, dijkstra_seq.dist_to[i], i, dijkstra_seq.edge_to[i]);
	}
	*/

	/*         Free & Release             */
	free(kernel_arguments.starting);
	free(kernel_arguments.neighbours);
	free(kernel_arguments.weights);
	free(kernel_arguments.mark_next);
	free_dijkstra(&kernel_arguments.dijkstra);
	free_dijkstra(&dijkstra_seq);
	clean_graph(&graph);
	status = clReleaseContext(cl_data.context);
	status |= clReleaseCommandQueue(cl_data.command_queue);
	status |= clReleaseCommandQueue(cl_data.device_queue);
	status |= clReleaseProgram(cl_data.cl_my_program);
	status |= clReleaseKernel(cl_data.cl_kernel_init);
	CLU_ERRCHECK(status, "Error while releasing ocl data");

	return EXIT_SUCCESS;
}

void verify(Dijkstra dijkstra_seq, Dijkstra dijkstra_par, int number_of_nodes) {
	for (int i = 0; i < number_of_nodes; i++) {
		if (dijkstra_par.dist_to[i] != dijkstra_seq.dist_to[i]) {
			fprintf(stderr, "Verification: Error\n");
			exit(-1);
		}
		if (dijkstra_par.edge_to[i] != dijkstra_seq.edge_to[i]) {
			printf("Due to parallel computation the parent node might differ\n");
			printf("[PAR]Distance[%d]: %d\t Parent[%d]: %d\n", i, dijkstra_par.dist_to[i], i, dijkstra_par.edge_to[i]);
			printf("[SEQ]Distance[%d]: %d\t Parent[%d]: %d\n", i, dijkstra_seq.dist_to[i], i, dijkstra_seq.edge_to[i]);
		}
	}
	printf("Verification: OK\n\n");
}

void dijkstra_init(OclData* cl_data, KernelArguments* arguments, cl_ulong* time) {
	
	cl_int status;
	cl_event event;
	int num = arguments->number_of_nodes;
	size_t global_size[1] = {(size_t) num };
	size_t size = sizeof(int) * num;
	size_t size_neighbours = sizeof(int)*arguments->size_neighbours;
	MyTimeData my_time;

	/*             SVM             */
	int* mem_dist_to = (int*)clSVMAlloc(cl_data->context, CL_MEM_READ_WRITE, size, 0);
	int* mem_marked = (int*)clSVMAlloc(cl_data->context, CL_MEM_READ_WRITE, size, 0);
	int* mem_marked_next = (int*)clSVMAlloc(cl_data->context, CL_MEM_READ_WRITE, size, 0);
	int* mem_edge_to = (int*)clSVMAlloc(cl_data->context, CL_MEM_WRITE_ONLY, size, 0);
	int* mem_start_and_end = (int*)clSVMAlloc(cl_data->context,CL_MEM_READ_ONLY,size,0);
	int* mem_neighbours = (int*)clSVMAlloc(cl_data->context,CL_MEM_READ_ONLY,size_neighbours,0);
	int* mem_weights = (int*)clSVMAlloc(cl_data->context,CL_MEM_READ_ONLY,size_neighbours,0);
	bool* mem_changed = (bool*)clSVMAlloc(cl_data->context,CL_MEM_READ_WRITE,sizeof(bool),0);
	int* mem_source_node = (int*)clSVMAlloc(cl_data->context, CL_MEM_READ_ONLY, sizeof(int), 0);


	/*               Prepare Arguments             */
	start_timer(&my_time);
	memcpy(mem_dist_to,arguments->dijkstra.dist_to,size);
	memset(mem_marked,0,size);
	memset(mem_marked_next, 0, size);
	memcpy(mem_start_and_end, arguments->starting, size);
	memcpy(mem_neighbours, arguments->neighbours, size_neighbours);
	memcpy(mem_weights, arguments->weights, size_neighbours);
	*mem_changed = false;
	*mem_source_node = arguments->source_node;
	end_timer(&my_time);
	*time += my_time.Result;

	/*             Setting Arguments             */
	clSetKernelArgSVMPointer(cl_data->cl_kernel_init, 0, mem_dist_to);
	clSetKernelArgSVMPointer(cl_data->cl_kernel_init, 1, mem_marked);
	clSetKernelArgSVMPointer(cl_data->cl_kernel_init, 2, mem_edge_to);
	clSetKernelArgSVMPointer(cl_data->cl_kernel_init, 3, mem_source_node);
	clSetKernelArgSVMPointer(cl_data->cl_kernel_init, 4, mem_marked_next);
	clSetKernelArgSVMPointer(cl_data->cl_kernel_init, 5, mem_start_and_end);
	clSetKernelArgSVMPointer(cl_data->cl_kernel_init, 6, mem_neighbours);
	clSetKernelArgSVMPointer(cl_data->cl_kernel_init, 7, mem_weights);
	clSetKernelArgSVMPointer(cl_data->cl_kernel_init, 8, mem_changed);
	
	/*             Enqueue Kernel             */
	CLU_ERRCHECK(clEnqueueNDRangeKernel(cl_data->command_queue, cl_data->cl_kernel_init, 1, NULL, global_size, NULL, 0, NULL, &event), "Failed to enqueue 1D kernel");
	*time += retrieveTotalExecutionTimeFromEvent(&event);

	/*               Copy Result               */
	//start_timer(&my_time);	//not necessary to add this time since arguments->dijkstra could have been used as SVM
	memcpy(arguments->dijkstra.dist_to,mem_dist_to,size);
	memcpy(arguments->dijkstra.edge_to, mem_edge_to, size);
	//end_timer(&my_time);
	//*time += my_time.Result;
	
	/*             Release             */
	clSVMFree(cl_data->context, mem_dist_to);
	clSVMFree(cl_data->context, mem_marked);
	clSVMFree(cl_data->context, mem_edge_to);
	clSVMFree(cl_data->context, mem_marked_next);
	clSVMFree(cl_data->context, mem_start_and_end);
	clSVMFree(cl_data->context, mem_neighbours);
	clSVMFree(cl_data->context, mem_weights);
	clSVMFree(cl_data->context, mem_changed);
	clSVMFree(cl_data->context, mem_source_node);
}
