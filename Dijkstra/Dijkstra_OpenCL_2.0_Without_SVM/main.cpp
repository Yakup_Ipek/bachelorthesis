/*
Author: Yakup Ipek
Dijkstra in parallel for OpenCL 2.0 without SVM
*/
#include <time.h>
#include "MyTime.h"
#include "myOpenCL_Lib.h"
#include "Dijkstra.h"
#include "Graph.h"
#include "matrixLib.h"
#include "GraphReader.h"

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

#define KERNEL_FILE_NAME "./kernel.cl"
#define NODES 199
#define INT_MAX 2147483647

typedef struct {
	cl_context context;
	cl_command_queue command_queue, device_queue;
	cl_device_id device_id;
	cl_program cl_my_program;
	cl_kernel cl_kernel_init;
} OclData;

typedef struct {
	int source_node;
	int size_neighbours;
	int* starting;
	int* neighbours;
	int* weights;
	int* mark_next;
	int number_of_nodes;
	Dijkstra dijkstra;
}KernelArguments;

void verify(Dijkstra dijkstra_seq, Dijkstra dijkstra_par, int number_of_nodes);
void dijkstra_init(OclData* cl_data, KernelArguments* arguments, cl_ulong* time);

int main(void) {

	srand(time(NULL));
	/*-------------Initialization-----------------*/
	OclData cl_data;
	Graph graph;
	cl_int status;
	Dijkstra dijkstra_seq;
	KernelArguments kernel_arguments;
	cl_ulong total_time = 0;
	/*----------------initialize OpenCL device--------------*/
	//cl_data.device_id = clInitWithDeviceQueue(&cl_data.context, &cl_data.command_queue, &cl_data.device_queue);
	cl_data.device_id = clInitWithDeviceQueueByParam(&cl_data.context, &cl_data.command_queue, &cl_data.device_queue, CL_PLATFORM_ID, CL_DEVICE_ID);
	
	/*________________Generate graph randomly in runtime__________________*/
	
	int number_of_nodes = NODES;
	int min_number_of_nodes = 1;
	int max_number_of_nodes = NODES / 2;
	int source_node = rand() % NODES;
	generate_random_directed_graph(&graph, number_of_nodes, max_number_of_nodes, min_number_of_nodes, 100);
	
	/*____________________________________________________________________*/

	/*_____________________Read graph from file_____________________*/
	/*
	read_weighted_graph_into_file(&graph, "../../Library/GraphWriterReader/GeneratedGraphs/[WEIGHTED]Nodes10K_Neighbours5K.txt");
	int number_of_nodes = graph.nodes_count;
	int source_node = 790;
	*/
	/*______________________________________________________________*/

	/*-----------initialize kernel arguments---------------------*/
	kernel_arguments.source_node = source_node;
	kernel_arguments.starting = (int*)malloc(sizeof(int)*number_of_nodes);	//starting indices for neighbours
	kernel_arguments.mark_next = (int*)calloc(number_of_nodes, sizeof(int));
	kernel_arguments.size_neighbours = size_to_wrap_up(graph);
	kernel_arguments.neighbours = (int*)malloc(sizeof(int)*kernel_arguments.size_neighbours);
	kernel_arguments.weights = (int*)malloc(sizeof(int)*kernel_arguments.size_neighbours);
	kernel_arguments.number_of_nodes = number_of_nodes;
	/*----------wrap up neighbors and weights - in order to transfer them at once to the kernel----------*/
	wrap_neighbours_up(graph, &kernel_arguments.neighbours);
	wrap_weights_up(graph, &kernel_arguments.weights);
	determine_starting_points(graph, &kernel_arguments.starting);

	/*----------------initialize data for shortest path-------*/
	initialize_dijkstra(&kernel_arguments.dijkstra, number_of_nodes);
	initialize_dijkstra(&dijkstra_seq, number_of_nodes);

	/*------------Build program and define kernel--------------*/
	char option[128];
	sprintf(option, "-DINT_MAX=%i -DBOUNDARY=%i", INT_MAX, kernel_arguments.size_neighbours);	//boundary is necessary for the last node to process correctly its neighbours
	myCLBuildProgram(cl_data.context, &cl_data.cl_my_program, cl_data.device_id, KERNEL_FILE_NAME, option);
	cl_data.cl_kernel_init = clCreateKernel(cl_data.cl_my_program, "init_kernel", &status);
	CLU_ERRCHECK(status, "Error creating init_kernel");

	/*
	print_adjacency_list(graph);
	printf("---------------------\n");
	printMatrix(kernel_arguments.starting, number_of_nodes, "int", NODES, "Starting points");
	printMatrix(kernel_arguments.neighbours, kernel_arguments.size_neighbours, "int", NODES, "Neighbours");
	*/

	printf("[Source Node]: %d\n\n", source_node);

	/*----------------compute dijkstra in parallel------------*/
	printf("------------------------------------------------\n");
	printf("[Dijkstra]Started parallel computation ...\n\n");

	dijkstra_init(&cl_data, &kernel_arguments, &total_time);

	printf("Total time in nanoseconds = %llu\n\n", total_time);
	printf("[Dijkstra]Finished parallel computation ...\n\n");
	printf("------------------------------------------------\n");

	/*----------------compute dijkstra sequentially------------*/
	
	printf("[Dijkstra]Started sequential computation ...\n\n");

	compute_dijkstra(&graph, &dijkstra_seq, kernel_arguments.source_node);

	printf("[Dijkstra]Finished sequential computation ...\n\n");
	printf("------------------------------------------------\n");

	verify(dijkstra_seq, kernel_arguments.dijkstra,kernel_arguments.number_of_nodes);
	
	
	/*
	for (int i = 0; i < NODES; i++) {
	printf("[PAR]Distance[%d]: %d\t Parent[%d]: %d\n", i, kernel_arguments.dijkstra.dist_to[i], i, kernel_arguments.dijkstra.edge_to[i]);
	printf("[SEQ]Distance[%d]: %d\t Parent[%d]: %d\n", i, dijkstra_seq.dist_to[i], i, dijkstra_seq.edge_to[i]);
	}
	*/

	/*         Free & Release             */
	free(kernel_arguments.starting);
	free(kernel_arguments.neighbours);
	free(kernel_arguments.weights);
	free(kernel_arguments.mark_next);
	free_dijkstra(&kernel_arguments.dijkstra);
	free_dijkstra(&dijkstra_seq);
	clean_graph(&graph);
	status = clReleaseContext(cl_data.context);
	status |= clReleaseCommandQueue(cl_data.command_queue);
	status |= clReleaseCommandQueue(cl_data.device_queue);
	status |= clReleaseProgram(cl_data.cl_my_program);
	status |= clReleaseKernel(cl_data.cl_kernel_init);
	CLU_ERRCHECK(status, "Error while releasing ocl data");

	return EXIT_SUCCESS;
}

void verify(Dijkstra dijkstra_seq, Dijkstra dijkstra_par, int number_of_nodes) {
	for (int i = 0; i < number_of_nodes; i++) {
		if (dijkstra_par.dist_to[i] != dijkstra_seq.dist_to[i]) {
			fprintf(stderr, "Verification: Error\n");
			exit(-1);
		}
		if (dijkstra_par.edge_to[i] != dijkstra_seq.edge_to[i]) {
			printf("Due to parallel computation the parent node might differ\n");
			printf("[PAR]Distance[%d]: %d\t Parent[%d]: %d\n", i, dijkstra_par.dist_to[i], i, dijkstra_par.edge_to[i]);
			printf("[SEQ]Distance[%d]: %d\t Parent[%d]: %d\n", i, dijkstra_seq.dist_to[i], i, dijkstra_seq.edge_to[i]);
			//print_dijkstra_path(dijkstra_par, i);
			//print_dijkstra_path(dijkstra_seq, i);
		}
	}
	printf("Verification: OK\n\n");
}

void dijkstra_init(OclData* cl_data, KernelArguments* arguments, cl_ulong* time) {

	cl_int status;
	cl_event event;
	bool changed = false;
	int num = arguments->number_of_nodes;
	size_t global_size[1] = {(size_t) num };
	size_t size = sizeof(int) * num;
	size_t size_neighbours = sizeof(int)*arguments->size_neighbours;
	MyTimeData my_time;

	/*             SVM             */
	cl_mem mem_dist_to = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, size, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_dist object");
	cl_mem mem_marked = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, size, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_marked object");
	cl_mem mem_marked_next = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, size, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_marked_next object");
	cl_mem mem_edge_to = clCreateBuffer(cl_data->context, CL_MEM_WRITE_ONLY, size, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_edge object");
	cl_mem mem_start_and_end = clCreateBuffer(cl_data->context, CL_MEM_READ_ONLY, size, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_start_and_end object");
	cl_mem mem_neighbours = clCreateBuffer(cl_data->context, CL_MEM_READ_ONLY, size_neighbours, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_neighbours object");
	cl_mem mem_weights = clCreateBuffer(cl_data->context, CL_MEM_READ_ONLY, size_neighbours, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_weights object");
	cl_mem mem_changed = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, sizeof(bool), NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_changed object");


	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_start_and_end, CL_TRUE, 0, size, arguments->starting, 0, NULL, &event), "Error writing buffer starting");
	*time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_neighbours, CL_TRUE, 0, size_neighbours, arguments->neighbours, 0, NULL, &event), "Error writing buffer neighbours");
	*time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_weights, CL_TRUE, 0, size_neighbours, arguments->weights, 0, NULL, &event), "Error writing buffer weights");
	*time += retrieveTotalExecutionTimeFromEvent(&event);

	
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 0, sizeof(cl_mem), (void *)&mem_dist_to), "Error while setting argument: mem_dist");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 1, sizeof(cl_mem), (void *)&mem_marked), "Error while setting argument: mem_marked");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 2, sizeof(cl_mem), (void *)&mem_edge_to), "Error while setting argument: mem_edge");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 3, sizeof(int), (void *)&arguments->source_node), "Error while setting argument: source");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 4, sizeof(cl_mem), (void *)&mem_marked_next), "Error while setting argument: mem_marked_next");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 5, sizeof(cl_mem), (void *)&mem_start_and_end), "Error while setting argument: mem_start_and_end");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 6, sizeof(cl_mem), (void *)&mem_neighbours), "Error while setting argument: mem_neighbours");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 7, sizeof(cl_mem), (void *)&mem_weights), "Error while setting argument: mem_weights");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 8, sizeof(cl_mem), (void *)&mem_changed), "Error while setting argument: mem_changed");

	/*             Enqueue Kernel             */
	CLU_ERRCHECK(clEnqueueNDRangeKernel(cl_data->command_queue, cl_data->cl_kernel_init, 1, NULL, global_size, NULL, 0, NULL, &event), "Failed to enqueue 1D kernel");
	*time += retrieveTotalExecutionTimeFromEvent(&event);

	/*          Reading Result             */
	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_dist_to, CL_TRUE, 0, size, arguments->dijkstra.dist_to, 0, NULL, &event), "Error reading buffer dist_to");
	*time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_edge_to, CL_TRUE, 0, size, arguments->dijkstra.edge_to, 0, NULL, &event), "Error reading buffer edge_to");
	*time += retrieveTotalExecutionTimeFromEvent(&event);

	/*             Release             */
	status = clReleaseMemObject(mem_dist_to);
	status |= clReleaseMemObject(mem_marked);
	status |= clReleaseMemObject(mem_edge_to);
	status |= clReleaseMemObject(mem_marked_next);
	status |= clReleaseMemObject(mem_start_and_end);
	status |= clReleaseMemObject(mem_neighbours);
	status |= clReleaseMemObject(mem_weights);
	status |= clReleaseMemObject(mem_changed);
	CLU_ERRCHECK(status, "Error while releasing ocl data");
}
