/*
Author: Yakup Ipek
Dijkstra kernel for OpenCL 1.2
*/

__kernel void dijkstra_second_step(volatile __global int* dist_to, __global int* marked,__global int* edge_to,__global int* mark_next,
 __global int* start_and_end, __global int* neighbours, __global int* weights,__global bool* changed){

	int gid = get_global_id(0);
	int start, end;

	if(marked[gid] > 0){
		marked[gid]=0;
		
		if(gid < get_global_size(0) - 1){
			start = start_and_end[gid];
			end = start_and_end[gid+1];
		}else{ //no further neighbors available to process	
			start = start_and_end[gid];
			end = BOUNDARY;
		}

		for(int i = start; i<end; i++){
			
			int node = neighbours[i];
			int weight = dist_to[gid] + weights[i];
			
			if((atomic_min(dist_to + node, weight)) != dist_to[node] ){
				edge_to[node] = gid;
				atomic_inc(mark_next + node);
				*changed=true;
				//printf("Parent: %d, Child_Node: %d, Distance: %d\n",gid,node, dist_to[node]);
			}
		}
	}
}

__kernel void init_kernel(__global int* dist_to, __global int* marked,__global int* edge_to,int source){
	
	int gid=get_global_id(0);
	if(source==gid){
		marked[gid]=1;
		dist_to[gid]=0;
		edge_to[gid]=-1;	//has no parent
	}else{
		marked[gid]=0;
		dist_to[gid]=INT_MAX;
	}
}