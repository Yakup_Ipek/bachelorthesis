/*
Author: Yakup Ipek
Breadth first search, sequential algorithm partially parallelized - kernel
*/


__kernel void init_kernel(__global int* dist_to, __global int* marked, int source){
	
	int gid=get_global_id(0);

	if(source==gid){
		marked[gid]=true;
		dist_to[gid]=0;
	}else{
		marked[gid]=0;
		dist_to[gid]=INT_MAX;
	}
}

__kernel void bfs_kernel(__global int* dist_to, __global int* edge_to, __global int* marked, __global int* adjacent, int parent_node, __global int* queue){

	int idx = get_global_id(0);
	int node = adjacent[idx];
	
	if(!marked[node]){
		edge_to[node] = parent_node;
		dist_to[node] = dist_to[parent_node]+1;
		marked[node] = 1;
		queue[idx] = node;
	}

}