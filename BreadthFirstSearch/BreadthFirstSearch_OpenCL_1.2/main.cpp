/*
Author: Yakup Ipek
Breadth first search, sequential algorithm partially parallelized
*/

#include <time.h>
#include <assert.h>
#include "myOpenCL_Lib.h"
#include "matrixLib.h"
#include "GraphReaderB.h"
#include "BreadthFirstSearchLib.h"

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

#define NODES 500	//hardware dependent due to recursive calls, program crushes if set too great with stackoverflow
#define KERNEL_FILE_NAME "./kernel.cl"

typedef struct {
	cl_context context;
	cl_command_queue command_queue;
	cl_device_id device_id;
	cl_program cl_my_program;
	cl_kernel cl_kernel_init, cl_kernel_bfs;
} OclData;

typedef struct {
	cl_ulong total_time=0;
} TimeData;

/*first kernel*/
void bfs_parallel_first_step(OclData* cl_data, BfsData* bfs, int number_of_nodes, int source, TimeData* time);
/*second kernel*/
void bfs_parallel(OclData* cl_data, BfsData* bfs, Graph* graph, List** queue, int source, TimeData* time);
void bfs_parallel_recursion(OclData* cl_data, BfsData* bfs, Graph* graph, List** queue, TimeData *time);
const char* test_solution(BfsData bfs, BfsData bfs_seq, int look_up_range);

int main(void) {

	Graph graph;
	Graph u_graph;
	OclData cl_data;
	BfsData bfs_par, bfs_par_2;
	BfsData bfs_seq, bfs_seq_2;
	TimeData time, time_2;
	List* queue=NULL;
	List* queue_2=NULL;
	cl_int status;
	cl_event event;
	
	/*-----------------Usability---------------*/
	printf("<-------------------------------------------------->\n");
	printf("\nUsability: number of nodes can be changed by changing the value of the NODES Makro\n");
	printf("\nImportant: If program crushes, then the number of nodes needs to be decreased!\n");
	printf("\nReason: Bfs for OpenCL 1.2 is implemted recursively, great number leads to stackoverflow\n");
	printf("\n<-------------------------------------------------->\n\n");
	/*----------------initialize OpenCL device--------------*/
	//cl_data.device_id = clInit(&cl_data.context,&cl_data.command_queue);
	cl_data.device_id = clInitByParam(&cl_data.context,&cl_data.command_queue, CL_PLATFORM_ID, CL_DEVICE_ID);
	
	/*________________Generate graph randomly in runtime__________________*/
	
	int number_of_nodes = NODES;
	int minimum_neigbors = 2;
	int maximum_neigbors = NODES / 4;
	int source = rand() % NODES;
	
	/*-------------generate graphs-----------------------*/
	
	generate_random_undirected_graph(&u_graph, number_of_nodes, minimum_neigbors);
	generate_random_directed_graph(&graph, number_of_nodes, maximum_neigbors, minimum_neigbors);
	
	/*____________________________________________________________________*/
	
	/*_____________________Read graph from file_____________________*/
	/*
	read_graph_from_file(&graph, "../../Library/GraphWriterReader/GeneratedGraphs/[NORMAL]Nodes10K_Neighbours5K.txt");
	int number_of_nodes = graph.nodes_count;
	int source = 5;
	*/
	/*____________________________________________________________________*/

	//print_adjacency_list(u_graph);
	//print_adjacency_list(graph);

	/*-------------Bfs initializing--------------*/
	initialize_bfs(&bfs_par, number_of_nodes);
	initialize_bfs(&bfs_par_2, number_of_nodes);
	initialize_bfs(&bfs_seq, number_of_nodes);
	initialize_bfs(&bfs_seq_2, number_of_nodes);

	/*-------------Bfs sequential --------------*/
	bfs_seqential(graph, &bfs_seq, source);
	bfs_seqential(u_graph, &bfs_seq_2, source);

	/*----------Testing functionality of sequential bfs---------*/
	//test((void*) &u_graph, source, 500, 100,2,true);
	//test((void*)&graph, source, 15, 10, 1, true);

	/*       Build program and define kernel       */	
	char option[128];
	sprintf(option, "-DINT_MAX=%i",INT_MAX);
	myCLBuildProgram(cl_data.context, &cl_data.cl_my_program, cl_data.device_id, KERNEL_FILE_NAME, option);
	cl_data.cl_kernel_init = clCreateKernel(cl_data.cl_my_program, "init_kernel", &status);
	CLU_ERRCHECK(status, "Error creating init_kernel");
	cl_data.cl_kernel_bfs = clCreateKernel(cl_data.cl_my_program,"bfs_kernel", &status);
	CLU_ERRCHECK(status, "Error creating bfs_kernel");
	
	/*++++++++++++++++++++++Directed Graph++++++++++++++++++++++++++*/
	/*-------------First Kernel Function--------------*/

	bfs_parallel_first_step(&cl_data, &bfs_par, number_of_nodes, source, &time);

	/*-------------Second Kernel Function--------------*/

	bfs_parallel(&cl_data, &bfs_par, &graph, &queue, source,&time);

	/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	
	/*++++++++++++++++++++++++++++Undirected Graph+++++++++++++++++++++++++*/
	/*-------------First Kernel Function--------------*/

	bfs_parallel_first_step(&cl_data, &bfs_par_2, number_of_nodes, source, &time_2);

	/*-------------Second Kernel Function--------------*/

	bfs_parallel(&cl_data, &bfs_par_2, &u_graph, &queue_2, source,&time_2);

	/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

	/*-------------Some tests--------------*/
	
	printf("[Directed_Graph]Verification : %s\n", verify_bfs(bfs_par, bfs_seq, number_of_nodes));
	printf("[Directed_Graph] Total time in nanoseconds = %llu\n\n", time.total_time);

	
	printf("[Undirected_Graph]Verification : %s\n", verify_bfs(bfs_par_2, bfs_seq_2, number_of_nodes));	
	printf("[Undirected_Graph] Total time in nanoseconds = %llu\n\n", time_2.total_time);
	

	/*              Free & Release                  */
	clean_graph(&u_graph);
	clean_graph(&graph);
	free_bfs(&bfs_par);
	free_bfs(&bfs_seq);
	free_bfs(&bfs_par_2);
	free_bfs(&bfs_seq_2);
	free_list(queue);
	free_list(queue_2);

	status = clReleaseContext(cl_data.context);
	status |= clReleaseCommandQueue(cl_data.command_queue);
	status |= clReleaseProgram(cl_data.cl_my_program);
	status |= clReleaseKernel(cl_data.cl_kernel_init);
	status |= clReleaseKernel(cl_data.cl_kernel_bfs);
	CLU_ERRCHECK(status, "Error while releasing ocl data");
}

void bfs_parallel_first_step(OclData* cl_data ,BfsData* bfs, int number_of_nodes, int source, TimeData* time) {
	
	cl_int status;
	cl_event event;
	/*             Mem Objects             */
	cl_mem mem_dist = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)*number_of_nodes, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_dist object");
	
	cl_mem mem_marked = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)*number_of_nodes, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_marked object");

	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_dist, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->dist_to, 0, NULL, &event), "Error writing buffer dist_to");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_marked, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->marked, 0, NULL, &event), "Error writing buffer marked");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 0, sizeof(cl_mem), (void *)&mem_dist), "Error while setting argument: local Memory");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 1, sizeof(cl_mem), (void *)&mem_marked), "Error while setting argument: local Memory");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 2, sizeof(int), (void *)&source), "Error while setting argument: local Memory");

	size_t global_size[1] = { (size_t)number_of_nodes };

	CLU_ERRCHECK(clEnqueueNDRangeKernel(cl_data->command_queue, cl_data->cl_kernel_init, 1, NULL, global_size, NULL, 0, NULL, &event), "Failed to enqueue 2D kernel");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_dist, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->dist_to, 0, NULL, &event), "Error reading buffer dist_to");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_marked, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->marked, 0, NULL, &event), "Error reading buffer marked");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

	/*------------release--------------*/
	status = clReleaseMemObject(mem_dist);
	status |= clReleaseMemObject(mem_marked);
	CLU_ERRCHECK(status, "Error while releasing ocl data");
}

void bfs_parallel(OclData* cl_data, BfsData* bfs, Graph* graph, List** queue,int source, TimeData* time) {
	enqueue(queue, source);	//first element needs to be added to the queue
	bfs_parallel_recursion(cl_data, bfs, graph, queue, time);
}

void bfs_parallel_recursion(OclData* cl_data, BfsData* bfs, Graph* graph , List** queue, TimeData* time) {

	if (!is_empty_l(*queue)) {
		
		cl_int status;
		cl_event event;
		cl_mem mem_dist, mem_marked, mem_edge;
		cl_mem mem_adjacent, mem_queue;
		size_t global_size[1];
		int number_of_nodes, number_of_neigbors;
		int* nodes;
		number_of_nodes = graph->nodes_count;

		mem_dist = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE , sizeof(int)*number_of_nodes, NULL, &status);
		CLU_ERRCHECK(status, "error when creating mem_dist object");
		
		mem_marked = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)*number_of_nodes, NULL, &status);
		CLU_ERRCHECK(status, "error when creating mem_marked object");
		
		mem_edge = clCreateBuffer(cl_data->context, CL_MEM_READ_ONLY, sizeof(int)*number_of_nodes, NULL, &status);
		CLU_ERRCHECK(status, "error when creating mem_edge object");

		CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_dist, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->dist_to, 0, NULL, &event), "Error writing buffer dist");
		time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

		CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_marked, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->marked, 0, NULL, &event), "Error writing buffer marked");
		time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

		nodes = (int*)malloc(sizeof(int)*number_of_nodes);
		int number_of_elements_in_queue = 0;

		/*add all elements of queue in order to process them in the for loop below*/
		while (!is_empty_l(*queue)) {
			*(nodes + number_of_elements_in_queue) = dequeue(queue);
			number_of_elements_in_queue++;
		}
	
		/* execute kernel for each node in the queue*/
		for (int i = 0; i < number_of_elements_in_queue; i++) {
		
			number_of_neigbors = *((graph->adjacency_list_count_per_node) + *(nodes + i));
			mem_adjacent = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)*number_of_neigbors,NULL, &status);
			CLU_ERRCHECK(status, "error when creating mem_adjacent object");

			CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_adjacent, CL_TRUE, 0, sizeof(int)*number_of_neigbors, *(graph->adjacency_list + *(nodes + i)), 0, NULL, &event), "Error writing buffer adjacent");
			time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

			//printf("Processing node: %d which has %d neighbors\n", node, number_of_neigbors);

			global_size[0] = number_of_neigbors;

			int* queue_t = (int*)malloc(sizeof(int)*number_of_neigbors);
			memset(queue_t, -1, sizeof(int)*number_of_neigbors);
			mem_queue = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)*number_of_neigbors, NULL, &status);
			CLU_ERRCHECK(status, "error when creating mem_queue object");

			CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_queue, CL_TRUE, 0, sizeof(int)*number_of_neigbors, queue_t, 0, NULL, &event), "Error writing buffer mem_queue");
			time->total_time += retrieveTotalExecutionTimeFromEvent(&event);


			/*
			for (int i = 0; i < number_of_neigbors; i++) {
				printf("[Before]queue[%d]: %d\n", i, queue_t[i]);
			}
			*/

			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 0, sizeof(cl_mem), (void *)&mem_dist), "Error while setting argument: mem_dist");
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 1, sizeof(cl_mem), (void *)&mem_edge), "Error while setting argument: mem_edge");
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 2, sizeof(cl_mem), (void *)&mem_marked), "Error while setting argument: mem_marked");
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 3, sizeof(cl_mem), (void *)&mem_adjacent), "Error while setting argument: mem_adjacent");
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 4, sizeof(int), (void *) &(*(nodes+i))), "Error while setting argument: nodes");
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 5, sizeof(cl_mem), (void *)&mem_queue), "Error while setting argument: mem_queue");

			
			CLU_ERRCHECK(clEnqueueNDRangeKernel(cl_data->command_queue, cl_data->cl_kernel_bfs, 1, NULL, global_size, NULL, 0, NULL, &event), "Failed to enqueue kernel");
			time->total_time += retrieveTotalExecutionTimeFromEvent(&event);
			
			CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_queue, CL_TRUE, 0, sizeof(int)*number_of_neigbors, queue_t, 0, NULL, &event), "Error reading buffer queue");
			time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

			/* add new unmarked nodes to the queue */
			for (int i = 0; i < number_of_neigbors; i++) {
				if ((*(queue_t+i))!=(-1)) {	
					enqueue(queue, *(queue_t + i));
				}
			}
			
			free(queue_t);
		}

		CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_dist, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->dist_to, 0, NULL, &event), "Error reading buffer mem_dist");
		time->total_time += retrieveTotalExecutionTimeFromEvent(&event);
		CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_edge, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->edge_to, 0, NULL, &event), "Error reading buffer mem_edge");
		time->total_time += retrieveTotalExecutionTimeFromEvent(&event);
		CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_marked, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->marked, 0, NULL, &event), "Error reading buffer mem_marked");
		time->total_time += retrieveTotalExecutionTimeFromEvent(&event);
		/*
		for (int i = 0; i < number_of_nodes; i++) {
			if (i == 0) printf("Marked: [%s-", bfs->marked[i] == true ? "true" : "false");
			else if(i==number_of_nodes-1) printf("%s]\n\n", bfs->marked[i] == true ? "true" : "false");
			else printf("%s-",bfs->marked[i] == true ? "true" : "false");
		}
		*/
	
		/* free pointers and release mem objects*/
		free(nodes);
		status = clReleaseMemObject(mem_dist);
		status |= clReleaseMemObject(mem_edge);
		status |= clReleaseMemObject(mem_marked);
		status |= clReleaseMemObject(mem_adjacent);
		status |= clReleaseMemObject(mem_queue);
		CLU_ERRCHECK(status, "error cleaning mem_objects");

		/*recursive call*/
		bfs_parallel_recursion(cl_data, bfs, graph, queue, time);
	}
}

const char* test_solution(BfsData bfs, BfsData bfs_seq, int look_up_range) {
	int look_up;
	for (int i = 0; i < 100; i++) {
		look_up = rand() % look_up_range;
		bool one = has_path_to(bfs, look_up);
		bool two = has_path_to(bfs_seq, look_up);
		/*
		if (has_path_to(bfs, look_up) == true) {
		printf("[Parallel] Is there a path from %d to %d: %s \n", source, look_up, has_path_to(bfs, look_up) == true ? "true" : "false");
		printf("[Parallel] The distance from %d to %d is: %d\n", source, look_up, distance_to(bfs,look_up));
		}
		if (has_path_to(bfs_seq, look_up) == true) {
		printf("[Sequential] Is there a path from %d to %d: %s \n", source, look_up, has_path_to(bfs_seq, look_up) == true ? "true" : "false");
		printf("[Sequential] The distance from %d to %d is: %d\n\n", source, look_up, distance_to(bfs_seq, look_up));
		}
		*/
		if ((one != two) || (distance_to(bfs, look_up) != distance_to(bfs_seq, look_up))) {
			return "Error";
		}
	}
	return "OK";
}
