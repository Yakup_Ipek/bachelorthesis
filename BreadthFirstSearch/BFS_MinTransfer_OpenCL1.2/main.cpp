/*
Author: Yakup Ipek
Breadth first search in parallel OCL 1.2 Minimum Data Transfer
*/


#include <time.h>
#include <assert.h>
#include "myOpenCL_Lib.h"
#include "matrixLib.h"
#include "myGraph.h"
#include "BreadthFirstSearchLib.h"
#include "GraphReaderB.h"

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

#define NODES 500
#define KERNEL_FILE_NAME "./kernel.cl"

typedef struct {
	cl_context context;
	cl_command_queue command_queue;
	cl_device_id device_id;
	cl_program cl_my_program;
	cl_kernel cl_kernel_init, cl_kernel_bfs;
} OclData;

typedef struct {
	cl_ulong total_time = 0;
} TimeData;

typedef struct {
	int source_node;
	int size_neighbours;
	int* starting;
	int* neighbours;
	int* mark_next;
	int number_of_nodes;
	BfsData bfs_parallel;
}KernelArguments;

void bfs_parallel_first_step(OclData* cl_data, KernelArguments* arguments, cl_ulong* total_time);
void bfs_parallel_second_step(OclData* cl_data, KernelArguments* arguments, cl_ulong* total_time);
const char* test_solution(BfsData bfs, BfsData bfs_seq, int look_up_range);

int main(void) {
	srand(time(NULL));
	Graph graph;
	OclData cl_data;
	BfsData bfs_seq;
	KernelArguments kernel_arguments;
	cl_ulong total_time = 0;
	cl_int status;
	cl_event event;

	/*----------------initialize OpenCL device--------------*/
	//cl_data.device_id = clInit(&cl_data.context, &cl_data.command_queue);
	cl_data.device_id = clInitByParam(&cl_data.context, &cl_data.command_queue, CL_PLATFORM_ID, CL_DEVICE_ID);

	/*________________Generate graph randomly in runtime__________________*/
	
	int number_of_nodes = NODES;
	int minimum_neigbors = 1;
	int maximum_neigbors = 10;
	int source = rand() % NODES;
	printf("\nSource Node: %d\n\n",source);
	
	/*-------------generate graphs-----------------------*/
	generate_random_directed_graph(&graph, number_of_nodes, maximum_neigbors, minimum_neigbors);
	//print_adjacency_list(graph);

	/*_____________________Read graph from file_____________________*/
	/*
	read_graph_from_file(&graph, "../../Library/GraphWriterReader/GeneratedGraphs/[NORMAL]Nodes10K_Neighbours5K.txt");
	int number_of_nodes = graph.nodes_count;
	int source = 5;
	*/
	/*____________________________________________________________________*/

	/*-----------initialize kernel arguments---------------------*/
	kernel_arguments.source_node = source;
	kernel_arguments.starting = (int*)malloc(sizeof(int)*number_of_nodes);	//starting indices for neighbours
	kernel_arguments.mark_next = (int*)calloc(number_of_nodes, sizeof(int));
	kernel_arguments.size_neighbours = size_to_wrap_up(graph);
	kernel_arguments.neighbours = (int*)malloc(sizeof(int)*kernel_arguments.size_neighbours);
	kernel_arguments.number_of_nodes = number_of_nodes;
	/*----------wrap up neighbors - in order to transfer them at once to the kernel----------*/
	wrap_neighbours_up(graph, &kernel_arguments.neighbours);
	determine_starting_points(graph, &kernel_arguments.starting);

	/*-------------Bfs initializing--------------*/
	initialize_bfs(&kernel_arguments.bfs_parallel, number_of_nodes);
	initialize_bfs(&bfs_seq, number_of_nodes);

	/*       Build program and define kernel       */
	char option[128];
	sprintf(option, "-DINT_MAX=%i -DBOUNDARY=%i", INT_MAX, kernel_arguments.size_neighbours);
	myCLBuildProgram(cl_data.context, &cl_data.cl_my_program, cl_data.device_id, KERNEL_FILE_NAME, option);
	cl_data.cl_kernel_init = clCreateKernel(cl_data.cl_my_program, "init_kernel", &status);
	CLU_ERRCHECK(status, "Error creating init_kernel");
	cl_data.cl_kernel_bfs = clCreateKernel(cl_data.cl_my_program, "bfs_kernel", &status);
	CLU_ERRCHECK(status, "Error creating bfs_kernel");


	/*-------------First Kernel Function--------------*/

	bfs_parallel_first_step(&cl_data, &kernel_arguments, &total_time);
	/*
	printMatrix(kernel_arguments.bfs_parallel.dist_to, number_of_nodes, "int", 5, "Distance");
	printMatrix(kernel_arguments.bfs_parallel.marked, number_of_nodes, "int", 5, "Marked");
	*/
	/*-------------Second Kernel Function--------------*/
	printf("[BreadthFirstSearch]Started parallel computation ...\n\n");

	bfs_parallel_second_step(&cl_data, &kernel_arguments, &total_time);

	printf("[BreadthFirstSearch]Finished parallel computation ...\n\n");
	printf("___________________________________________________\n\n");
	/*----------------bfs sequentially------------*/
	
	printf("[BreadthFirstSearch]Started sequential computation ...\n\n");

	bfs_seqential(graph, &bfs_seq, source);

	printf("[BreadthFirstSearch]Finished sequential computation ...\n\n");
	
	printf("------------------------------------------------\n");

	printf("Verification : %s\n", verify_bfs(kernel_arguments.bfs_parallel, bfs_seq, number_of_nodes));
	printf("Total time in nanoseconds = %llu\n\n",total_time);
	/*
	printMatrix(kernel_arguments.bfs_parallel.dist_to, number_of_nodes, "int", 5, "[PAR] Distance");
	printMatrix(kernel_arguments.bfs_parallel.marked, number_of_nodes, "int", 5, "[PAR] Marked");

	printMatrix(bfs_seq.dist_to, number_of_nodes, "int", 5, "[SEQ] Distance");
	printMatrix(bfs_seq.marked, number_of_nodes, "int", 5, "[SEQ] Marked");
	*/

	/*              Free & Release                  */

	clean_graph(&graph);
	free_bfs(&kernel_arguments.bfs_parallel);
	free_bfs(&bfs_seq);
	free(kernel_arguments.starting);
	free(kernel_arguments.neighbours);
	free(kernel_arguments.mark_next);

	status = clReleaseContext(cl_data.context);
	status |= clReleaseCommandQueue(cl_data.command_queue);
	status |= clReleaseProgram(cl_data.cl_my_program);
	status |= clReleaseKernel(cl_data.cl_kernel_init);
	status |= clReleaseKernel(cl_data.cl_kernel_bfs);
	CLU_ERRCHECK(status, "Error while releasing ocl data");

	return EXIT_SUCCESS;
}


const char* test_solution(BfsData bfs, BfsData bfs_seq, int look_up_range) {
	int look_up;
	for (int i = 0; i < 100; i++) {
		look_up = rand() % look_up_range;
		bool one = has_path_to(bfs, look_up);
		bool two = has_path_to(bfs_seq, look_up);
		if ((one != two) || (distance_to(bfs, look_up) != distance_to(bfs_seq, look_up))) {
			return "Error";
		}
	}
	return "OK";
}

void bfs_parallel_first_step(OclData* cl_data, KernelArguments* arguments, cl_ulong* total_time) {
	cl_int status;
	cl_event event;

	cl_mem mem_dist = clCreateBuffer(cl_data->context,CL_MEM_WRITE_ONLY,sizeof(int)*arguments->number_of_nodes,NULL,&status);
	CLU_ERRCHECK(status, "Error creating mem_dist");
	cl_mem mem_marked = clCreateBuffer(cl_data->context,CL_MEM_WRITE_ONLY,sizeof(int) * arguments->number_of_nodes,NULL,&status);
	CLU_ERRCHECK(status, "Error creating mem_marked");


	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 0, sizeof(cl_mem), (void *)&mem_dist), "Error while setting argument: mem_dist");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 1, sizeof(cl_mem), (void *)&mem_marked), "Error while setting argument: mem_marked");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 2, sizeof(int), (void *)&arguments->source_node), "Error while setting argument: source node");

	size_t global_size[1] = {(size_t) arguments->number_of_nodes };

	CLU_ERRCHECK(clEnqueueNDRangeKernel(cl_data->command_queue, cl_data->cl_kernel_init, 1, NULL, global_size, NULL, 0, NULL, &event), "Failed to enqueue kernel");
	*total_time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_dist, CL_TRUE, 0, sizeof(int)*arguments->number_of_nodes,arguments->bfs_parallel.dist_to, 0, NULL, &event), "Error reading buffer dist_to");
	*total_time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_marked, CL_TRUE, 0, sizeof(int)*arguments->number_of_nodes, arguments->bfs_parallel.marked, 0, NULL, &event), "Error reading buffer marked");
	*total_time += retrieveTotalExecutionTimeFromEvent(&event);

	/*              Release                  */
	status = clReleaseMemObject(mem_dist);
	status |= clReleaseMemObject(mem_marked);
	CLU_ERRCHECK(status, "Error while releasing ocl data");
}

void bfs_parallel_second_step(OclData* cl_data, KernelArguments* arguments, cl_ulong* total_time) {

	cl_int status;
	cl_event event;
	cl_mem mem_dist, mem_marked, mem_edge, mem_updated, mem_second_marked, mem_start_and_end, mem_adjacent;
	size_t global_size[1] = {(size_t)arguments->number_of_nodes};
	int number_of_nodes = arguments->number_of_nodes;
	bool updated;

	/*Creating Device Buffers, whose size do not vary during runtime*/
	mem_dist = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)*number_of_nodes, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_dist object");
	mem_marked = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)*number_of_nodes, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_marked object");
	mem_edge = clCreateBuffer(cl_data->context, CL_MEM_WRITE_ONLY, sizeof(int)*number_of_nodes, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_edge object");
	mem_updated = clCreateBuffer(cl_data->context, CL_MEM_WRITE_ONLY, sizeof(bool), NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_updated object");
	mem_second_marked = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)*number_of_nodes, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_second_marked object");
	mem_start_and_end = clCreateBuffer(cl_data->context, CL_MEM_READ_ONLY, sizeof(int)*number_of_nodes, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_start_end object");
	mem_adjacent = clCreateBuffer(cl_data->context, CL_MEM_READ_ONLY, sizeof(int)*arguments->size_neighbours, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_adjacent object");

	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_dist, CL_TRUE, 0, sizeof(int)*number_of_nodes, arguments->bfs_parallel.dist_to, 0, NULL, &event), "Error writing buffer dist");
	*total_time += retrieveTotalExecutionTimeFromEvent(&event);
	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_marked, CL_TRUE, 0, sizeof(int)*number_of_nodes, arguments->bfs_parallel.marked, 0, NULL, &event), "Error writing buffer marked");
	*total_time += retrieveTotalExecutionTimeFromEvent(&event);
	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_second_marked, CL_TRUE, 0, sizeof(int)*number_of_nodes, arguments->bfs_parallel.marked, 0, NULL, &event), "Error writing buffer marked");
	*total_time += retrieveTotalExecutionTimeFromEvent(&event);
	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_start_and_end, CL_TRUE, 0, sizeof(int)*number_of_nodes, arguments->starting, 0, NULL, &event), "Error writing buffer starting");
	*total_time += retrieveTotalExecutionTimeFromEvent(&event);
	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_adjacent, CL_TRUE, 0, sizeof(int)*arguments->size_neighbours, arguments->neighbours, 0, NULL, &event), "Error writing buffer adjacent");
	*total_time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 0, sizeof(cl_mem), (void *)&mem_dist), "Error while setting argument: mem_dist");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 1, sizeof(cl_mem), (void *)&mem_edge), "Error while setting argument: mem_edge");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 2, sizeof(cl_mem), (void *)&mem_marked), "Error while setting argument: mem_marked");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 3, sizeof(cl_mem), (void *)&mem_second_marked), "Error while setting argument: mem_second_marked");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 4, sizeof(cl_mem), (void *)&mem_start_and_end), "Error while setting argument: mem_start_and_end");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 5, sizeof(cl_mem), (void *)&mem_adjacent), "Error while setting argument: mem_adjacent");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 6, sizeof(cl_mem), (void *)&mem_updated), "Error while setting argument: updated");

	int change_argument = 1;
	do  {
		updated = false;

		CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_updated, CL_TRUE, 0, sizeof(bool),&updated, 0, NULL, &event), "Error writing variable updated");
		*total_time += retrieveTotalExecutionTimeFromEvent(&event);

		CLU_ERRCHECK(clEnqueueNDRangeKernel(cl_data->command_queue, cl_data->cl_kernel_bfs, 1, NULL, global_size, NULL, 0, NULL, &event), "Failed to enqueue kernel");
		*total_time += retrieveTotalExecutionTimeFromEvent(&event);

		CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_updated, CL_TRUE, 0, sizeof(bool), &updated, 0, NULL, &event), "Error reading variable updated");
		*total_time += retrieveTotalExecutionTimeFromEvent(&event);

		if(change_argument){
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 3, sizeof(cl_mem), (void *)&mem_marked), "Error while setting argument: mem_marked");
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 2, sizeof(cl_mem), (void *)&mem_second_marked), "Error while setting argument: mem_second_marked");
			change_argument = 0;
		}
		else {
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 2, sizeof(cl_mem), (void *)&mem_marked), "Error while setting argument: mem_marked");
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 3, sizeof(cl_mem), (void *)&mem_second_marked), "Error while setting argument: mem_second_marked");
			change_argument = 1;
		}

	} while (updated);

	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_dist, CL_TRUE, 0, sizeof(int)*arguments->number_of_nodes, arguments->bfs_parallel.dist_to, 0, NULL, &event), "Error reading buffer dist_to");
	*total_time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_marked, CL_TRUE, 0, sizeof(int)*arguments->number_of_nodes, arguments->bfs_parallel.marked, 0, NULL, &event), "Error reading buffer marked");
	*total_time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_edge, CL_TRUE, 0, sizeof(int)*arguments->number_of_nodes, arguments->bfs_parallel.edge_to, 0, NULL, &event), "Error reading buffer edge_to");
	*total_time += retrieveTotalExecutionTimeFromEvent(&event);

	/*              Release                  */
	status = clReleaseMemObject(mem_dist);
	status |= clReleaseMemObject(mem_marked);
	status |= clReleaseMemObject(mem_edge);
	status |= clReleaseMemObject(mem_second_marked);
	status |= clReleaseMemObject(mem_adjacent);
	status |= clReleaseMemObject(mem_start_and_end);
	status |= clReleaseMemObject(mem_updated);
	CLU_ERRCHECK(status, "Error while releasing ocl data");
}
