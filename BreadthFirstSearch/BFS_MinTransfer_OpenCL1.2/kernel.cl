/*
Author: Yakup Ipek
Breadth first search kernel OCL 1.2
*/

__kernel void init_kernel(__global int* dist_to, __global int* marked, int source){
	
	int gid=get_global_id(0);

	if(source==gid){
		marked[gid]=1;
		dist_to[gid]=0;
	}else{
		marked[gid]=0;
		dist_to[gid]=INT_MAX;
	}
}

__kernel void bfs_kernel(__global int* dist_to, __global int* edge_to, __global int* marked, __global int* second_marked, __global int* start_and_end, __global int* adjacent,__global bool* updated){
	
	int gid = get_global_id(0);
	int start,end;

	if(marked[gid] == 1){
		marked[gid] = 2;
		second_marked[gid] = 2;

		if(gid < get_global_size(0)-1){
			start = start_and_end[gid];
			end = start_and_end[gid+1];
		}else{
			start = start_and_end[gid];
			end = BOUNDARY;
		}

		for(int i = start; i<end; i++){
			int node = adjacent[i];
			//printf("Node: %d, I process my neighbour: %d\n",gid,node);
			if(atomic_cmpxchg(marked + node,0,2) == 0){	//zero means not marked
				second_marked[node] = 1;
				edge_to[node] = gid;
				dist_to[node] = dist_to[gid]+1;
				updated[0]=true;
				//printf("Node: %d has Dist: %d, I process my neighbour: %d, which has now Dist: %d\n",gid,dist_to[gid],node,dist_to[node]);
			}
		}
	}
}