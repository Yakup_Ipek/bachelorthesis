/*
Author: Yakup Ipek
Breadth first search in parallel for OpenCL 2.0
*/
#include "myOpenCL_Lib.h"
#include "BreadthFirstSearchLib.h"
#include "MyTime.h"
#include "GraphReaderB.h"

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

#define NODES 500
#define KERNEL_FILE_NAME "./kernel.cl"

typedef struct {
	cl_context context;
	cl_command_queue command_queue, device_queue;
	cl_device_id device_id;
	cl_program cl_my_program;
	cl_kernel cl_kernel_init;
} OclData;

typedef struct {
	cl_ulong total_time = 0;
} TimeData;


const char* test_solution(BfsData bfs, BfsData bfs_seq, int look_up_range);
void bfs_parallel(OclData* cl_data, Graph* graph, BfsData* bfs, int number_of_nodes, int source, TimeData* time);

int main(void) {

	OclData cl_data;
	Graph graph;
	Graph u_graph;
	BfsData bfs_par, bfs_par_2;
	BfsData bfs_seq, bfs_seq_2;
	TimeData time, time_2;
	cl_int status;
	cl_event event;

	/*----------------initialize OpenCL device--------------*/
	//cl_data.device_id = clInitWithDeviceQueue(&cl_data.context, &cl_data.command_queue,&cl_data.device_queue);
	cl_data.device_id = clInitWithDeviceQueueByParam(&cl_data.context, &cl_data.command_queue,&cl_data.device_queue, CL_PLATFORM_ID, CL_DEVICE_ID);

	/*________________Generate graph randomly in runtime__________________*/
	
	int number_of_nodes = NODES;
	int minimum_neigbors = 2;
	int maximum_neigbors = NODES / 4;
	int source = rand() % NODES;
	
	/*-------------generate graphs-----------------------*/
	
	generate_random_undirected_graph(&u_graph, number_of_nodes, minimum_neigbors);
	generate_random_directed_graph(&graph, number_of_nodes, maximum_neigbors, minimum_neigbors);
	
	/*____________________________________________________________________*/

	/*_____________________Read graph from file_____________________*/
	/*
	read_graph_from_file(&graph, "../../Library/GraphWriterReader/GeneratedGraphs/[NORMAL]Nodes10K_Neighbours5K.txt");
	int number_of_nodes = graph.nodes_count;
	int source = 5;
	*/
	/*____________________________________________________________________*/
	
	/*-------------Bfs initializing--------------*/
	initialize_bfs(&bfs_par, number_of_nodes);
	initialize_bfs(&bfs_par_2, number_of_nodes);
	initialize_bfs(&bfs_seq, number_of_nodes);
	initialize_bfs(&bfs_seq_2, number_of_nodes);

	/*-------------Bfs sequential --------------*/
	bfs_seqential(graph, &bfs_seq, source);
	bfs_seqential(u_graph, &bfs_seq_2, source);

	/*       Build program and define kernel       */
	char option[128];
	sprintf(option, "-DINT_MAX=%i", INT_MAX);
	myCLBuildProgram(cl_data.context, &cl_data.cl_my_program, cl_data.device_id, KERNEL_FILE_NAME, option);
	cl_data.cl_kernel_init = clCreateKernel(cl_data.cl_my_program, "init_kernel", &status);
	CLU_ERRCHECK(status, "Error creating init_kernel");

	/*++++++++++++++++++++++Directed Graph++++++++++++++++++++++++++*/
	/*------------- Kernel Function--------------*/
	bfs_parallel(&cl_data, &graph, &bfs_par, number_of_nodes, source, &time);
	/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


	/*++++++++++++++++++++++Undirected Graph++++++++++++++++++++++++++*/
	/*------------- Kernel Function--------------*/
	bfs_parallel(&cl_data, &u_graph, &bfs_par_2, number_of_nodes, source, &time_2);
	/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

	printf("[Directed_Graph]Verification : %s\n", verify_bfs(bfs_par, bfs_seq, number_of_nodes));
	printf("[Directed_Graph] Total time in nanoseconds = %llu\n\n", time.total_time);

	
	printf("[Undirected_Graph]Verification : %s\n", verify_bfs(bfs_par_2, bfs_seq_2, number_of_nodes));
	printf("[Undirected_Graph] Total time in nanoseconds = %llu\n\n", time_2.total_time);
	

	/*              Free & Release                  */
	clean_graph(&graph);
	clean_graph(&u_graph);
	free_bfs(&bfs_par);
	free_bfs(&bfs_seq);
	free_bfs(&bfs_par_2);
	free_bfs(&bfs_seq_2);

	status = clReleaseContext(cl_data.context);
	status |= clReleaseCommandQueue(cl_data.command_queue);
	status |= clReleaseCommandQueue(cl_data.device_queue);
	status |= clReleaseProgram(cl_data.cl_my_program);
	status |= clReleaseKernel(cl_data.cl_kernel_init);
	CLU_ERRCHECK(status, "Error while releasing ocl data");
}


void bfs_parallel(OclData* cl_data, Graph* graph, BfsData* bfs_t, int number_of_nodes, int source, TimeData* time) {
	cl_int status;
	cl_event event;
	MyTimeData time_t;
	/*       Creating and initializing SVMs */
	int* source_kernel = (int*)clSVMAlloc(cl_data->context, CL_MEM_READ_ONLY, sizeof(int), 0);
	*source_kernel = source;
	bool* updated = (bool*)clSVMAlloc(cl_data->context, CL_MEM_READ_WRITE, sizeof(bool), 0);
	*updated = false;
	int* tmp_marked = (int*)clSVMAlloc(cl_data->context, CL_MEM_READ_WRITE,sizeof(int)*number_of_nodes,0);
	memset(tmp_marked, 0, sizeof(int)*number_of_nodes);

	BfsData* bfs = (BfsData*)clSVMAlloc(cl_data->context, CL_MEM_READ_WRITE, sizeof(BfsData), 0);
	bfs->dist_to = (int*)clSVMAlloc(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)*number_of_nodes, 0);
	bfs->edge_to = (int*)clSVMAlloc(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)*number_of_nodes, 0);
	bfs->marked = (int*)clSVMAlloc(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)*number_of_nodes, 0);

	Graph* graph_kernel = (Graph*)clSVMAlloc(cl_data->context, CL_MEM_READ_WRITE, sizeof(Graph), 0);
	graph_kernel->nodes_count = graph->nodes_count;
	graph_kernel->adjacency_list = (int**)clSVMAlloc(cl_data->context, CL_MEM_READ_WRITE, sizeof(int*)*graph_kernel->nodes_count, 0);
	graph_kernel->adjacency_list_count_per_node = (int*)clSVMAlloc(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)*graph_kernel->nodes_count, 0);

	start_timer(&time_t);	//measure time for initializing svm buffers
	for (int i = 0; i < graph->nodes_count; i++) {
		*((graph_kernel->adjacency_list_count_per_node) + i) = *((graph->adjacency_list_count_per_node) + i);
		*((graph_kernel->adjacency_list) + i) = (int*)clSVMAlloc(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)* *((graph_kernel->adjacency_list_count_per_node) + i), 0);
		memcpy(*((graph_kernel->adjacency_list)+i), *((graph->adjacency_list) + i), *((graph->adjacency_list_count_per_node) + i) * sizeof(int));
	}
	end_timer(&time_t);
	time->total_time += time_t.Result;

	/*     Setting Arguments*/
	clSetKernelArgSVMPointer(cl_data->cl_kernel_init, 0, graph_kernel);
	clSetKernelArgSVMPointer(cl_data->cl_kernel_init, 1, bfs);
	clSetKernelArgSVMPointer(cl_data->cl_kernel_init, 2, tmp_marked);
	clSetKernelArgSVMPointer(cl_data->cl_kernel_init, 3, source_kernel);
	clSetKernelArgSVMPointer(cl_data->cl_kernel_init, 4, updated);

	/*     Enqueue Kernel     */
	size_t global_size[1] = {(size_t) number_of_nodes };

	CLU_ERRCHECK(clEnqueueNDRangeKernel(cl_data->command_queue, cl_data->cl_kernel_init, 1, NULL, global_size, NULL, 0, NULL, &event), "Failed to enqueue kernel");
	
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

	/*        Copy Values      */
	memcpy(bfs_t->dist_to, bfs->dist_to, sizeof(int)*number_of_nodes);
	memcpy(bfs_t->edge_to, bfs->edge_to, sizeof(int)*number_of_nodes);
	memcpy(bfs_t->marked, bfs->marked, sizeof(int)*number_of_nodes);

	/*------------release--------------*/
	clSVMFree(cl_data->context, updated);
	clSVMFree(cl_data->context, source_kernel);
	clSVMFree(cl_data->context, tmp_marked);
	clSVMFree(cl_data->context, bfs->dist_to);
	clSVMFree(cl_data->context, bfs->edge_to);
	clSVMFree(cl_data->context, bfs->marked);
	clSVMFree(cl_data->context, bfs);
	clSVMFree(cl_data->context, graph_kernel->adjacency_list_count_per_node);
	for (int i = 0; i < graph_kernel->nodes_count; i++) {
		clSVMFree(cl_data->context, *((graph_kernel->adjacency_list) + i));
	}
	clSVMFree(cl_data->context, graph_kernel->adjacency_list);
	clSVMFree(cl_data->context, graph_kernel);
}

const char* test_solution(BfsData bfs, BfsData bfs_seq, int look_up_range) {
	int look_up;
	for (int i = 0; i < 100; i++) {
		look_up = rand() % look_up_range;
		bool one = has_path_to(bfs, look_up);
		bool two = has_path_to(bfs_seq, look_up);
		if ((one != two) || (distance_to(bfs, look_up) != distance_to(bfs_seq, look_up))) {
			return "Error";
		}
	}
	return "OK";
}
