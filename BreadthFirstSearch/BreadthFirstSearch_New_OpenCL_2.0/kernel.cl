/*
Author: Yakup Ipek
Breadth first search kernel for OpenCL 2.0
*/

typedef struct {
	int* nodes;	//all nodes in graph
	int nodes_count;	//important in order to free memory
	int** adjacency_list; //list of successor node
	int* adjacency_list_count_per_node; //necessary for printing the list
} Graph;

typedef struct {
	int* marked;	//marked[v] is there a path to node v
	int* edge_to; //edge_to[v] parent node of v
	int* dist_to; //dist_to[v] number of edges to  node v
} BfsData;

__kernel void bfs_kernel(__global Graph* graph, __global BfsData* bfs, __global int* tmp_marked,__global bool* updated);
__kernel void determine_if_algorithm_finished(__global Graph* graph, __global BfsData* bfs, __global int* tmp_marked,__global bool* updated);
__kernel void init_kernel(__global Graph* graph, __global BfsData* bfs,__global int* tmp_marked, __global int* source, __global bool* updated);

__kernel void bfs_kernel(__global Graph* graph, __global BfsData* bfs, __global int* tmp_marked,__global bool* updated){

	int idx = get_global_id(0);
	
	if(bfs->marked[idx]==1){
		bfs->marked[idx]++;	//the node should not be processed again
		//printf("I am: %d, will process: %d elements\n",idx, graph->adjacency_list_count_per_node[idx]);
		
		for(int i = 0; i<graph->adjacency_list_count_per_node[idx]; i++){
			int node = (graph->adjacency_list[idx])[i];
			
			//printf("Source: %d\n", idx);
			//the if statement is crucial for determining which node needs to be processes in next function call, but also for setting appropriate values
			//the value in tmp_marked[id] can be greater 1, since the atomic_inc can be executed simultanously by more than one workitem, but only one passes the if
			if(atomic_inc(((tmp_marked)+node)) == 0){	//atomic_inc increments argument by one and returns old value
				//printf("Source: %d, I have so far dist: %d, I process my Neigbor: %d, and mark him: %d\n",idx, bfs->dist_to[idx],node, tmp_marked[node]);	
				bfs->edge_to[node] = idx;
				bfs->dist_to[node] = bfs->dist_to[idx]+1;
				updated[0]=true;
			}
		}
	}

	//since synchronizing over all workitems is not possible, the updated variable needs to be checked in other kernel, hereby synchronizing is managed
	if(idx == 0){	
		ndrange_t child_ndrange = ndrange_1D(get_global_size(0));
		void (^my_block) (void)= ^{determine_if_algorithm_finished(graph, bfs,tmp_marked, updated);};	
		enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,child_ndrange,my_block);
	}
}

/* Determine whether to stop or to continue, new processed nodes in bfs_kernel are marked here for future calls*/
__kernel void determine_if_algorithm_finished(__global Graph* graph, __global BfsData* bfs, __global int* tmp_marked,__global bool* updated){
	
	int id = get_global_id(0);
	
	if(tmp_marked[id]>=1 && bfs->marked[id] ==0){	//mark nodes for next iteration
		//printf("I am Node: %d, I am marked in tmp_marked: %d and in bfs->marked: %d\n",id, tmp_marked[id], bfs->marked[id]);
		bfs->marked[id]=1;
	}
	
	if(id==0){
		if(updated[0]==true){	//was any node processed, if so then enqueue next kernel
			updated[0]=false;
			ndrange_t child_ndrange = ndrange_1D(get_global_size(0));
			void (^my_block) (void)= ^{bfs_kernel(graph, bfs,tmp_marked, updated);};	
			enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,child_ndrange,my_block);
		}
	}
}

/* Initialize graph, source node is the root*/
__kernel void init_kernel(__global Graph* graph, __global BfsData* bfs,__global int* tmp_marked, __global int* source, __global bool* updated){
	
	int gid=get_global_id(0);

	if(source[0]==gid){
		bfs->marked[gid]=1;
		bfs->dist_to[gid]=0;
		tmp_marked[gid]=1;
	}else{
		bfs->marked[gid]=0;
		bfs->dist_to[gid]=INT_MAX;
	}

	if(gid==0){	//enqueue kernel
		ndrange_t child_ndrange = ndrange_1D(get_global_size(0));
		void (^my_block) (void)= ^{bfs_kernel(graph, bfs,tmp_marked, updated);};	
		enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,child_ndrange,my_block);
	}
}