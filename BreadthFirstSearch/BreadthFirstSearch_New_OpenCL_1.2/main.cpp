/*
Author: Yakup Ipek
Breadth first search in parallel
*/


#include <time.h>
#include <assert.h>
#include "myOpenCL_Lib.h"
#include "matrixLib.h"
#include "myGraph.h"
#include "BreadthFirstSearchLib.h"
#include "GraphReaderB.h"

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

#define NODES 500
#define KERNEL_FILE_NAME "./kernel.cl"


typedef struct {
	cl_context context;
	cl_command_queue command_queue;
	cl_device_id device_id;
	cl_program cl_my_program;
	cl_kernel cl_kernel_init, cl_kernel_bfs;
} OclData;

typedef struct {
	cl_ulong total_time=0;
} TimeData;


void bfs_parallel(OclData* cl_data, BfsData* bfs, Graph* graph, int source, TimeData* time);
void bfs_parallel_first_step(OclData* cl_data, BfsData* bfs, int number_of_nodes, int source, TimeData* time);
const char* test_solution(BfsData bfs, BfsData bfs_seq, int look_up_range);
void neighbor_per_node_array_for_kernel(Graph* graph, cl_ulong* array_for_kernel, int* nodes_to_process, cl_ulong number_of_nodes_to_process);
void index_array_for_kernel(Graph* graph, cl_ulong* array_for_kernel, int* nodes_to_process, cl_ulong number_of_nodes_to_process);
cl_ulong size_of_adjacency_for_kernel(Graph* graph, int* nodes_to_process, cl_ulong number_of_nodes_to_process);
void adjacency_for_kernel(Graph* graph, int* array_for_kernel, int* nodes_to_process, cl_ulong number_of_nodes_to_process);

int main(void) {

	Graph graph;
	Graph u_graph;
	OclData cl_data;
	BfsData bfs_par, bfs_par_2;
	BfsData bfs_seq, bfs_seq_2;
	TimeData time, time_2;
	cl_int status;
	cl_event event;
	
	/*----------------initialize OpenCL device--------------*/
	//cl_data.device_id = clInit(&cl_data.context, &cl_data.command_queue);
	cl_data.device_id = clInitByParam(&cl_data.context, &cl_data.command_queue, CL_PLATFORM_ID, CL_DEVICE_ID);
	
	/*________________Generate graph randomly in runtime__________________*/
	
	int number_of_nodes = NODES;
	int minimum_neigbors = 2;
	int maximum_neigbors = NODES / 4;
	int source = rand() % NODES;
	
	/*-------------generate graphs-----------------------*/
	
	generate_random_undirected_graph(&u_graph, number_of_nodes, minimum_neigbors);
	generate_random_directed_graph(&graph, number_of_nodes, maximum_neigbors, minimum_neigbors);
	
	//print_adjacency_list(u_graph);
	//print_adjacency_list(graph);

	/*____________________________________________________________________*/

	/*_____________________Read graph from file_____________________*/
	/*
	read_graph_from_file(&graph, "../../Library/GraphWriterReader/GeneratedGraphs/[NORMAL]Nodes10_Neighbours5.txt");
	int number_of_nodes = graph.nodes_count;
	int source = 5;
	*/
	/*____________________________________________________________________*/

	/*-------------Bfs initializing--------------*/
	initialize_bfs(&bfs_par, number_of_nodes);
	initialize_bfs(&bfs_par_2, number_of_nodes);
	initialize_bfs(&bfs_seq, number_of_nodes);
	initialize_bfs(&bfs_seq_2, number_of_nodes);

	/*-------------Bfs sequential --------------*/
	bfs_seqential(graph, &bfs_seq, source);
	bfs_seqential(u_graph, &bfs_seq_2, source);

	/*       Build program and define kernel       */
	char option[128];
	sprintf(option, "-DINT_MAX=%i", INT_MAX);
	myCLBuildProgram(cl_data.context, &cl_data.cl_my_program, cl_data.device_id, KERNEL_FILE_NAME, option);
	cl_data.cl_kernel_init = clCreateKernel(cl_data.cl_my_program, "init_kernel", &status);
	CLU_ERRCHECK(status, "Error creating init_kernel");
	cl_data.cl_kernel_bfs = clCreateKernel(cl_data.cl_my_program, "bfs_kernel", &status);
	CLU_ERRCHECK(status, "Error creating bfs_kernel");


	/*++++++++++++++++++++++Directed Graph++++++++++++++++++++++++++*/
	/*-------------First Kernel Function--------------*/

	bfs_parallel_first_step(&cl_data, &bfs_par, number_of_nodes, source, &time);

	/*-------------Second Kernel Function--------------*/
	
	bfs_parallel(&cl_data, &bfs_par,&graph, source,&time);
	
	/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

	/*++++++++++++++++++++++++++++Undirected Graph+++++++++++++++++++++++++*/
	/*-------------First Kernel Function--------------*/

	bfs_parallel_first_step(&cl_data, &bfs_par_2, number_of_nodes, source, &time_2);
	
	/*-------------Second Kernel Function--------------*/

	bfs_parallel(&cl_data, &bfs_par_2,&u_graph, source,&time_2);

	/*-------------Some tests--------------*/

	printf("[Directed_Graph]Verification : %s\n", verify_bfs(bfs_par, bfs_seq, number_of_nodes));
	printf("[Directed_Graph] Total time in nanoseconds = %llu\n\n", time.total_time);

	
	printf("[Undirected_Graph]Verification : %s\n", test_solution(bfs_par_2, bfs_seq_2, number_of_nodes));
	printf("[Undirected_Graph] Total time in nanoseconds = %llu\n\n", time_2.total_time);
	

	/*              Free & Release                  */
	clean_graph(&u_graph);
	clean_graph(&graph);
	free_bfs(&bfs_par);
	free_bfs(&bfs_seq);
	free_bfs(&bfs_par_2);
	free_bfs(&bfs_seq_2);

	status = clReleaseContext(cl_data.context);
	status |= clReleaseCommandQueue(cl_data.command_queue);
	status |= clReleaseProgram(cl_data.cl_my_program);
	status |= clReleaseKernel(cl_data.cl_kernel_init);
	status |= clReleaseKernel(cl_data.cl_kernel_bfs);
	CLU_ERRCHECK(status, "Error while releasing ocl data");

}

void bfs_parallel_first_step(OclData* cl_data, BfsData* bfs, int number_of_nodes, int source, TimeData* time) {

	cl_int status;
	cl_event event;
	/*             Mem Objects             */
	cl_mem mem_dist = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE , sizeof(int)*number_of_nodes,NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_dist object");
	cl_mem mem_marked = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE , sizeof(int)*number_of_nodes, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_marked object");

	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_dist, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->dist_to, 0, NULL, &event), "Error reading buffer bfs->dist_to");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_marked, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->dist_to, 0, NULL, &event), "Error writing buffer bfs->marked");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 0, sizeof(cl_mem), (void *)&mem_dist), "Error while setting argument: mem_dist");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 1, sizeof(cl_mem), (void *)&mem_marked), "Error while setting argument: mem_marked");
	CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_init, 2, sizeof(int), (void *)&source), "Error while setting argument: source node");

	size_t global_size[1] = {(size_t) number_of_nodes };

	CLU_ERRCHECK(clEnqueueNDRangeKernel(cl_data->command_queue, cl_data->cl_kernel_init, 1, NULL, global_size, NULL, 0, NULL, &event), "Failed to enqueue 1D kernel");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);
	
	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_dist, CL_FALSE, 0, sizeof(int)*number_of_nodes, bfs->dist_to, 0, NULL, &event), "Error reading buffer dist_to");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_marked, CL_FALSE, 0, sizeof(int)*number_of_nodes, bfs->marked, 0, NULL, &event), "Error reading buffer marked");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

	/*------------release--------------*/
	status = clReleaseMemObject(mem_dist);
	status |= clReleaseMemObject(mem_marked);
	CLU_ERRCHECK(status, "Error while releasing ocl data");
}

void bfs_parallel(OclData* cl_data, BfsData* bfs, Graph* graph, int source, TimeData* time) {

	cl_int status;
	cl_event event;
	cl_mem mem_dist, mem_marked, mem_edge, mem_updated, mem_index, mem_neighbor_per_node, mem_vertices;
	cl_mem mem_adjacent, mem_queue;
	size_t global_size[1];
	bool first_enqueue = true;
	cl_ulong next_time_to_process = 0;
	cl_ulong adjacent_size = 0;
	cl_ulong number_of_nodes, number_of_neigbors;
	cl_ulong number_of_nodes_to_process = 1;
	bool updated = true;
	int* adjacent = NULL;
	int* nodes_to_process = NULL;
	cl_ulong* index = NULL;
	cl_ulong* neigbor_per_node = NULL;
	number_of_nodes = graph->nodes_count;

	/*Creating Device Buffers, whose size do not vary during runtime*/
	mem_dist = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)*number_of_nodes, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_dist object");
	mem_marked = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE , sizeof(int)*number_of_nodes, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_marked object");
	mem_edge = clCreateBuffer(cl_data->context, CL_MEM_WRITE_ONLY, sizeof(int)*number_of_nodes, NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_edge object");
	mem_updated = clCreateBuffer(cl_data->context, CL_MEM_WRITE_ONLY, sizeof(bool),NULL, &status);
	CLU_ERRCHECK(status, "error when creating mem_updated object");

	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_dist, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->dist_to, 0, NULL, &event), "Error writing buffer bfs->dist");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);
	CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_marked, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->marked, 0, NULL, &event), "Error writing buffer bfs->marked");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

	
	/* execute kernel*/
	while (updated) {
			//printf("Process #Nodes: %li\n",number_of_nodes_to_process);
			nodes_to_process = (int*)malloc(sizeof(int)*number_of_nodes_to_process);
			if(first_enqueue){	//the algorithm starts with the root node
				memcpy(nodes_to_process,&source,sizeof(int));
				first_enqueue = false;
			}
			else {
				memcpy(nodes_to_process, adjacent, sizeof(int)*number_of_nodes_to_process);
				free(adjacent);	//reason: all iterations except the first are dependent on previous one, after value is copied the memory is freed
			}

			index = (cl_ulong*)malloc(sizeof(cl_ulong)*number_of_nodes_to_process);
			neigbor_per_node = (cl_ulong*)malloc(sizeof(cl_ulong)*number_of_nodes_to_process);
			adjacent_size = size_of_adjacency_for_kernel(graph, nodes_to_process, number_of_nodes_to_process);
			next_time_to_process = adjacent_size;
			adjacent = (int*)malloc(sizeof(int)*adjacent_size);
			//printf("Number Of Nodes To Process: %llu and Total_Amount_Of_Neighbours->Size:%llu\n",number_of_nodes_to_process,adjacent_size);
			adjacency_for_kernel(graph, adjacent, nodes_to_process, number_of_nodes_to_process);
			index_array_for_kernel(graph, index, nodes_to_process, number_of_nodes_to_process);
			neighbor_per_node_array_for_kernel(graph, neigbor_per_node, nodes_to_process, number_of_nodes_to_process);
		
			mem_index = clCreateBuffer(cl_data->context, CL_MEM_READ_ONLY, sizeof(cl_ulong)*number_of_nodes_to_process, NULL, &status);
			CLU_ERRCHECK(status, "error when creating mem_index object");
			mem_neighbor_per_node = clCreateBuffer(cl_data->context, CL_MEM_READ_ONLY, sizeof(cl_ulong)*number_of_nodes_to_process, NULL, &status);
			CLU_ERRCHECK(status, "error when creating mem_neighbor_per_node object");
			mem_adjacent = clCreateBuffer(cl_data->context, CL_MEM_READ_WRITE, sizeof(int)*adjacent_size, NULL, &status);
			CLU_ERRCHECK(status, "error when creating mem_adjacent object");
			mem_vertices = clCreateBuffer(cl_data->context, CL_MEM_READ_ONLY, sizeof(int)*number_of_nodes_to_process, NULL, &status);
			CLU_ERRCHECK(status, "error when creating mem_vertices object");
			
			/*Writing Values to Device Buffer*/
			updated = false;
			CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_updated, CL_TRUE, 0, sizeof(bool), &updated, 0, NULL, &event), "Error writing buffer updated");
			time->total_time += retrieveTotalExecutionTimeFromEvent(&event);
			CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_index, CL_TRUE, 0, sizeof(cl_ulong)*number_of_nodes_to_process, index, 0, NULL, &event), "Error writing buffer index");
			time->total_time += retrieveTotalExecutionTimeFromEvent(&event);
			CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_adjacent, CL_TRUE, 0, sizeof(int)*adjacent_size, adjacent, 0, NULL, &event), "Error writing buffer adjacent");
			time->total_time += retrieveTotalExecutionTimeFromEvent(&event);
			CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_neighbor_per_node, CL_TRUE, 0, sizeof(cl_ulong)*number_of_nodes_to_process, neigbor_per_node, 0, NULL, &event), "Error writing buffer neighbor_per_node");
			time->total_time += retrieveTotalExecutionTimeFromEvent(&event);
			CLU_ERRCHECK(clEnqueueWriteBuffer(cl_data->command_queue, mem_vertices, CL_TRUE, 0, sizeof(int)*number_of_nodes_to_process, nodes_to_process, 0, NULL, &event), "Error writing buffer vertices");
			time->total_time += retrieveTotalExecutionTimeFromEvent(&event);
			
			/*Setting Arguments*/
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 0, sizeof(cl_mem), (void *)&mem_dist), "Error while setting argument: mem_dist");
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 1, sizeof(cl_mem), (void *)&mem_edge), "Error while setting argument: mem_edge");
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 2, sizeof(cl_mem), (void *)&mem_marked), "Error while setting argument: mem_marked");
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 3, sizeof(cl_mem), (void *)&mem_index), "Error while setting argument: mem_index");
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 4, sizeof(cl_mem), (void *)&mem_adjacent), "Error while setting argument: mem_adjacent");
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 5, sizeof(cl_mem), (void *)&mem_neighbor_per_node), "Error while setting argument: neigbor per node");
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 6, sizeof(cl_mem), (void *)&mem_updated), "Error while setting argument: parent node");
			CLU_ERRCHECK(clSetKernelArg(cl_data->cl_kernel_bfs, 7, sizeof(cl_mem), (void *)&mem_vertices), "Error while setting argument: parent node");

			/*Kernel*/
			global_size[0] = number_of_nodes_to_process;
			//printf("GlobalSize: %d\n",global_size[0]);
			CLU_ERRCHECK(clEnqueueNDRangeKernel(cl_data->command_queue, cl_data->cl_kernel_bfs, 1, NULL, global_size, NULL, 0, NULL, &event), "Failed to enqueue 2D kernel");
			time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

			CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_updated, CL_TRUE, 0, sizeof(bool), &updated, 0, NULL, &event), "Error reading buffer dist");
			time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

			number_of_nodes_to_process = next_time_to_process;

			/*free*/
			free(nodes_to_process);
			free(index);
			free(neigbor_per_node);
	}
	
	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_dist, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->dist_to, 0, NULL, &event), "Error reading buffer dist");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);
	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_edge, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->edge_to, 0, NULL, &event), "Error reading buffer edge");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);
	CLU_ERRCHECK(clEnqueueReadBuffer(cl_data->command_queue, mem_marked, CL_TRUE, 0, sizeof(int)*number_of_nodes, bfs->marked, 0, NULL, &event), "Error reading buffer marked");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);


	/* free pointers and release mem objects*/
	
	free(adjacent);	//needs to be done due to last iteration, which does not deallocate reserved space
	status = clReleaseMemObject(mem_dist);
	status |= clReleaseMemObject(mem_edge);
	status |= clReleaseMemObject(mem_marked);
	status |= clReleaseMemObject(mem_adjacent);
	status |= clReleaseMemObject(mem_vertices);
	status |= clReleaseMemObject(mem_neighbor_per_node);
	status |= clReleaseMemObject(mem_updated);
	CLU_ERRCHECK(status, "error cleaning mem_objects");
}


void neighbor_per_node_array_for_kernel(Graph* graph, cl_ulong* array_for_kernel, int* nodes_to_process, cl_ulong number_of_nodes_to_process) {
	
	for (cl_ulong i = 0; i < number_of_nodes_to_process; i++) {
			int node = *(nodes_to_process + i);
			*(array_for_kernel + i) = *((graph->adjacency_list_count_per_node) + node);
	}
}

void index_array_for_kernel(Graph* graph, cl_ulong* array_for_kernel,int* nodes_to_process, cl_ulong number_of_nodes_to_process) {
	long index_count=0;
	for (cl_ulong i = 0; i < number_of_nodes_to_process; i++) {
		
		if (i == 0) {
			*(array_for_kernel + i) = 0;
		}
		else {
			int node = *(nodes_to_process + (i-1));
			index_count += *((graph->adjacency_list_count_per_node) + node);
			*(array_for_kernel + i) = index_count;
		}
	}
}
/* used to allocate correct size of memory for neighbour nodes*/
cl_ulong size_of_adjacency_for_kernel(Graph* graph, int* nodes_to_process, cl_ulong number_of_nodes_to_process) {
	cl_ulong number_of_total_neigbours = 0;
	for (cl_ulong i = 0; i < number_of_nodes_to_process; i++) {
		int node = *(nodes_to_process + i);
		number_of_total_neigbours += *((graph->adjacency_list_count_per_node) + node);
	}
	return number_of_total_neigbours;
}
/* safe the adjacency nodes into argument which will be passed to the kernel*/
void adjacency_for_kernel(Graph* graph,int* array_for_kernel,int* nodes_to_process, cl_ulong number_of_nodes_to_process){

	cl_ulong index = 0;
	for (cl_ulong i = 0; i < number_of_nodes_to_process; i++) {
		int node = *(nodes_to_process + i);
		for (int j = 0; j < *((graph->adjacency_list_count_per_node) + node); j++) {
			*(array_for_kernel + index) = *((*((graph->adjacency_list) + node)) + j);
			index++;
		}
	}

}

const char* test_solution(BfsData bfs, BfsData bfs_seq, int look_up_range) {
	int look_up;
	for (int i = 0; i < 100; i++) {
		look_up = rand() % look_up_range;
		bool one = has_path_to(bfs, look_up);
		bool two = has_path_to(bfs_seq, look_up);
		if ((one != two) || (distance_to(bfs, look_up) != distance_to(bfs_seq, look_up))) {
			return "Error";
		}
	}
	return "OK";
}
