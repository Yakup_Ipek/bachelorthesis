/*
Author: Yakup Ipek
Breadth first search kernel
*/

__kernel void init_kernel(__global int* dist_to, __global int* marked, int source){
	
	int gid=get_global_id(0);

	if(source==gid){
		marked[gid]=1;
		dist_to[gid]=0;
	}else{
		marked[gid]=0;
		dist_to[gid]=INT_MAX;
	}
}

__kernel void bfs_kernel(__global int* dist_to, __global int* edge_to, __global int* marked, __global long* index, __global int* adjacent, __global long* neigbor_per_node, __global bool* updated, __global int* vertices){
	//printf("Get_global_size: %d\n",get_global_size(0));
	int idx = get_global_id(0);
	ulong start = index[idx];
	ulong end = start+neigbor_per_node[idx];
	int vertex = vertices[idx]; 
	//printf("I Am Node: %d\n", vertex);
	//printf("Start: %d, End: %d\n",start,end);
	//process condition by simultaneously marking the node
	for(ulong i = start; i<end; i++){
		int node = adjacent[i];
		//printf("Node: %d, I process my neighbour: %d\n",vertex,node);
		if(atomic_add(marked + node,1)==0){	//zero means not marked, greater than zero means already marked, atomic_add returns old value
			//printf("I am Node: %d\t Parent Node: %d\n",node, vertex);
			edge_to[node] = vertex;
			dist_to[node] = dist_to[vertex]+1;
			updated[0]=true;
		}
	}

}