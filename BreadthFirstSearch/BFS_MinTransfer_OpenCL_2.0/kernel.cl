/*
Author: Yakup Ipek
Breadth first search in parallel OCL 2.0 Minimum Data Transfer
*/

__kernel void bfs_kernel(volatile __global int* dist_to, __global int* marked,__global int* edge_to,__global int* second_marked,
 __global int* start_and_end, __global int* adjacent,__global bool* updated);


__kernel void bfs_last_step(volatile __global int* dist_to, __global int* marked,__global int* edge_to,__global int* second_marked,
__global int* start_and_end, __global int* adjacent,__global bool* updated){
	
	int gid = get_global_id(0);
	
	if(gid == 0){
		
		if(updated[0] == true){	//enqueue kernel
			updated[0] = false;
			ndrange_t ndrange = ndrange_1D(get_global_size(0));
			void(^my_block)(void) = ^{bfs_kernel(dist_to,second_marked,edge_to,marked,start_and_end,adjacent,updated);};			
			enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,ndrange,my_block);	
		}
	}
}

__kernel void bfs_kernel(volatile __global int* dist_to, __global int* marked,__global int* edge_to,__global int* second_marked,
 __global int* start_and_end, __global int* adjacent,__global bool* updated){

	int gid = get_global_id(0);
	int start, end;
	
	if(marked[gid] == 1){

		marked[gid] = 2;
		second_marked[gid] = 2;

		if(gid < get_global_size(0) - 1){
			start = start_and_end[gid];
			end = start_and_end[gid+1];
		}else{ //no further neighbors available to process	
			start = start_and_end[gid];
			end = BOUNDARY;
		}

		for(int i = start; i<end; i++){
			int node = adjacent[i];
			if(atomic_cmpxchg(marked + node,0,2) == 0){	//zero means not marked
				second_marked[node] = 1;
				edge_to[node] = gid;
				dist_to[node] = dist_to[gid]+1;
				updated[0]=true;
				//printf("Node: %d has Dist: %d, I process my neighbour: %d, which has now Dist: %d\n",gid,dist_to[gid],node,dist_to[node]);
			}
		}
	}

	if(gid == 0){
		ndrange_t ndrange = ndrange_1D(get_global_size(0));
		void(^my_block)(void) = ^{bfs_last_step(dist_to,marked,edge_to,second_marked,start_and_end,adjacent,updated);};			
		enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,ndrange,my_block);
	}
}


__kernel void init_kernel(__global int* dist_to, __global int* edge_to,__global int* marked,__global int* second_marked,int source,
 __global int* start_and_end, __global int* adjacent, __global bool* updated){
	
	int gid=get_global_id(0);

	if(source==gid){
		marked[gid]=1;
		second_marked[gid] = 1;
		dist_to[gid]=0;
	}else{
		marked[gid]=0;
		second_marked[gid] = 0;
		dist_to[gid]=INT_MAX;
	}
	
	if(gid == 0){
		ndrange_t ndrange = ndrange_1D(get_global_size(0));
		void(^my_block)(void) = ^{bfs_kernel(dist_to,marked,edge_to,second_marked,start_and_end,adjacent,updated);};			
		enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,ndrange,my_block);
	}
}