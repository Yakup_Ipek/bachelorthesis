Important: Some OCL 2.0 applications operate on Fine-Grained Shared Virtual Memory
Among the SVM-Types my machine supports FINE-GRAINED-BUFFER with Atomics, so the host directly writes to the svm-buffers without mapping or unmapping it!!!!
For those hardware which solely support COARSE-GRAINED-BUFFER a mapping and unmapping of the buffer is required on the host side!!!
For more details see: https://software.intel.com/en-us/articles/opencl-20-shared-virtual-memory-overview