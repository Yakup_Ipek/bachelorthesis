#include "matrixLib.h"

void fillMatrixRandomly(void* matrix, int size, const char* type) {
	srand(time(NULL));
	switch (typeParser(type)) {
	case 0: //int
		for (int i = 0; i < size; i++) {
			*((int*)matrix + i) = rand() % RANDOM;
		}
		return;
	case 1: //float
		for (int i = 0; i < size; i++) {
			*((float*)matrix + i) = rand() % RANDOM;
		}
		return;
	case 2: //double
		for (int i = 0; i < size; i++) {
			*((double*)matrix + i) = rand() % RANDOM;
		}
		return;
	case 3: //char
		for (int i = 0; i < size; i++) {
			*((char*)matrix + i) = (char)(rand() % 256);
		}
		return;
	default:
		fprintf(stderr, "Illegal type for filling matrix");
		exit(-1);
	}
}

void printMatrix(void* matrix, int length,const char* type, int breakAfterNElements, const char* label) {
	printf("\n---------------Print_Matrix: %s---------------\n", label);
	switch (typeParser(type)) {
	case 0: //int
	{
		int* pcopyM = (int*)matrix;
		for (int i = 0; i < length; i++) {
			printf("%d\t", *(pcopyM + i));
			if (((i + 1) % breakAfterNElements) == 0) printf("\n");
		}
		break;
	}
	case 1: //float
	{
		float* fcopyM = (float*)matrix;
		for (int i = 0; i < length; i++) {
			printf("%0.10f\t", *(fcopyM + i));
			if (((i + 1) % breakAfterNElements) == 0) printf("\n");
		}
		break;
	}
	case 2: //double
	{
		double* dcopyM = (double*)matrix;
		for (int i = 0; i < length; i++) {
			printf("%0.15lf\t", *(dcopyM + i));
			if (((i + 1) % breakAfterNElements) == 0) printf("\n");
		}
		break;
	}
	case 3: //char
	{
		char* copyM = (char*)matrix;
		for (int i = 0; i < length; i++) {
			printf("%c\t", *(copyM + i));
			if (((i + 1) % breakAfterNElements) == 0) printf("\n");
		}
		break;
	}
	default: //illegal type
		fprintf(stderr, "Illegal type argument for printing matrix");
		exit(-1);
	}
	printf("\n-----------------------------------------\n");
}

void verifyTwoMatrices(void* matrix_one, void* matrix_two, int size, const char* type) {
	for (int i = 0; i < size; i++) {
		switch (typeParser(type)) {
		case 0: //int
			if (*((int*)matrix_one + i) != *((int*)matrix_two + i)) {
				printf("Verification: Error(Matrices are not the same)\n");
				return;
			}
			break;
		case 1: //float
			if (*((float*)matrix_one + i) != *((float*)matrix_two + i)) {
				printf("Verification: Error(Matrices are not the same)\n");
				return;
			}
			break;
		case 2: //double
			if (*((double*)matrix_one + i) != *((double*)matrix_two + i)) {
				printf("Verification: Error(Matrices are not the same)\n");
				return;
			}
			break;
		case 3: //char
			if (*((char*)matrix_one + i) != *((char*)matrix_two + i)) {
				printf("Verification: Error(Matrices are not the same)\n");
				return;
			}
			break;
		default: //illegal type
			fprintf(stderr, "Illegal type argument for printing matrix");
			exit(-1);
		}
	}
	printf("Verification: OK\n");
}

void matrixMult(void* A, int rowA, int colA, void* B, int rowB, int colB, void* C, const char* type) {
	int counter = 0;
	printf("----------------Start - Matrix Multiplication Sequential Computation---------------\n");
	for (int i = 0; i < rowA; i++) {
		for (int j = 0; j < colB; j++) {
			printf("Progress: ");
			switch (typeParser(type)) {
			case 0: ((int*)C)[colB*i + j] = 0; break;
			case 1: ((float*)C)[colB*i + j] = 0.0; break;
			case 2: ((double*)C)[colB*i + j] = 0.0; break;
			default:
				fprintf(stderr, "Illegal type argument for printing matrix");
				exit(-1);
			}
			for (int k = 0; k < colA; k++) {
				switch (typeParser(type)) {
				case 0: ((int*)C)[colB*i + j] += ((int*)A)[i*colA + k] * ((int*)B)[k*colB + j]; break;
				case 1: ((float*)C)[colB*i + j] += ((float*)A)[i*colA + k] * ((float*)B)[k*colB + j]; break;
				case 2: ((double*)C)[colB*i + j] += ((double*)A)[i*colA + k] * ((double*)B)[k*colB + j]; break;
				}
			}
			printf("\t%0.0f %%\r", (float)++counter / (float)(rowA*colB) * 100);
		}
	}
	printf("----------------Finished - Matrix Multiplication Sequential Computation---------------\n");
	printf("\n");
}

void sortMatrix(void* matrix, int size, const char* type) {
	//bubblesort
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size - 1; j++) {
			switch (typeParser(type)) {
			case 0: //int
				if (*((int*)matrix + j)>*((int*)matrix + j + 1)) {
					int tmp = *((int*)matrix + j);
					*((int*)matrix + j) = *((int*)matrix + j + 1);
					*((int*)matrix + j + 1) = tmp;
				}
				continue;
			case 1: //float
				if (*((float*)matrix + j)>*((float*)matrix + j + 1)) {
					float tmp = *((float*)matrix + j);
					*((float*)matrix + j) = *((float*)matrix + j + 1);
					*((float*)matrix + j + 1) = tmp;
				}
				continue;
			case 2: //double
				if (*((double*)matrix + j)>*((double*)matrix + j + 1)) {
					double tmp = *((double*)matrix + j);
					*((double*)matrix + j) = *((double*)matrix + j + 1);
					*((double*)matrix + j + 1) = tmp;
				}
				continue;
			case 3: //char
				if (*((char*)matrix + j)>*((char*)matrix + j + 1)) {
					char tmp = *((char*)matrix + j);
					*((char*)matrix + j) = *((char*)matrix + j + 1);
					*((char*)matrix + j + 1) = tmp;
				}
				continue;
			default: //illegal type
				fprintf(stderr, "Illegal type argument for printing matrix");
				exit(-1);
			}
		}
	}
}

int typeParser(const char* type) {

	char delimiter[] = "\0";
	char intType[] = "int";
	char floatType[] = "float";
	char doubleType[] = "double";
	char charType[] = "char";
#ifdef _WIN32
	if (_stricmp(type, strtok(intType, delimiter)) == 0) { return 0; }
	else if (_stricmp(type, strtok(floatType, delimiter)) == 0) { return 1; }
	else if (_stricmp(type, strtok(doubleType, delimiter)) == 0) { return 2; }
	else if (_stricmp(type, strtok(charType, delimiter)) == 0) { return 3; }
	else { return -1; }
#else
	if (strcmp(type, strtok(intType, delimiter)) == 0) { return 0; }
	else if (strcmp(type, strtok(floatType, delimiter)) == 0) { return 1; }
	else if (strcmp(type, strtok(doubleType, delimiter)) == 0) { return 2; }
	else if (strcmp(type, strtok(charType, delimiter)) == 0) { return 3; }
	else { return -1; }
#endif
}
