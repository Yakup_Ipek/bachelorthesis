/*
Author: Yakup Ipek
Timer in nanoseconds
*/

#ifndef __MYTIME__
#define __MYTIME__

#ifdef _WIN32
#include <windows.h>

typedef struct myTime_ {
  LARGE_INTEGER StartTime, EndTime, Frequency;
  unsigned long long Result;
} MyTimeData;

void start_timer(MyTimeData* time);

void end_timer(MyTimeData* time);

#else
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>

typedef struct myTime_ {
	unsigned long StartTime, EndTime, Result;
} MyTimeData;


void start_timer(MyTimeData* time);

void end_timer(MyTimeData* time);

#endif // WIN32
#endif //__MYTIME__