/*
Author: Yakup Ipek
Writing graph into file
*/
#include "GraphWriter.h"


/*Write Weighted Graph into file*/
void write_weighted_graph_into_file(Graph graph,char* file_name) {
	printf("Writing Graph %s: Started ...\n",file_name);
	ofstream myfile;
	myfile.open(file_name);
	
	/*First line, number of nodes*/
	myfile << graph.nodes_count << "\n";


	/*Write #neighbors into file*/
	myfile << "[";
	for (int i = 0; i < graph.nodes_count; i++) {
		myfile << *(graph.adjacency_list_count_per_node + i);
		if (i < graph.nodes_count - 1) {
			myfile << " ";
		}
	}
	myfile << "]\n";

	/*Write tuple (neighbor,edge_weight) into file*/
	for (int i = 0; i < graph.nodes_count; i++) {
		printf("Progress: ");
		myfile << i << "#";
		for (int j = 0; j < *(graph.adjacency_list_count_per_node + i); j++) {
			myfile << "(" << *(*(graph.adjacency_list + i) + j) << "," << *(*(graph.weighting_list + i) + j) << ")";
			if (j < *(graph.adjacency_list_count_per_node + i) - 1) myfile << "|";
		}
		myfile << "\n";
		printf("%0.0f %%\r",(((float)i+1)/(float)graph.nodes_count)*100);
	}
	printf("\n");
	myfile.close();
	printf("Writing Graph %s: Finished ...\n",file_name);
}

/*Write Graph into file*/
void write_graph_into_file(Graph graph, char* file_name) {
	printf("Writing Graph %s: Started ...\n", file_name);
	ofstream myfile;
	myfile.open(file_name);

	/*First line, number of nodes*/
	myfile << graph.nodes_count << "\n";


	/*Write #neighbors into file*/
	myfile << "[";
	for (int i = 0; i < graph.nodes_count; i++) {
		myfile << *(graph.adjacency_list_count_per_node + i);
		if (i < graph.nodes_count - 1) {
			myfile << " ";
		}
	}
	myfile << "]\n";

	/*Write tuple (neighbor,edge_weight) into file*/
	for (int i = 0; i < graph.nodes_count; i++) {
		printf("Progress: ");
		myfile << i << "#";
		for (int j = 0; j < *(graph.adjacency_list_count_per_node + i); j++) {
			myfile << *(*(graph.adjacency_list + i) + j) ;
			if (j < *(graph.adjacency_list_count_per_node + i) - 1) myfile << "|";
		}
		myfile << "\n";
		printf("%0.0f %%\r", (((float)i+1) / (float)graph.nodes_count) * 100);
	}
	printf("\n");
	myfile.close();
	printf("Writing Graph %s: Finished ...\n", file_name);
}