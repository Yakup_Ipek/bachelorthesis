/*
Author: Yakup Ipek
Header for reading graph from file
*/
#pragma once
#ifndef __GRAPH_READER__
#define __GRAPH_READER__

#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <string.h>
#include <vector>
#include <string>
#include "../DijkstraLibrary/Graph.h"

using namespace std;

void read_graph_from_file(Graph* graph, char* file_name);
void read_weighted_graph_from_file(Graph* graph, char* file_name);

#endif
