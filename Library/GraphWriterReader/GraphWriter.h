/*
Author: Yakup Ipek
Header for writing graph into file
*/
#pragma once
#ifndef __GRAPH_WRITER__
#define __GRAPH_WRITER__

#include <iostream>
#include <fstream>
#include "../DijkstraLibrary/Graph.h"

using namespace std;


void write_weighted_graph_into_file(Graph graph, char* file_name);
void write_graph_into_file(Graph graph, char* file_name);

#endif
