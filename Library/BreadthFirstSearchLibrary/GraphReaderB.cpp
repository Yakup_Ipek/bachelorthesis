#include "GraphReaderB.h"

void read_graph_from_file(Graph* graph, char* file_name) {
	printf("Reading Graph %s: Started ...\n", file_name);
	const char* delimiter = "[]| ";
	string line;
	ifstream myfile;
	myfile.open(file_name, ios::out);

	if (!myfile.is_open()) {
		fprintf(stderr, "File: %s could not be opened\n", file_name);
		exit(-1);
	}

	/*Read first line, which contains the number of nodes*/
	getline(myfile, line);
	graph->nodes_count = atoi(line.c_str());

	/*Allocate memory*/
	graph->nodes = (int*)malloc(sizeof(int)*graph->nodes_count);
	graph->adjacency_list_count_per_node = (int*)malloc(sizeof(int)*graph->nodes_count);
	graph->adjacency_list = (int**)malloc(sizeof(int*)*graph->nodes_count);


	/*label nodes*/
	for (int i = 0; i < graph->nodes_count; i++) {
		graph->nodes[i] = i;
	}

	/*Read second line (Information regarding the adjacency) */
	getline(myfile, line);
	int index = 0;
	char* token = strtok((char*)line.c_str(), delimiter);

	while (token != NULL) {
		*(graph->adjacency_list_count_per_node + index++) = atoi(token);
		//printf("Number of Neighbours: %d\n", *(graph->adjacency_list_count_per_node + (index-1)));
		token = strtok(NULL, delimiter);
	}
	//printf("\n");
	/*Allocate memory to safe neighbours and weights*/
	for (int i = 0; i < graph->nodes_count; i++) {
		graph->adjacency_list[i] = (int*)malloc(sizeof(int)* graph->adjacency_list_count_per_node[i]);
	}

	int row = 0, column = 0;
	while (getline(myfile, line)) {
		printf("Progress: ");
		line.erase(0, line.find("#") + 1);	//delete Node label and # symbol
		token = strtok((char*)line.c_str(), delimiter);
		//printf("Neighbours: \n");
		while (token != NULL) {
			*(*((graph->adjacency_list) + row) + column++) = atoi(token);
			//printf("%d\t", *(*((graph->adjacency_list) + row) + (column - 1)));
			token = strtok(NULL, delimiter);
		}
		//printf("\n");
		row++;
		column = 0;
		printf("%0.0f %%\r", ((float)row / (float)graph->nodes_count) * 100);
	}
	printf("\n");
	myfile.close();
	printf("Reading Graph %s: Finished ...\n", file_name);
}