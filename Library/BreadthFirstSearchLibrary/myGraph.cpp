/*
Author: Yakup Ipek
Graph library with some functionalities
*/
#include "myGraph.h"


void clean_graph(Graph* graph) {
	for (int i = 0; i < graph->nodes_count; i++) {
		free(*((graph->adjacency_list) + i));
	}
	free(graph->adjacency_list);
	free(graph->adjacency_list_count_per_node);
	free(graph->nodes);
}

void print_adjacency_list(Graph graph) {
	printf("--------------Directed Graph---------------\n");
	for (int i = 0; i < graph.nodes_count; i++) {
		printf("Node: %d =[", i);
		for (int j = 0; j < *((graph.adjacency_list_count_per_node) + i); j++) {
			if (j == (*((graph.adjacency_list_count_per_node) + i) - 1)) printf("%d", *((*((graph.adjacency_list) + i)) + j));
			else printf("%d-", *((*((graph.adjacency_list) + i)) + j));
		}
		printf("]\n\n");
	}
}

void generate_random_directed_graph(Graph* graph, int num_nodes, int max_num_of_neighbors, int min_num_of_neigbors) {
	printf("Started generating directed graph ...\n");
	srand(time(NULL));

	assert(max_num_of_neighbors<num_nodes && num_nodes);	//number of nodes has to be greater zero and greate then number of neighbors
	assert(max_num_of_neighbors >= min_num_of_neigbors && "maximum number of neighbours needs to be greater than minimum number of neighbors");	//minimum number of neighbors needs to be less than maximum number of neighbors

	int random_amount, connect_to, node_index = 0;
	graph->nodes_count = num_nodes;
	graph->nodes = (int*)malloc(sizeof(int)*num_nodes);
	graph->adjacency_list = (int**)malloc(sizeof(int*)*num_nodes);	//each node has an adjacency list
	graph->adjacency_list_count_per_node = (int*)malloc(sizeof(int)*num_nodes);	//each node has an adjacency list

																				/*   initialize nodes   */
	for (int i = 0; i < num_nodes; i++) {
		*((graph->nodes) + i) = i;
	}

	/* define neighbors  */
	for (int i = 0; i < num_nodes; i++) {
		printf("Progress: ");
		int mod = max_num_of_neighbors != 1 ? max_num_of_neighbors - 1 : max_num_of_neighbors;
		int r = rand() % (max_num_of_neighbors);
		if (max_num_of_neighbors == min_num_of_neigbors) {	//exactly min neighbors
			random_amount = min_num_of_neigbors;
		}
		else {
			if ((r + min_num_of_neigbors) > num_nodes) {
				random_amount = num_nodes;
			}
			else {
				random_amount = r + min_num_of_neigbors;
			}
		}

		*((graph->adjacency_list) + i) = (int*)malloc(sizeof(int)*random_amount);
		*((graph->adjacency_list_count_per_node) + i) = random_amount;
		/*  choose neighbors   */
		int* flag = (int*)calloc(num_nodes, sizeof(int));
		flag[i] = 1;
		for (int j = 0; j < random_amount; j++) {

			//avoid redundant neighbors
			connect_to = rand() % num_nodes;
			while (flag[connect_to]) { //if its marked, then start to increment, reason: random may take more time to find unmarked node
				if (connect_to == num_nodes - 1) connect_to = 0;
				else connect_to++;
			}
			flag[connect_to] = 1;	//mark
									//printf("Node: %d\tConnect_to: %d\n",i,connect_to);
			*(*((graph->adjacency_list) + i) + j) = connect_to;
		}
		printf("%0.0f %%\r", ((float)i / ((float)num_nodes - 1)) * 100);
		free(flag);
	}
	printf("\n");
	printf("Directed graph generated...\n\n");
}

void generate_random_undirected_graph(Graph* graph, int num_nodes, int min_num_of_neigbors) {
	printf("Started generating undirected graph ...\n");
	srand(time(NULL));

	assert(num_nodes>min_num_of_neigbors && "number of neighbors has to be less than #nodes of graph");	//number of nodes has to be greater zero and greate then number of neighbors

	int random_amount, connect_to, node_index = 0;
	graph->nodes_count = num_nodes;
	graph->nodes = (int*)malloc(sizeof(int)*num_nodes);
	graph->adjacency_list = (int**)malloc(sizeof(int*)*num_nodes);	//each node has an adjacency list
	graph->adjacency_list_count_per_node = (int*)malloc(sizeof(int)*num_nodes);	//each node has an adjacency list

																				/*each node can have maximally nodes_count neighbors*/
	for (int i = 0; i < num_nodes; i++) {
		*((graph->adjacency_list) + i) = (int*)malloc(sizeof(int)*graph->nodes_count);
		for (int j = 0; j < graph->nodes_count; j++) {
			*((*((graph->adjacency_list) + i)) + j) = -1;	//initialize with -1, in order to decide later how many neighbor the node has
		}
	}

	/*   initialize nodes   */
	for (int i = 0; i < num_nodes; i++) {
		*((graph->nodes) + i) = i;
	}

	/* define neighbors  */
	for (int i = 0; i < num_nodes; i++) {
		printf("Progress: ");
		random_amount = (rand() % (num_nodes - min_num_of_neigbors)) + min_num_of_neigbors;	//number of nodes to add

																							/*  choose neighbors   */
		int* flag = (int*)calloc(num_nodes, sizeof(int));
		flag[i] = 1;	//mark own node
		for (int k = 0; k < num_nodes; k++) {	//update flag, since other nodes may have add themself to my list
			int idx = *(*((graph->adjacency_list) + i) + k);
			if (idx != -1) {
				flag[idx] = 1;
			}
			else break;
		}

		int counter = 0;
		for (int k = 0; k < num_nodes; k++) {
			if (flag[k] == 0) counter++;
		}
		int remaining_to_add = counter > random_amount ? random_amount : counter;	//if other nodes connected to me already, I need to recompute the amount to add new nodes

																					// add new nodes
		for (int j = 0; j < remaining_to_add; j++) {

			//avoid redundant neighbors
			connect_to = rand() % num_nodes;
			while (flag[connect_to]) { //if its marked, then start to increment, reason: random may take more time to find unmarked node
				if (connect_to == num_nodes - 1) connect_to = 0;
				else connect_to++;
			}
			flag[connect_to] = 1;	//mark

									//add to the end of the list
			for (int k = 0; k < num_nodes; k++) {
				int value = *(*((graph->adjacency_list) + i) + k);
				if (value == -1) {
					*(*((graph->adjacency_list) + i) + k) = connect_to;
					break;
				}
			}

			//add i_th node to its neighbor
			for (int k = 0; k < num_nodes; k++) {
				int value = *((*((graph->adjacency_list) + connect_to)) + k);
				if (value == -1) {	//add to the last
					*((*((graph->adjacency_list) + connect_to)) + k) = i;
					break;
				}
			}
		}
		free(flag);
		printf("%0.0f %%\r", ((float)i / ((float)num_nodes - 1)) * 100);
	}
	printf("\n");
	/* number of neighbors is set*/
	for (int i = 0; i < num_nodes; i++) {
		for (int j = 0; j < graph->nodes_count; j++) {
			if (*((*((graph->adjacency_list) + i)) + j) == -1) {
				*((graph->adjacency_list_count_per_node) + i) = j;
				break;
			}
		}
	}
	printf("Undirected graph generated...\n\n");
}


int size_to_wrap_up(Graph graph) {
	int result = 0;
	for (int i = 0; i < graph.nodes_count; i++) {
		result += graph.adjacency_list_count_per_node[i];
	}
	return result;
}

void determine_starting_points(Graph graph, int** start) {
	for (int i = 0; i < graph.nodes_count; i++) {
		if (i == 0) {
			*((*start) + i) = 0;
		}
		else {
			*((*start) + i) = *((*start) + (i - 1)) + *(graph.adjacency_list_count_per_node + (i - 1));
		}
	}
}

void wrap_neighbours_up(Graph graph, int** neighbours) {
	for (int i = 0, index = 0; i < graph.nodes_count; i++) {
		for (int j = 0; j < graph.adjacency_list_count_per_node[i]; j++) {
			*((*neighbours) + (index++)) = *((*(graph.adjacency_list + i)) + j);
		}
	}
}

