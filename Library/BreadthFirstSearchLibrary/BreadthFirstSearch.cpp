/*
Author: Yakup Ipek
Implementing some functionalities for BreadthFirstSearch
*/
#include "BreadthFirstSearchLib.h"

void initialize_bfs(BfsData* bfs, int num) {
	bfs->dist_to = (int*)malloc(sizeof(int)*num);
	bfs->edge_to = (int*)malloc(sizeof(int)*num);
	bfs->marked = (int*)malloc(sizeof(int)*num);
}

void free_bfs(BfsData* bfs) {
	free(bfs->dist_to);
	free(bfs->edge_to);
	free(bfs->marked);
}

void bfs_seqential(Graph graph, BfsData* bfs, int source) {

	List* queue = NULL;
	for (int i = 0; i < graph.nodes_count; i++) {
		bfs->marked[i] = 0;
		bfs->dist_to[i] = INT_MAX;
	}
	bfs->dist_to[source] = 0;
	bfs->marked[source] = 1;
	enqueue(&queue, source);
	while (!is_empty_l(queue)) {
		int v = dequeue(&queue);
		for (int i = 0; i < *(graph.adjacency_list_count_per_node + v); i++) {
			int w = *((*(graph.adjacency_list + v)) + i);
			if (!bfs->marked[w]) {
				bfs->edge_to[w] = v;
				bfs->dist_to[w] = bfs->dist_to[v] + 1;
				bfs->marked[w] = 1;
				enqueue(&queue, w);
			}
		}
	}

}

bool has_path_to(BfsData bfs, int node) {
	return bfs.marked[node] > 0 ? true : false;
}

int distance_to(BfsData bfs, int node) {
	return bfs.dist_to[node];
}

List* path_to(BfsData bfs, int node) {
	List* head = (List*)malloc(sizeof(List));
	head->value = node;
	head->next = NULL;
	List* temp = (List*)malloc(sizeof(List));
	int i;
	for (i = node; bfs.dist_to[i] != 0; i = bfs.edge_to[i]) {

		if (head->value == i) continue;
		else temp->value = i;

		temp->next = (List*)(malloc(sizeof(List)));
		temp = temp->next;
	}
	temp->value = i;
	temp->next = NULL;
	head->next = temp;
	return head;
}

void print_list(List* head) {
	assert(head != NULL && "List is NULL");
	List* temp = head;
	printf("Path:[");
	while (true) {
		if (temp->next != NULL) {
			printf("%d->", temp->value);
		}
		else {
			printf("%d", temp->value);
			break;
		}
		temp = temp->next;
	}
	printf("]\n");
}



bool is_empty_l(List* head) {
	return head == NULL ? true : false;
}

void enqueue(List** head, int element) {

	if ((*head) == NULL) {	//when list is empty
		*head = (List*)malloc(sizeof(List));
		(*head)->value = element;
		(*head)->next = NULL;
	}
	else {
		List* temp = *head;
		while (temp->next != NULL) {
			temp = temp->next;
		}
		temp->next = (List*)malloc(sizeof(List));
		temp->next->value = element;
		temp->next->next = NULL;
	}
}

int dequeue(List** head) {
	if ((*head) == NULL) return -1;
	List* next = (*head)->next;
	int value = (*head)->value;
	free(*head);
	*head = next;
	return value;
}

void free_list(List* head) {
	List* tmp;
	while (head != NULL) {
		tmp = head;
		head = head->next;
		free(tmp);
	}
}

const char* verify_bfs(BfsData first_bfs, BfsData second_bfs,int number_of_nodes) {
	for (int i = 0; i < number_of_nodes; i++) {
		if ((first_bfs.dist_to[i] != second_bfs.dist_to[i]) || (has_path_to(first_bfs, i) != has_path_to(second_bfs, i))) {
			//printf("Node: %d, [FirstParam] Dist: %d, [SecondParam] Dist: %d\n\n",i, first_bfs.dist_to[i], second_bfs.dist_to[i]);
			//printf("Node: %d, [FirstParam] Marked : %d, [SecondParam] Marked : %d\n\n",i, has_path_to(first_bfs, i), has_path_to(second_bfs, i));
			return "ERROR";
		}
	}
	return "OK";
}

void test_sequential_bfs(Graph* graph, int source, int random_range, int number_of_tests, int type, bool print_path) {

	BfsData bfs;
	srand(time(NULL));
	int number_of_nodes;
	initialize_bfs(&bfs, graph->nodes_count);
	bfs_seqential(*graph, &bfs, source);
	number_of_nodes = graph->nodes_count;

	assert(random_range>number_of_nodes && "random_range should be greater than number of nodes, so that also nodes which are not contained in graph are checked");

	for (int i = 0; i < number_of_tests; i++) {
		int path_to_node = rand() % random_range;
		switch (type) {
		case 1:
			if (has_path_to(bfs, path_to_node) == true) {
				//printf("[YES]Test: %d\n", has_path_to(bfs, path_to_node));
				printf("Directed Graph: Is there a path from %d to %d: %s\n", source, path_to_node, has_path_to(bfs, path_to_node) == true ? "yes" : "no");
				printf("Directed Graph: Distance from %d to %d: %d\n", source, path_to_node, distance_to(bfs, path_to_node));
				if (print_path == true) print_list(path_to(bfs, path_to_node));
			}
			else {
				//printf("[NO]Test: %d\n", has_path_to(bfs, path_to_node));
				printf("Directed Graph: Is there a path from %d to %d: %s\n", source, path_to_node, has_path_to(bfs, path_to_node) == true ? "yes" : "no");
			}
			break;
		case 2:
			if (has_path_to(bfs, path_to_node) == true) {
				printf("Undirected Graph: Is there a path from %d to %d: %s\n", source, path_to_node, has_path_to(bfs, path_to_node) == true ? "yes" : "no");
				printf("Undirected Graph: Distance from %d to %d: %d\n", source, path_to_node, distance_to(bfs, path_to_node));
				if (print_path == true) print_list(path_to(bfs, path_to_node));
			}
			else {
				printf("Undirected Graph: Is there a path from %d to %d: %s\n", source, path_to_node, has_path_to(bfs, path_to_node) == true ? "yes" : "no");
			}
			break;
		default:
			fprintf(stderr, "wrong type");
			break;
		}
		printf("\n");
	}
	free_bfs(&bfs);
}
