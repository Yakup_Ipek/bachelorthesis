/*
Author: Yakup Ipek
Header for Graph generation
*/

#ifndef  __MYGRAPH_H__
#define __MYGRAPH_H__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>

typedef struct{
	int* nodes;	//all nodes in graph
	int nodes_count;	//important in order to free memory
	int** adjacency_list; //list of successor node
	int* adjacency_list_count_per_node; //necessary for printing the list
} Graph;

void generate_random_directed_graph(Graph* graph, int num_nodes, int max_num_of_neighbors, int min_num_of_neigbors);
void generate_random_undirected_graph(Graph* graph, int num_nodes, int min_num_of_neigbors);
void print_adjacency_list(Graph graph);
void clean_graph(Graph* graph);
int size_to_wrap_up(Graph graph);
void wrap_neighbours_up(Graph graph, int** neighbours);
void determine_starting_points(Graph graph, int** start);

#endif // __MYGRAPH_H__
