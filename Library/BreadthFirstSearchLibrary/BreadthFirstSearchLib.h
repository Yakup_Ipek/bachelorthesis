/*
Author: Yakup Ipek
This Library is necessary for executing the Breadth first search algorithm
*/
#ifndef __BREADTH_FIRST_SEARCH_LIB__
#define __BREADTH_FIRST_SEARCH_LIB__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <limits.h>
#include "myGraph.h"

typedef struct {
	int* marked;	//marked[v] is there a path to node v
	int* edge_to; //edge_to[v] parent node of v
	int* dist_to; //dist_to[v] number of edges to  node v
} BfsData;

typedef struct node {
	int value;
	struct node* next;
} List;

/* returns a list containing the shortest path from source to node
*  bfs contains data calculated regarding source node
*/
List* path_to(BfsData bfs, int node);

/*
* is there a path from root to 'node' (Second argument)
*/
bool has_path_to(BfsData bfs, int node);

/*
* return the distance from root to node
*/
int distance_to(BfsData bfs, int node);

/*
*	computes the breadth first search algorithm sequentially
*/
void bfs_seqential(Graph graph, BfsData* bfs, int source);


/*
*	initialize structure BfsData
*/
void initialize_bfs(BfsData* bfs, int num);

/*
*	free memory
*/
void free_bfs(BfsData* bfs);

/*
* verify if two computed Bfs match
* used in order to check whether sequential verson matches with the parallel version 
*/
const char* verify_bfs(BfsData first_bfs, BfsData second_bfs, int number_of_nodes);

/*
* list functions
*/
bool is_empty_l(List* head);
void enqueue(List** head, int element);
int dequeue(List** head);
void free_list(List* head);

#endif