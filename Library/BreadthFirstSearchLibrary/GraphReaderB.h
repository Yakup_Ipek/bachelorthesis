/*
Author: Yakup Ipek
Reading graph from file
*/
#ifndef __GRAPHREADERB_H__
#define __GRAPHREADERB_H__

#include <fstream>
#include <string.h>
#include <string>
#include "myGraph.h"

using namespace std;

void read_graph_from_file(Graph* graph, char* file_name);

#endif // !__GRAPHREADERB_H__