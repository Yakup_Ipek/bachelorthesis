#include "MyTime.h"

#ifdef _WIN32

void start_timer(MyTimeData* time) {
	QueryPerformanceFrequency(&(time->Frequency));
	QueryPerformanceCounter(&(time->StartTime));
}

void end_timer(MyTimeData* time) {
	ULARGE_INTEGER EndTime, ElapsedMicroseconds;
	QueryPerformanceCounter(&(time->EndTime));
	ElapsedMicroseconds.QuadPart = (time->EndTime).QuadPart - (time->StartTime).QuadPart;
	ElapsedMicroseconds.QuadPart *= 1000000000ULL;	//convert to nanoseconds
	ElapsedMicroseconds.QuadPart /= (time->Frequency).QuadPart;
	time->Result = ElapsedMicroseconds.QuadPart;
}

#else

void start_timer(MyTimeData* time) {
	struct timeval tv;
	gettimeofday(&tv, 0);
	time->StartTime = tv.tv_sec * 1000000000ull + tv.tv_usec * 1000ull;
}

void end_timer(MyTimeData* time) {
	struct timeval tv;
	gettimeofday(&tv, 0);
	time->EndTime = tv.tv_sec * 1000000000ull + tv.tv_usec * 1000ull;
	time->Result = time->EndTime - time->StartTime;
}

#endif // WIN32