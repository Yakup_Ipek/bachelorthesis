#include "myOpenCL_Lib.h"

#ifdef _WIN32
#include <Share.h>
void read_from_user_input_platform_id(int* id, int numplatforms) {

	while (true) {
		printf("Enter: ");
		scanf_s("%d", id);
		if (*(id)<0 || ((cl_uint)*(id))>(numplatforms - 1)) {
			printf("\nWrong value entered try again\n");
			continue;
		}
		else {
			break;
		}
	}
}

void read_from_user_input_device_id(int* id, int numDevices) {

	//check wether entered value is correct or not
	while (true) {
		printf("Enter: ");
		scanf_s("%d", id);
		if (((cl_uint)(*id)) > (numDevices - 1) || (*id) < 0) {
			printf("\nEntered value is wrong try again\n");
			continue;
		}
		else {
			break;
		}
	}
}

void read_program_from_source(const char* file_name, char** source_str, size_t* sourceSize) {
	FILE* fp = _fsopen(file_name, "rb", _SH_DENYWR);
	if (!fp) {
		printf("Error while loading the source code %s\n", file_name);
		exit(1);
	}
	*source_str = (char *)malloc(sizeof(char)*MAX_SOURCE_SIZE);

	*sourceSize = fread(*source_str, 1, MAX_SOURCE_SIZE, fp);

	fclose(fp);
}
#else
#include <unistd.h>
#include <errno.h>

void read_from_user_input_platform_id(int* id, int numplatforms) {

	while (true) {
		printf("Enter: ");
		scanf("%d", id);
		if (*(id)<0 || ((cl_uint)*(id))>(numplatforms - 1)) {
			printf("\nWrong value entered try again\n");
			continue;
		}
		else {
			break;
		}
	}
}

void read_from_user_input_device_id(int* id, int numDevices) {

	//check wether entered value is correct or not
	while (true) {
		printf("Enter: ");
		scanf("%d", id);
		if (((cl_uint)(*id)) > (numDevices - 1) || (*id) < 0) {
			printf("\nEntered value is wrong try again\n");
			continue;
		}
		else {
			break;
		}
	}
}

void read_program_from_source(const char* file_name, char** source_str, size_t* sourceSize) {
	/*
	char cwd[1024];
	if (getcwd(cwd, sizeof(cwd)) != NULL)
	    fprintf(stdout, "Current working dir: %s\n", cwd);
	else
	    perror("getcwd() error");
	*/
	FILE* fp = fopen(file_name, "r");
	if (!fp) {
		printf("Error while loading the source code %s\n", file_name);
		exit(1);
	}
	*source_str = (char *)malloc(sizeof(char)*MAX_SOURCE_SIZE);

	*sourceSize = fread(*source_str, 1, MAX_SOURCE_SIZE, fp);

	fclose(fp);
}
#endif // WIN32

cl_device_id clInitWithDeviceQueue(cl_context* context, cl_command_queue* hostQueue, cl_command_queue* deviceQueue) {
	cl_int status;

	cl_uint numPlatforms = 0;
	cl_platform_id *platforms = NULL;
	cl_int platformID;

	//---------------------------------------------------------//
	//--------------retrieving platforms-----------------------//	
	CLU_ERRCHECK(clGetPlatformIDs(0, NULL, &numPlatforms), "ERROR when retrieving number of platforms")
		platforms = (cl_platform_id *)malloc(
			numPlatforms * sizeof(cl_platform_id));

	// the second call, get the platforms
	CLU_ERRCHECK(clGetPlatformIDs(numPlatforms, platforms, NULL), "Error when retrieving platforms");
	choosePlatformID(numPlatforms, platforms, &platformID);

	//--------------------------------------------------------//
	//-------------retrieving devices ------------------------//
	cl_uint numDevices = 0;
	cl_device_id *devices = NULL;
	cl_int deviceId;
	// as before, the devices are retrieved in two steps (Platform[0]: Intel CPU, HD Graphics; Platform[1]: NVIDIA GPU Platform[2]: Intel CPU Experimental OpenCL 2.0)
	CLU_ERRCHECK(clGetDeviceIDs(platforms[platformID], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices), "Error when retrieving number of available devices");

	// allocating space for the devices
	devices = (cl_device_id *)malloc(numDevices * sizeof(cl_device_id));
	// the second call get the devices
	CLU_ERRCHECK(clGetDeviceIDs(platforms[platformID], CL_DEVICE_TYPE_ALL, numDevices, devices, NULL), "Error when retrieving devices")

		chooseDeviceID(numDevices, devices, platforms[platformID], &deviceId);
	//--------------------------------------------------------//
	//-------------creating a context-------------------------//

	*context = clCreateContext(NULL, numDevices, devices, NULL, NULL, &status);
	CLU_ERRCHECK(status, "Error when creating context");
	//--------------------------------------------------------//
	//-------------creating a comand queue -------------------//

	cl_uint queueSize;
	clGetDeviceInfo(devices[deviceId], CL_DEVICE_QUEUE_ON_DEVICE_PREFERRED_SIZE, sizeof(queueSize), &queueSize, NULL);
	//printf("Preffered Size for commandQueue: %u\n", queueSize);

	cl_command_queue_properties properties = CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE | CL_QUEUE_ON_DEVICE | CL_QUEUE_ON_DEVICE_DEFAULT | CL_QUEUE_PROFILING_ENABLE;

	cl_queue_properties qprop[] = { CL_QUEUE_SIZE, queueSize, CL_QUEUE_PROPERTIES, properties, 0 };


	cl_command_queue_properties property = CL_QUEUE_PROFILING_ENABLE;
	cl_queue_properties qprop2[] = { CL_QUEUE_PROPERTIES, property, 0 };
	*hostQueue = clCreateCommandQueueWithProperties(*context, devices[deviceId], qprop2, &status);
	CLU_ERRCHECK(status, "Error when creating commandqueue with properties");

	*deviceQueue = clCreateCommandQueueWithProperties(*context, devices[deviceId], qprop, &status);
	CLU_ERRCHECK(status, "Error when creating commandqueue with properties");

	return *(devices + deviceId);
}

cl_device_id clInitWithDeviceQueueByParam(cl_context* context, cl_command_queue* hostQueue, cl_command_queue* deviceQueue, int platform_id, int device_id){
	cl_int status;

	cl_uint numPlatforms = 0;
	cl_platform_id *platforms = NULL;
	cl_int platformID=(cl_int) platform_id;
	cl_uint numDevices = 0;
	cl_device_id *devices = NULL;
	cl_int deviceId=(cl_int) device_id;

	//---------------------------------------------------------//
	//--------------retrieving platforms-----------------------//	
	CLU_ERRCHECK(clGetPlatformIDs(0, NULL, &numPlatforms), "ERROR when retrieving number of platforms")
		platforms = (cl_platform_id *)malloc(
			numPlatforms * sizeof(cl_platform_id));

	// the second call, get the platforms
	CLU_ERRCHECK(clGetPlatformIDs(numPlatforms, platforms, NULL), "Error when retrieving platforms");

	//--------------------------------------------------------//
	//-------------retrieving devices ------------------------//
	
	// as before, the devices are retrieved in two steps (Platform[0]: Intel CPU, HD Graphics; Platform[1]: NVIDIA GPU Platform[2]: Intel CPU Experimental OpenCL 2.0)
	CLU_ERRCHECK(clGetDeviceIDs(platforms[platformID], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices), "Error when retrieving number of available devices");

	// allocating space for the devices
	devices = (cl_device_id *)malloc(numDevices * sizeof(cl_device_id));
	// the second call get the devices
	CLU_ERRCHECK(clGetDeviceIDs(platforms[platformID], CL_DEVICE_TYPE_ALL, numDevices, devices, NULL), "Error when retrieving devices");

	//--------------------------------------------------------//
	//-------------creating a context-------------------------//

	*context = clCreateContext(NULL, numDevices, devices, NULL, NULL, &status);
	CLU_ERRCHECK(status, "Error when creating context");
	//--------------------------------------------------------//
	//-------------creating a comand queue -------------------//

	cl_uint queueSize;
	clGetDeviceInfo(devices[deviceId], CL_DEVICE_QUEUE_ON_DEVICE_PREFERRED_SIZE, sizeof(queueSize), &queueSize, NULL);
	//printf("Preffered Size for commandQueue: %u\n", queueSize);

	cl_command_queue_properties properties = CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE | CL_QUEUE_ON_DEVICE | CL_QUEUE_ON_DEVICE_DEFAULT | CL_QUEUE_PROFILING_ENABLE;

	cl_queue_properties qprop[] = { CL_QUEUE_SIZE, queueSize, CL_QUEUE_PROPERTIES, properties, 0 };


	cl_command_queue_properties property = CL_QUEUE_PROFILING_ENABLE;
	cl_queue_properties qprop2[] = { CL_QUEUE_PROPERTIES, property, 0 };
	*hostQueue = clCreateCommandQueueWithProperties(*context, devices[deviceId], qprop2, &status);
	CLU_ERRCHECK(status, "Error when creating commandqueue with properties");

	*deviceQueue = clCreateCommandQueueWithProperties(*context, devices[deviceId], qprop, &status);
	CLU_ERRCHECK(status, "Error when creating commandqueue with properties");

	return *(devices + deviceId);
}


cl_device_id clInit(cl_context* context, cl_command_queue* queue) {
	cl_int status;

	cl_uint numPlatforms = 0;
	cl_platform_id *platforms = NULL;
	cl_int platformID;

	//---------------------------------------------------------//
	//--------------retrieving platforms-----------------------//	
	CLU_ERRCHECK(clGetPlatformIDs(0, NULL, &numPlatforms), "ERROR when retrieving number of platforms");

	platforms = (cl_platform_id *)malloc(numPlatforms * sizeof(cl_platform_id));

	// the second call, get the platforms
	CLU_ERRCHECK(clGetPlatformIDs(numPlatforms, platforms, NULL), "Error when retrieving platforms");
	choosePlatformID(numPlatforms, platforms, &platformID);

	//--------------------------------------------------------//
	//-------------retrieving devices ------------------------//
	cl_uint numDevices = 0;
	cl_device_id *devices = NULL;
	cl_int deviceId;

	// as before, the devices are retrieved in two steps (Platform[0]: Intel CPU, HD Graphics; Platform[1]: NVIDIA GPU Platform[2]: Intel CPU Experimental OpenCL 2.0)
	CLU_ERRCHECK(clGetDeviceIDs(platforms[platformID], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices), "Error when retrieving number of available devices");

	// allocating space for the devices
	devices = (cl_device_id *)malloc(numDevices * sizeof(cl_device_id));

	// the second call get the devices
	CLU_ERRCHECK(clGetDeviceIDs(platforms[platformID], CL_DEVICE_TYPE_ALL, numDevices, devices, NULL), "Error when retrieving devices");
	chooseDeviceID(numDevices, devices, platforms[platformID], &deviceId);

	//--------------------------------------------------------//
	//-------------creating a context-------------------------//

	*context = clCreateContext(NULL, numDevices, devices, NULL, NULL, &status);
	CLU_ERRCHECK(status, "Error when creating context");
	//--------------------------------------------------------//
	//-------------creating a comand queue -------------------//
	cl_command_queue_properties property = CL_QUEUE_PROFILING_ENABLE;
	*queue = clCreateCommandQueue(*context, devices[deviceId], property, &status);
	CLU_ERRCHECK(status, "Error when creating commandqueue with properties");

	return *(devices + deviceId);
}

cl_device_id clInitByParam(cl_context* context, cl_command_queue* queue, int platform_id, int device_id){
	
	cl_int status;
	cl_uint numPlatforms = 0;
	cl_platform_id *platforms = NULL;
	cl_int platformID=(cl_int) platform_id;
	cl_uint numDevices = 0;
	cl_device_id *devices = NULL;
	cl_int deviceId = (cl_int)device_id;


	//---------------------------------------------------------//
	//--------------retrieving platforms-----------------------//	
	CLU_ERRCHECK(clGetPlatformIDs(0, NULL, &numPlatforms), "ERROR when retrieving number of platforms");

	platforms = (cl_platform_id *)malloc(numPlatforms * sizeof(cl_platform_id));

	// the second call, get the platforms
	CLU_ERRCHECK(clGetPlatformIDs(numPlatforms, platforms, NULL), "Error when retrieving platforms");
	
	
	//--------------------------------------------------------//
	//-------------retrieving devices ------------------------//
	
	// as before, the devices are retrieved in two steps (Platform[0]: Intel CPU, HD Graphics; Platform[1]: NVIDIA GPU Platform[2]: Intel CPU Experimental OpenCL 2.0)
	CLU_ERRCHECK(clGetDeviceIDs(platforms[platformID], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices), "Error when retrieving number of available devices");

	// allocating space for the devices
	devices = (cl_device_id *)malloc(numDevices * sizeof(cl_device_id));

	// the second call get the devices
	CLU_ERRCHECK(clGetDeviceIDs(platforms[platformID], CL_DEVICE_TYPE_ALL, numDevices, devices, NULL), "Error when retrieving devices");
	
	
	//--------------------------------------------------------//
	//-------------creating a context-------------------------//

	*context = clCreateContext(NULL, numDevices, devices, NULL, NULL, &status);
	CLU_ERRCHECK(status, "Error when creating context");
	//--------------------------------------------------------//
	//-------------creating a comand queue -------------------//
	cl_command_queue_properties property = CL_QUEUE_PROFILING_ENABLE;
	*queue = clCreateCommandQueue(*context, devices[deviceId], property, &status);
	CLU_ERRCHECK(status, "Error when creating commandqueue with properties");

	return *(devices + deviceId);
	
}

void choosePlatformID(cl_uint numplatforms, cl_platform_id* platforms, cl_int* id) {
	size_t buff_size = 256;
	char** buffer = (char**)malloc(sizeof(char*)*numplatforms);
	for (int i = 0; i < numplatforms; i++) {
		*(buffer + i) = (char*)malloc(sizeof(char)*buff_size);
	}
	//char** buffer = (char**)malloc(sizeof(char)*buff_size);
	printf("------------Choose platform------------\n");
	for (int i = 0; i < numplatforms; i++) {
		CLU_ERRCHECK(clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, sizeof(char)*buff_size, *(buffer + i), NULL), "Error when reading platform info");
		printf("%d.platform: %s\n", i, *(buffer + i));
	}

	printf("\nSelect a platform by entering the number in front of the desired platform\n\n");
	read_from_user_input_platform_id(id, numplatforms);
	printf("\nChosen platform is: %s\n", *(buffer + (*id)));

	for (int i = 0; i < numplatforms; i++) {
		free(*(buffer + i));
	}
	free(buffer);
	printf("----------------------------------\n");
}

void chooseDeviceID(cl_uint numDevices, cl_device_id* devices, cl_platform_id platform, cl_int* id) {

	size_t buff_size = 256;
	char** openCL_buffer = (char**)malloc(sizeof(char*)*numDevices);
	char** vendor_buffer = (char**)malloc(sizeof(char*)*numDevices);
	char** name_buffer = (char**)malloc(sizeof(char*)*numDevices);

	for (int i = 0; i < numDevices; i++) {
		*(openCL_buffer + i) = (char*)malloc(sizeof(char)*buff_size);
		*(vendor_buffer + i) = (char*)malloc(sizeof(char)*buff_size);
		*(name_buffer + i) = (char*)malloc(sizeof(char)*buff_size);
	}

	printf("\n------------Choose device------------\n");
	for (int i = 0; i < numDevices; i++) {
		CLU_ERRCHECK(clGetDeviceInfo(devices[i], CL_DEVICE_OPENCL_C_VERSION, buff_size, *(openCL_buffer + i), NULL), "Error when reading device openCL_version");
		CLU_ERRCHECK(clGetDeviceInfo(devices[i], CL_DEVICE_NAME, buff_size, *(vendor_buffer + i), NULL), "Error when reading device name");
		CLU_ERRCHECK(clGetDeviceInfo(devices[i], CL_DEVICE_VENDOR, buff_size, *(name_buffer + i), NULL), "Error when reading device vendor");
		printf("%d.%s\tVendor: %s \tOPENCL_VERSION: %s\n", i, *(name_buffer + i), *(vendor_buffer + i), *(openCL_buffer + i));
		if (numDevices>1 && i + 1<numDevices) printf("---------Next Device-------\n");
	}


	printf("\nSelect a device by entering the number in front of the desired device\n\n");
	//check wether entered value is correct or not
	read_from_user_input_device_id(id, numDevices);
	printf("\nChosen device is:\n%s\tVendor: %s\tOPENCL_VERSION: %s\n\n", *(name_buffer + (*id)), *(vendor_buffer + (*id)), *(openCL_buffer + (*id)));

	for (int i = 0; i < numDevices; i++) {
		free(*(openCL_buffer + i));
		free(*(vendor_buffer + i));
		free(*(name_buffer + i));
	}

	free(openCL_buffer);
	free(vendor_buffer);
	free(name_buffer);
	printf("----------------------------------\n");
}

void myCLBuildProgram(cl_context context, cl_program* program, cl_device_id deviceId, const char* file_name, const char* option) {

	cl_int errcode;
	size_t sourceSize;
	char *source_str;

	read_program_from_source(file_name, &source_str, &sourceSize);
	//fprintf(stdout,source_str);
	*program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t*)&sourceSize, &errcode);

	CLU_ERRCHECK(errcode, "Problem when creating program with source");

	//check whether the program has to be run with the OpenCL2.0 option
	if (needToCompileWithOCL2(deviceId)) {
		char optionOCL2[BUFFER_SIZE_FOR_BUILDING_PROGRAM] = { "-cl-std=CL2.0 " };
		strcat(optionOCL2, option);
		errcode = clBuildProgram(*program, 1, &deviceId, (const char*)optionOCL2, NULL, NULL);
	}
	else {
		errcode = clBuildProgram(*program, 1, &deviceId, (const char*)option, NULL, NULL);
	}
	if (errcode != CL_SUCCESS) {
		printf("Error Code: %s\n", checkCLStatus(errcode));
		// Allocate memory for the log
		cl_build_status status;
		// Get the log
		clGetProgramBuildInfo(*program, deviceId, CL_PROGRAM_BUILD_STATUS, sizeof(cl_build_status), &status, NULL);
		
		if(status==CL_BUILD_NONE){
		    printf("CL_BUILD_NONE\n");
		    CLU_ERRCHECK(errcode,"Error building program");
		}else if(status==CL_BUILD_ERROR){
		    printf("CL_BUILD_ERROR");
		}else if(status==CL_BUILD_SUCCESS){
		    printf("CL_BUILD_SUCCESS");
		}else if(status==CL_BUILD_IN_PROGRESS){
		    printf("CL_BUILD_IN_PROGRESS");
		}
		
		int log_size = 1024*sizeof(char);
		char *log = (char *)malloc(log_size);
		clGetProgramBuildInfo(*program, deviceId, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);
		printf("Error: %d\n", errcode);
		printf("Log: %s\n", log);
		free(log);
		exit(errcode);
	}
}

cl_ulong retrieveTotalExecutionTimeFromEvent(cl_event* event) {
	cl_ulong start_time, finish_time;
	size_t size = sizeof(cl_ulong);
	clWaitForEvents(1, event);
	clGetEventProfilingInfo(*event, CL_PROFILING_COMMAND_START, size, &start_time, NULL);
	clGetEventProfilingInfo(*event, CL_PROFILING_COMMAND_END, size, &finish_time, NULL);
	return finish_time - start_time;
}


int needToCompileWithOCL2(cl_device_id id) {
	size_t buff_size = 256;
	char* openCL_buffer = (char*)malloc(sizeof(char)*buff_size);
	CLU_ERRCHECK(clGetDeviceInfo(id, CL_DEVICE_OPENCL_C_VERSION, buff_size, openCL_buffer, NULL), "Error when reading device openCL_version");
	int yesOrNo = strstr(openCL_buffer, "OpenCL C 2.0") != NULL ? 1 : 0;
	free(openCL_buffer);
	return yesOrNo;
}

const char* checkCLStatus(cl_int status) {
	switch (status) {
	case CL_SUCCESS:  return "CL_SUCCESS";
	case CL_INVALID_PROGRAM_EXECUTABLE: return "CL_INVALID_PROGRAM_EXECUTABLE";
	case CL_INVALID_COMMAND_QUEUE: return "CL_INVALID_COMMAND_QUEUE";
	case CL_INVALID_KERNEL: return "CL_INVALID_KERNEL";
	case CL_INVALID_CONTEXT: return "CL_INVALID_CONTEXT";
	case CL_INVALID_KERNEL_ARGS: return "CL_INVALID_KERNEL_ARGS";
	case CL_INVALID_WORK_DIMENSION: return "CL_INVALID_WORK_DIMENSION";
	case CL_INVALID_WORK_GROUP_SIZE: return "CL_INVALID_WORK_GROUP_SIZE";
	case CL_INVALID_WORK_ITEM_SIZE: return "CL_INVALID_WORK_ITEM_SIZE";
	case CL_INVALID_GLOBAL_OFFSET: return "CL_INVALID_GLOBAL_OFFSET";
	case CL_INVALID_EVENT_WAIT_LIST: return "CL_INVALID_EVENT_WAIT_LIST";
	case CL_INVALID_VALUE: return "CL_INVALID_VALUE";
	case CL_INVALID_MEM_OBJECT: return "CL_INVALID_MEM_OBJECT";
	case CL_INVALID_BUFFER_SIZE: return "CL_INVALID_BUFFER_SIZE";
	case CL_INVALID_HOST_PTR: return "CL_INVALID_HOST_PTR";
	case CL_INVALID_PROGRAM: return "CL_INVALID_PROGRAM";
	case CL_INVALID_BINARY: return "CL_INVALID_BINARY";
	case CL_INVALID_BUILD_OPTIONS: return "CL_INVALID_BUILD_OPTIONS";
	case CL_INVALID_OPERATION: return "CL_INVALID_OPERATION";
	case CL_INVALID_ARG_INDEX: return "CL_INVALID_ARG_INDEX ";
	case CL_INVALID_ARG_VALUE: return "CL_INVALID_ARG_VALUE";
	case CL_INVALID_SAMPLER: return "CL_INVALID_SAMPLER";
	case CL_INVALID_ARG_SIZE: return "CL_INVALID_ARG_SIZE";
	case CL_INVALID_KERNEL_NAME: return "CL_INVALID_KERNEL_NAME";
	case CL_INVALID_KERNEL_DEFINITION: return "CL_INVALID_KERNEL_DEFINITION";
	case CL_INVALID_PLATFORM: return "CL_INVALID_PLATFORM";
	case CL_INVALID_DEVICE: return "CL_INVALID_DEVICE";
	case CL_INVALID_EVENT: return "CL_INVALID_EVENT";
	case CL_COMPILER_NOT_AVAILABLE: return "CL_COMPILER_NOT_AVAILABLE";
	case CL_BUILD_PROGRAM_FAILURE: return "CL_BUILD_PROGRAM_FAILURE";
	case CL_OUT_OF_RESOURCES: return "CL_OUT_OF_RESOURCES";
	case CL_MEM_OBJECT_ALLOCATION_FAILURE: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
	case CL_OUT_OF_HOST_MEMORY: return "CL_OUT_OF_HOST_MEMORY";
	case CL_DEVICE_NOT_AVAILABLE: return "CL_DEVICE_NOT_AVAILABLE";
	}
}
