/*
Author: Yakup Ipek
Library for initializing OpenCL Device and building program from Source
*/
#ifndef __MYOPENCL_LIB_H__
#define __MYOPENCL_LIB_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#ifdef _WIN32
#include <Share.h>
#endif // WIN32

#define MAX_SOURCE_SIZE 1024ul*1024ul
#define BUFFER_SIZE_FOR_BUILDING_PROGRAM 2048ul

#define CLU_ERRCHECK(__err, __message, ...) \
if(__err != CL_SUCCESS) { \
	fprintf(stderr, "OpenCL Assertion failure in %s#%d:\n", __FILE__, __LINE__); \
	fprintf(stderr, "Error code: %s\n", checkCLStatus(__err)); \
	fprintf(stderr, __message "\n", ##__VA_ARGS__); \
	exit(-1); \
}

#define QUOTE(str) #str
#define EXPAND_QUOTE(str) QUOTE(str)

void read_from_user_input_platform_id(int* id, int numplatforms);
void read_from_user_input_device_id(int* id, int numDevices);
void read_program_from_source(const char* file_name, char** source_str, size_t* sourceSize);
void choosePlatformID(cl_uint numplatforms, cl_platform_id* platforms, cl_int* id);
void chooseDeviceID(cl_uint numDevices, cl_device_id* devices, cl_platform_id platform, cl_int* id);
cl_device_id clInitWithDeviceQueue(cl_context* context, cl_command_queue* hostQueue, cl_command_queue* deviceQueue);
cl_device_id clInit(cl_context* context, cl_command_queue* queue);
cl_device_id clInitWithDeviceQueueByParam(cl_context* context, cl_command_queue* hostQueue, cl_command_queue* deviceQueue, int platform_id, int device_id);
cl_device_id clInitByParam(cl_context* context, cl_command_queue* queue, int platform_id, int device_id);
cl_ulong retrieveTotalExecutionTimeFromEvent(cl_event* event);
void myCLBuildProgram(cl_context context, cl_program* program, cl_device_id deviceId, const char* file_name, const char* option);
int needToCompileWithOCL2(cl_device_id id);
const char* checkCLStatus(cl_int status);

#endif // !__MYOPENCL_LIB_H__
