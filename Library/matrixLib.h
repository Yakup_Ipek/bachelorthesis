/*
Author: Yakup Ipek
Library with some functionalities for matrices
*/
#ifndef __MATRIX_LIB_H__
#define __MATRIX_LIB_H__
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>

#define RANDOM 10;

void fillMatrixRandomly(void* matrix, int size,const char* type);
void printMatrix(void* matrix, int length,const char* type, int breakAfterNElements,const char* label);
void verifyTwoMatrices(void* matrix_one, void* matrix_two, int size,const char* type);
void matrixMult(void* A, int rowA, int colA, void* B, int rowB, int colB, void* C,const char* type);
void sortMatrix(void* matrix, int size,const char* type);
int typeParser(const char* type);

#endif //__MATRIX_LIB_H__
