/*
Author: Yakup Ipek
Library to generate compute the Dijkstra-Algorithm
*/
#include "Dijkstra.h"

void initialize_dijkstra(Dijkstra* data, int num) {
	data->dist_to = (int*)malloc(sizeof(int)*num);
	data->edge_to = (int*)malloc(sizeof(int)*num);
	data->marked = (int*)malloc(sizeof(int)*num);
}

void free_dijkstra(Dijkstra* data) {
	free(data->dist_to);
	free(data->edge_to);
	free(data->marked);
}

void initialize(Graph* graph, Dijkstra* data, int source, std::list<int>& list) {
	for (int i = 0; i<graph->nodes_count; i++) {
		if (i == source) {
			data->dist_to[i] = 0;
			data->edge_to[i] = -1;	//has no parent
			list.push_back(i);
		}
		else {
			data->dist_to[i] = INT_MAX;
			data->edge_to[i] = -1;	//since the vertices are labeled with  natural positive numbers 
			list.push_back(i);
		}
	}
}

int get_node_with_min_distance(Dijkstra* data, std::list<int> &list) {
	std::list<int>::iterator it;
	int min = INT_MAX;
	int result=-1;	//if no minimum is found then -1 is returned
	for (it = list.begin(); it != list.end(); ++it) {
		if (min > data->dist_to[*it]) {
			min = data->dist_to[*it];
			result = *it;
		}
	}
	return result;
}

bool contains(std::list<int> list, int element) {
	std::list<int>::const_iterator it;
	for (it = list.begin(); it != list.end(); ++it) {
		if (element == *it) return true;
	}
	return false;
}

bool has_path_to_source(Dijkstra data, int source, int node) {
	int i;
	for (i = node; data.dist_to[i] != 0 && data.dist_to[i] != INT_MAX; i = data.edge_to[i]);	//follow path to the root
	if (i == source) return true;
	else return false;
}

void print_dijkstra_path(Dijkstra data, int node) {
	int i;
	std::cout << "Path to Node:" << node <<" [" ;
	std::list<std::tuple<int,int>> mylist;

	/*Add elements of the path to a list*/
	for (i = node; data.dist_to[i] != 0 && data.dist_to[i] != INT_MAX; i = data.edge_to[i]) {
		std::tuple<int, int>mytuple(i,data.dist_to[i]);
		mylist.push_front(mytuple);
 	}

	/*last element to add*/
	std::tuple<int, int>mytuple(i, data.dist_to[i]);
	mylist.push_front(mytuple);
	
	/*Print the path*/
	int count = 0;
	std::list<std::tuple<int,int>>::iterator it;
	for (it = mylist.begin(); it != mylist.end(); it++) {
		std::cout << "(N[" << std::get<0>(*it) << "]|W[" << std::get<1>(*it) << "])";
		if(count++ < mylist.size()-1){
			std::cout << "->";
		}
	}
	std::cout << "]\n\n";
}

void compute_dijkstra(Graph* graph, Dijkstra* data,int source) {
	std::list<int> mylist;
	initialize(graph, data, source, mylist);
	int node;
	while (!mylist.empty()) {
		node = get_node_with_min_distance(data,mylist);
		if (node == -1) {	//node is not connected, stop
			break;
		}
		else {
			mylist.remove(node);
			//printf("Removed Node: %d From List Whose Parent is: %d\n",node,data->edge_to[node]);
			for (int i = 0; i < graph->adjacency_list_count_per_node[node]; i++) {
				if (contains(mylist, (graph->adjacency_list[node])[i])) {
					int neighbor = (graph->adjacency_list[node])[i];
					int dist = data->dist_to[node] + (graph->weighting_list[node])[i];
					if (dist < data->dist_to[neighbor]) {
						//printf("I am Node: %d, and I am updating my Neighbor: %d, OLD: %d|NEW: %d\n",node,neighbor,data->dist_to[neighbor],dist);
						data->dist_to[neighbor] = dist;
						data->edge_to[neighbor] = node;
					}
				}
			}
		}

	}
}

