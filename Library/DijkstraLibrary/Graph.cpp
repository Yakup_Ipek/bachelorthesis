/*
Author: Yakup Ipek
Library to generate a directed graph
*/

#include "Graph.h"

void clean_graph(Graph* graph) {
	for (int i = 0; i < graph->nodes_count; i++) {
		free(*((graph->adjacency_list) + i));
		free(*((graph->weighting_list) + i));
	}
	free(graph->adjacency_list);
	free(graph->weighting_list);
	free(graph->adjacency_list_count_per_node);
	free(graph->nodes);
}

void print_adjacency_list(Graph graph) {
	printf("--------------Graph---------------\n");
	for (int i = 0; i < graph.nodes_count; i++) {
		printf("Node: %d =[", i);
		for (int j = 0; j < *((graph.adjacency_list_count_per_node) + i); j++) {
			if (j == (*((graph.adjacency_list_count_per_node) + i) - 1)) printf("(N:%d|W:%d)", *((*((graph.adjacency_list) + i)) + j), *((*((graph.weighting_list) + i)) + j));
			else printf("(N:%d|W:%d)-", *((*((graph.adjacency_list) + i)) + j), *((*((graph.weighting_list) + i)) + j));
		}
		printf("]\n\n");
	}
}

void generate_random_directed_graph(Graph* graph, int num_nodes, int max_num_of_neighbors, int min_num_of_neigbors, int weighting_range) {
	printf("Started generating directed graph ...\n");
	srand(time(NULL));

	assert(max_num_of_neighbors<num_nodes && num_nodes);	//number of nodes has to be greater zero and greater than the number of neighbors
	assert(max_num_of_neighbors >= min_num_of_neigbors && "maximum number of neighbours needs to be greater than minimum number of neighbors\n");	//minimum number of neighbors needs to be less than maximum number of neighbors
	assert(weighting_range<=1024 && "If weighing range is too great than an overflow might occur\n");

	int random_amount, connect_to, node_index = 0;
	graph->nodes_count = num_nodes;
	graph->nodes = (int*)malloc(sizeof(int)*num_nodes);
	graph->adjacency_list = (int**)malloc(sizeof(int*)*num_nodes);	//each node has an adjacency list
	graph->weighting_list = (int**)malloc(sizeof(int*)*num_nodes);	//each node has a weighting list
	graph->adjacency_list_count_per_node = (int*)malloc(sizeof(int)*num_nodes);	//each node has an adjacency list

																				/*   initialize nodes   */
	for (int i = 0; i < num_nodes; i++) {
		*((graph->nodes) + i) = i;
	}

	/* define neighbors  */
	for (int i = 0; i < num_nodes; i++) {
		printf("Progress: ");
		int mod = max_num_of_neighbors != 1 ? max_num_of_neighbors - 1 : max_num_of_neighbors;
		int r = rand() % (mod);
		if (max_num_of_neighbors == min_num_of_neigbors) {	//exactly min neighbors
			random_amount = min_num_of_neigbors;
		}
		else {
			if ((r + min_num_of_neigbors) > num_nodes) {
				random_amount = num_nodes - 1;
			}
			else {
				random_amount = r + min_num_of_neigbors;
			}
		}
		//printf("random_amount: %d, max: %d, min: %d\n", random_amount, max_num_of_neighbors, min_num_of_neigbors);
		*((graph->adjacency_list) + i) = (int*)malloc(sizeof(int)*random_amount);
		*((graph->weighting_list) + i) = (int*)malloc(sizeof(int)*random_amount);
		*((graph->adjacency_list_count_per_node) + i) = random_amount;
		/*  choose neighbors   */
		int* flag = (int*)calloc(num_nodes, sizeof(int));
		flag[i] = 1;
		for (int j = 0; j < random_amount; j++) {

			//avoid redundant neighbors
			connect_to = rand() % num_nodes;
			while (flag[connect_to]) { //if its marked, then start to increment, reason: random may take more time to find unmarked node
				if (connect_to == num_nodes - 1) connect_to = 0;
				else connect_to++;
			}
			flag[connect_to] = 1;	//mark
									//printf("Node: %d\tConnect_to: %d\n",i,connect_to);
			*(*((graph->adjacency_list) + i) + j) = connect_to;
			*(*((graph->weighting_list) + i) + j) = (rand() % weighting_range) + 1;	//define weighting on edges, at least one
		}
		printf("%0.0f %%\r", ((float)i / ((float)num_nodes - 1)) * 100);
		free(flag);
	}
	printf("\n");
	printf("Directed graph generated...\n\n");
}

int size_to_wrap_up(Graph graph) {
	int result = 0;
	for (int i = 0; i < graph.nodes_count; i++) {
		result += graph.adjacency_list_count_per_node[i];
	}
	return result;
}

void determine_starting_points(Graph graph, int** start) {
	for (int i = 0; i < graph.nodes_count; i++) {
		if (i == 0) {
			*((*start) + i) = 0;
		}
		else {
			*((*start) + i) = *((*start) + (i-1)) + *(graph.adjacency_list_count_per_node + (i - 1));
		}
	}
}

void wrap_neighbours_up(Graph graph, int** neighbours) {
	for (int i = 0, index=0; i < graph.nodes_count; i++) {
		for (int j = 0; j < graph.adjacency_list_count_per_node[i]; j++) {
			*((*neighbours) + (index++)) = *((*(graph.adjacency_list + i)) + j);
		}
	}
}

void wrap_weights_up(Graph graph, int** weights) {
	for (int i = 0, index = 0; i < graph.nodes_count; i++) {
		for (int j = 0; j < graph.adjacency_list_count_per_node[i]; j++) {
			*((*weights) + (index++)) = *((*(graph.weighting_list + i)) + j);
		}
	}
}

