/*
Author: Yakup Ipek
Library to generate a directed graph
*/

#pragma once
#ifndef __MYGRAPHWITHWEIGHTING_H__
#define __MYGRAPHWITHWEIGHTING_H__

#include <assert.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int* nodes;	//all nodes in graph
	int nodes_count;	//important in order to free memory
	int** adjacency_list; //list of successor node
	int** weighting_list; //list of weightings to other nodes
	int* adjacency_list_count_per_node; //necessary for printing the list
} Graph;

void generate_random_directed_graph(Graph* graph, int num_nodes, int max_num_of_neighbors, int min_num_of_neigbors, int weighting_range);
void print_adjacency_list(Graph graph);
void clean_graph(Graph* graph);
int size_to_wrap_up(Graph graph);
void wrap_neighbours_up(Graph graph, int** neighbours);
void wrap_weights_up(Graph graph, int** weights);
void determine_starting_points(Graph graph, int** start);

#endif