/*
Author: Yakup Ipek
Library to generate compute the Dijkstra-Algorithm
*/
#pragma once

#ifndef __DIJKSTRA_H__
#define __DIJKSTRA_H__

#include <iostream>
#include <list>
#include <tuple>
#include "Graph.h"
#define INT_MAX 2147483647

typedef struct {
	int* dist_to; //dist_to[v] number of edges to  node v
	int* edge_to; //edge_to[v] parent node of v
	int* marked;	//marked[v] is there a path to node v
} Dijkstra;

void initialize_dijkstra(Dijkstra* data, int num);

void free_dijkstra(Dijkstra* data);

void print_dijkstra_path(Dijkstra data, int node);

void compute_dijkstra(Graph* graph, Dijkstra* data, int source);

bool has_path_to_source(Dijkstra data, int source, int node);

#endif