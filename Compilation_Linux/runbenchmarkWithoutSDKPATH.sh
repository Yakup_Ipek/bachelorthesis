#!/bin/bash

echo [LOG] THIS IS A BENCHMARK FOR OPENCL 2.0

export RUN_PATH=$PWD
export LIBRARY_PATH=../Library
export EXTERN=../External_Library
export MYLIB="-I$LIBRARY_PATH -L$LIBRARY_PATH -lMatrixLib -lMyTime -lMyOpenCL"
export EXTERNLIB="-I$EXTERN -L$EXTERN -l_stb_image -l_stb_image_write"
export MY_BREADTH_FIRST_SEARCH_LIB="-I$LIBRARY_PATH/BreadthFirstSearchLibrary -L$LIBRARY_PATH/BreadthFirstSearchLibrary -lBreadthFirstSearch -lGraphReaderB -lmyGraph"
export MY_DIJKSTRA_LIB=" -I$LIBRARY_PATH/DijkstraLibrary  -L$LIBRARY_PATH/DijkstraLibrary  -lDijkstra -lDijkstraGraph"
export GRAPH_LIB="-I$LIBRARY_PATH/GraphWriterReader -L$LIBRARY_PATH/GraphWriterReader  -lGraphReader -lGraphWriter"

echo [LOG] CHECK WHETHER THERE IS ANY DEVICE WHICH SUPPORTS OPENCL 2.0 OR HIGHER 

g++ -std=c++11 SetEnvironmentVariable.cpp -o test -lOpenCL
./test

if [ $(ls . | grep -w "Configuration.sh" --count) -eq 1 ];
then
	source Configuration.sh
	echo $CL_PLATFORM_ID
	echo $CL_DEVICE_ID
	rm Configuration.sh
else
	echo -e "There is no OpenCL Device which supports OpenCL 2.0 or higher\n"
	sleep 1
	echo -e "If you whish to continue to run only the applications which require OpenCL 1.2 then press yes else no\n"
	echo 
	select yn in "Yes" "No";
	do
		case $yn in
			"Yes" ) 
				echo "You chose to continue"
				break
				;;
			"No" )
				echo "You chose to quit"
				exit;;
			* )
				echo "Invalid input; Terminating"
				exit;;
		esac
	done
	echo 
	if test "${DISPLAY_INFO+set}" != set; then
		export DISPLAY_INFO=1
	fi

	g++ -std=c++11 SetEnvironmentVariable.cpp -DDISPLAY_INFO=$DISPLAY_INFO -o test -lOpenCL 
	./test
	rm test

	while true; do
		read -p "Please enter the PLATFORM_ID analogue to the information above: " yn
		CL_PLATFORM_ID=$yn

		read -p "Please enter the DEVICE_ID analogue to the information above: " yn
		CL_DEVICE_ID=$yn

		echo -e "\n"
		echo "Do you want to keep these values?"
		echo -e "\nYou entered for CL_PLATFORM_ID:"  $CL_PLATFORM_ID
		echo -e "\nYou entered for CL_DEVICE_ID:"  $CL_DEVICE_ID
		echo -e "\n"
		Terminate=-1
		select yn in "Yes" "No" "QUIT";
		do
			case $yn in
				"Yes" ) 
					Terminate=1
					echo $Terminate
					break
					;;
				"No" )
					Terminate=0
					echo -e "Enter Again\n"
					break
					;;
				"QUIT" )
					echo "Quit"
					exit
					;;
				* )
					echo "Invalid input;"
					break;;
			esac
		done
		if [ $Terminate -eq 1 ]; then
			break
		fi
	done
fi

if test "${CL_PLATFORM_ID+set}" = set; then
		export CL_PLATFORM_ID
else
	echo "NO PLATFORM ID SET; EXIT"
	exit
fi

if test "${CL_DEVICE_ID+set}" = set; then
		export CL_DEVICE_ID
else
	echo "NO DEVICE ID SET; EXIT"
	exit
fi


echo [LOG] CREATE OBJECT FILES

g++ -std=c++11 -c -fpic $LIBRARY_PATH/matrixLib.cpp -o $LIBRARY_PATH/matrixLib.o
g++ -std=c++11 -c -fpic $LIBRARY_PATH/MyTime.cpp -o $LIBRARY_PATH/MyTime.o
g++ -std=c++11 -c -fpic $LIBRARY_PATH/myOpenCL_Lib.cpp -o $LIBRARY_PATH/myOpenCL_Lib.o
g++ -std=c++11 -c -fpic $LIBRARY_PATH/BreadthFirstSearchLibrary/BreadthFirstSearch.cpp -o $LIBRARY_PATH/BreadthFirstSearchLibrary/BreadthFirstSearch.o
g++ -std=c++11 -c -fpic $LIBRARY_PATH/BreadthFirstSearchLibrary/GraphReaderB.cpp -o $LIBRARY_PATH/BreadthFirstSearchLibrary/GraphReaderB.o
g++ -std=c++11 -c -fpic $LIBRARY_PATH/BreadthFirstSearchLibrary/myGraph.cpp -o $LIBRARY_PATH/BreadthFirstSearchLibrary/myGraph.o
g++ -std=c++11 -c -fpic $LIBRARY_PATH/DijkstraLibrary/Dijkstra.cpp -o $LIBRARY_PATH/DijkstraLibrary/Dijkstra.o
g++ -std=c++11 -c -fpic $LIBRARY_PATH/DijkstraLibrary/Graph.cpp -o $LIBRARY_PATH/DijkstraLibrary/DijkstraGraph.o
g++ -std=c++11 -c -fpic $LIBRARY_PATH/GraphWriterReader/GraphReader.cpp -o $LIBRARY_PATH/GraphWriterReader/GraphReader.o
g++ -std=c++11 -c -fpic $LIBRARY_PATH/GraphWriterReader/GraphWriter.cpp -o $LIBRARY_PATH/GraphWriterReader/GraphWriter.o
g++ -std=c++11 -c -fpic $EXTERN/stb_image.c -o $EXTERN/stb_image.o
g++ -std=c++11 -c -fpic $EXTERN/stb_image_write.c -o $EXTERN/stb_image_write.o

echo [LOG] CREATE SHARED LIBRARIES

g++ -std=c++11 -shared -o $LIBRARY_PATH/libMatrixLib.so $LIBRARY_PATH/matrixLib.o 
g++ -std=c++11 -shared -o $LIBRARY_PATH/libMyTime.so $LIBRARY_PATH/MyTime.o
g++ -std=c++11 -shared -o $LIBRARY_PATH/libMyOpenCL.so $LIBRARY_PATH/myOpenCL_Lib.o 
g++ -std=c++11 -shared -o $LIBRARY_PATH/BreadthFirstSearchLibrary/libBreadthFirstSearch.so $LIBRARY_PATH/BreadthFirstSearchLibrary/BreadthFirstSearch.o
g++ -std=c++11 -shared -o $LIBRARY_PATH/BreadthFirstSearchLibrary/libGraphReaderB.so $LIBRARY_PATH/BreadthFirstSearchLibrary/GraphReaderB.o
g++ -std=c++11 -shared -o $LIBRARY_PATH/BreadthFirstSearchLibrary/libmyGraph.so $LIBRARY_PATH/BreadthFirstSearchLibrary/myGraph.o
g++ -std=c++11 -shared -o $LIBRARY_PATH/DijkstraLibrary/libDijkstra.so $LIBRARY_PATH/DijkstraLibrary/Dijkstra.o
g++ -std=c++11 -shared -o $LIBRARY_PATH/DijkstraLibrary/libDijkstraGraph.so $LIBRARY_PATH/DijkstraLibrary/DijkstraGraph.o
g++ -std=c++11 -shared -o $LIBRARY_PATH/GraphWriterReader/libGraphReader.so $LIBRARY_PATH/GraphWriterReader/GraphReader.o
g++ -std=c++11 -shared -o $LIBRARY_PATH/GraphWriterReader/libGraphWriter.so $LIBRARY_PATH/GraphWriterReader/GraphWriter.o
g++ -std=c++11 -shared -o $EXTERN/lib_stb_image.so $EXTERN/stb_image.o
g++ -std=c++11 -shared -o $EXTERN/lib_stb_image_write.so $EXTERN/stb_image_write.o

echo [LOG] SET_LD_LIBRARY_PATH

export LD_LIBRARY_PATH=$PWD/../Library:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$PWD/../Library/BreadthFirstSearchLibrary:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$PWD/../Library/DijkstraLibrary:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$PWD/../Library/GraphWriterReader:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$PWD/$EXTERN:$LD_LIBRARY_PATH

echo [LOG] REMOVE OBJECT FILES

rm -r $LIBRARY_PATH/*.o
rm -r $LIBRARY_PATH/BreadthFirstSearchLibrary/*.o
rm -r $LIBRARY_PATH/DijkstraLibrary/*.o
rm -r $LIBRARY_PATH/GraphWriterReader/*.o
rm -r $EXTERN/*.o

echo [LOG] START COMPILING

# -w -e "BitonicSort" -w -e "BreadthFirstSearch" -w -e "CalculationOfPi" -w -e "GaussianBlur" -w -e "Matrix_Multiplication" -w -e "JacobiIteration" -w -e "Dijkstra"
for i in $(find ../ -maxdepth 2 | grep -w -e "BitonicSort" -w -e "BreadthFirstSearch" -w -e "CalculationOfPi" -w -e "GaussianBlur" -w -e "Matrix_Multiplication" -w -e "JacobiIteration" -w -e "Dijkstra")
do
  if [ $(find $i -maxdepth 1 -type d | wc -l) -eq 1 -o $(find $i -maxdepth 1 -type d | wc -l) -eq 3 ] 
  then
	if [ $(ls $i | wc -l) -eq 2 -o $(ls $i | wc -l) -eq 4 ]
	then
		g++ -std=c++11 $i/main.cpp -o $i/run -lOpenCL $MYLIB $MY_DIJKSTRA_LIB $GRAPH_LIB $MY_BREADTH_FIRST_SEARCH_LIB  $EXTERNLIB -DCL_PLATFORM_ID=$CL_PLATFORM_ID -DCL_DEVICE_ID=$CL_DEVICE_ID -g
	fi
  fi
done

echo [LOG] START RUNNING

for i in $(find ../ -maxdepth 3 | grep -w "run")
do
  echo [__________________Running Application____________ $i _____________]
  cd $(dirname $i)
  ./run
  rm run
  cd $RUN_PATH
done

cd $RUN_PATH

unset LD_LIBRARY_PATH
rm -r $LIBRARY_PATH/*.so
rm -r $LIBRARY_PATH/BreadthFirstSearchLibrary/*.so
rm -r $LIBRARY_PATH/DijkstraLibrary/*.so
rm -r $LIBRARY_PATH/GraphWriterReader/*.so
rm -r $EXTERN/*.so

echo [LOG] BENCHMARK IS FINISHED
