#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#ifndef DISPLAY_INFO
	#define DISPLAY_INFO 0
#endif

int main(void){
	
	char* value;
	size_t valueSize;
	cl_uint platformCount;
	cl_platform_id* platforms;
	cl_uint deviceCount;
	cl_device_id* devices;
	cl_uint maxComputeUnits;
	size_t buff_size = 256;
	char* openCL_buffer = (char*)malloc(sizeof(char)*buff_size);
	int compatible_device_available = 0;
	
	// get all platforms
	clGetPlatformIDs(0, NULL, &platformCount);
	platforms = (cl_platform_id*)malloc(sizeof(cl_platform_id) * platformCount);
	clGetPlatformIDs(platformCount, platforms, NULL);
	
	//printf("______________________________________________________________\n\n");
	for (int i = 0; i < platformCount; i++) {

		// get all devices
		clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, 0, NULL, &deviceCount);
		devices = (cl_device_id*)malloc(sizeof(cl_device_id) * deviceCount);
		clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, deviceCount, devices, NULL);
	
		
		for (int j = 0; j < deviceCount; j++) {
			if(DISPLAY_INFO != 0){
				printf("Platform_ID: %d\n", i);
				printf("Device_ID: %d\n", j);
				printf("\nSome details are printed concerning the device\n");
				// print device name
				clGetDeviceInfo(devices[j], CL_DEVICE_NAME, 0, NULL, &valueSize);
				value = (char*)malloc(valueSize);
				clGetDeviceInfo(devices[j], CL_DEVICE_NAME, valueSize, value, NULL);
				printf("\tDevice: %s\n",value);
				free(value);

				// print hardware device version
				clGetDeviceInfo(devices[j], CL_DEVICE_VERSION, 0, NULL, &valueSize);
				value = (char*)malloc(valueSize);
				clGetDeviceInfo(devices[j], CL_DEVICE_VERSION, valueSize, value, NULL);
				printf("\t%d.%d] Hardware version: %s\n",j,1, value);
				free(value);

				// print software driver version
				clGetDeviceInfo(devices[j], CL_DRIVER_VERSION, 0, NULL, &valueSize);
				value = (char*)malloc(valueSize);
				clGetDeviceInfo(devices[j], CL_DRIVER_VERSION, valueSize, value, NULL);
				printf("\t%d.%d] Software version: %s\n",j,2, value);
				free(value);

				// print c version supported by compiler for device
				clGetDeviceInfo(devices[j], CL_DEVICE_OPENCL_C_VERSION, 0, NULL, &valueSize);
				value = (char*)malloc(valueSize);
				clGetDeviceInfo(devices[j], CL_DEVICE_OPENCL_C_VERSION, valueSize, value, NULL);
				printf("\t%d.%d] OpenCL C version: %s\n",j,3, value);
				free(value);

				// print parallel compute units
				clGetDeviceInfo(devices[j], CL_DEVICE_MAX_COMPUTE_UNITS,sizeof(maxComputeUnits), &maxComputeUnits, NULL);
				printf("\t%d.%d] Parallel compute units: %d\n",j,4, maxComputeUnits);
			}else{
				
				clGetDeviceInfo(devices[j], CL_DEVICE_OPENCL_C_VERSION, buff_size, openCL_buffer, NULL);
				int is_compatible = strstr(openCL_buffer, "OpenCL C 2.0") != NULL ? 1 : 0;
				is_compatible |= strstr(openCL_buffer, "OpenCL C 2.1") != NULL ? 1 : 0;
				is_compatible |= strstr(openCL_buffer, "OpenCL C 2.2") != NULL ? 1 : 0;
				
				if(is_compatible){
					
					FILE* file;
					file = fopen("Configuration.sh","w");
					//set environment variable
					
					if(file==NULL){
						fprintf(stderr,"Could not create file");
						exit(-1);
					}
					
					char buffer[128];
					if(sprintf(buffer,"CL_PLATFORM_ID=%d\n",i)==0){
						fprintf(stderr,"Error writing platform id to buffer\n");
						exit(-1);
					}
					if(fprintf(file,"%s",buffer)==0){
						fprintf(stderr,"Error writing platform id to file\n");
						exit(-1);
					}
					if(sprintf(buffer,"CL_DEVICE_ID=%d\n",j)==0){
						fprintf(stderr,"Error writing device id to buffer\n");
						exit(-1);
					}
					if(fprintf(file,"%s",buffer)==0){
						fprintf(stderr,"Error writing device id to file\n");
						exit(-1);
					}
					compatible_device_available = 1;
					
					
					fclose(file);
					break;
				}
			}
			
		}
		if(compatible_device_available) break;
		
		free(devices);
		printf("______________________________________________________________\n\n");
	}
	
	//if(!compatible_device_available) printf("There is no OpenCL device which supports Version 2.0 or higher\n");
	
	free(openCL_buffer);
	free(platforms);
	
	return EXIT_SUCCESS;
}
