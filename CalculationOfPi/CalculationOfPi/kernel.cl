/*
Author: Yakup Ipek
Pi Kernel for OpenCL 1.X
*/

__kernel void pi_step_one(__global VALUE *output){
	int global_index = get_global_id(0);
	VALUE numerator = (VALUE)pown((VALUE)-1,global_index);
	VALUE denominator = 2*global_index+1;
	VALUE result = numerator/denominator;
	output[global_index]=result;
}

__kernel void pi_step_two(__global VALUE* input,__global VALUE *output,__local VALUE* localMem, int boundary){
	int global_index=get_global_id(0);
	int local_index=get_local_id(0);

	if(global_index < boundary){
		localMem[local_index] = input[global_index];
	}else{ //identity element for sum operation is 0
		localMem[local_index] = (VALUE) 0;
	}
	barrier(CLK_LOCAL_MEM_FENCE);

	for(int offset = get_local_size(0)/2; offset > 0; offset >>= 1) {

		if(local_index < offset) {
			VALUE other = localMem[local_index+ offset];
			VALUE mine = localMem[local_index];
			localMem[local_index] = mine + other;
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	if(local_index== 0) { output[get_group_id(0)] = localMem[0]; }
}

