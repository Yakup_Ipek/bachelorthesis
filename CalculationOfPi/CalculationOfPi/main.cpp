/*
Author: Yakup Ipek
Pi Computation in Parallel for OpenCL 1.X
*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include "matrixLib.h"
#include "myOpenCL_Lib.h"

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

#define VALUE float
#define SIZE 1000000
#define LOCALWORKSIZE 1024
#define GLOBALWORKSIZE(x) roundUp(x,LOCALWORKSIZE)

#define ISPOWEROFTWO(a)\
if(!((a != 0) && ((a & (~a + 1)) == a))) { \
	fprintf(stderr, "The Size Variable needs to be a power of 2\n"); \
	exit(-1); \
}

#define KERNEL_FILE_NAME "./kernel.cl"

typedef struct {
	cl_context context;
	cl_command_queue command_queue;
	cl_device_id device_id;
	cl_program clProgram;
	cl_kernel clKernel, clKernel_2;
}_ocl_;

typedef struct {
	cl_ulong total_time;
} _time_;


/* used to define globalworksize, remainder of division has to be zero*/
int roundUp(int globalSize, int localSize);
/* used for comparison of result*/
void calcPiSequential(VALUE* result);
/* recursively calculating reduction*/
void reductionStep(_ocl_* cl, VALUE* in, VALUE* out, int numberOfElements, _time_* time);

int main(void) {
	ISPOWEROFTWO(LOCALWORKSIZE)

	_ocl_ clData;
	_time_ time;
	cl_int status;
	cl_event event;
	size_t globalWorkSize_FirstStep[1] = { SIZE };
	//clData.device_id= clInit(&clData.context, &clData.command_queue);
	clData.device_id= clInitByParam(&clData.context, &clData.command_queue, CL_PLATFORM_ID, CL_DEVICE_ID);

	/*-----------Building program-----------*/
	char option[128];
	sprintf(option, "-DVALUE=%s", EXPAND_QUOTE(VALUE));
	myCLBuildProgram(clData.context, &clData.clProgram, clData.device_id, KERNEL_FILE_NAME, option);
	clData.clKernel = clCreateKernel(clData.clProgram, "pi_step_one", &status);
	CLU_ERRCHECK(status, "Error creating kernel_1 for pi calculation");
	clData.clKernel_2 = clCreateKernel(clData.clProgram, "pi_step_two", &status);
	CLU_ERRCHECK(status, "Error creating kernel_2 for pi calculation");

	/*-----------Memory Objects----------*/
	cl_mem mem_output = clCreateBuffer(clData.context, CL_MEM_READ_WRITE, SIZE*sizeof(VALUE), NULL, &status);
	CLU_ERRCHECK(status, "Error creating output memory buffer");
	cl_mem mem_result = clCreateBuffer(clData.context, CL_MEM_READ_WRITE, GLOBALWORKSIZE(SIZE) / LOCALWORKSIZE*sizeof(VALUE), NULL, &status);
	CLU_ERRCHECK(status, "Error creating result memory buffer");

	VALUE* output = (VALUE*) malloc(sizeof(VALUE)*SIZE);
	VALUE* result = (VALUE*)calloc(GLOBALWORKSIZE(SIZE) / LOCALWORKSIZE,sizeof(VALUE));
	VALUE* result_seq = (VALUE*)calloc(1, sizeof(VALUE));
	
	/*-------------First Kernel Function--------------*/
	CLU_ERRCHECK(clSetKernelArg(clData.clKernel, 0, sizeof(cl_mem), (void *)&mem_output), "Error while setting argument: mem_output");

	CLU_ERRCHECK(clEnqueueNDRangeKernel(clData.command_queue, clData.clKernel, 1, NULL, globalWorkSize_FirstStep, NULL, 0, NULL, &event), "Failed to enqueue 2D kernel");
	
	time.total_time = retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clEnqueueReadBuffer(clData.command_queue, mem_output, CL_TRUE, 0, SIZE*sizeof(VALUE), output, 0, NULL, &event), "Error reading buffer A");
	
	time.total_time += retrieveTotalExecutionTimeFromEvent(&event);

	/*-------------Second Kernel Function --------------*/
	reductionStep(&clData, output, result, SIZE, &time);

	printf("\nTotal time in nanoseconds = %llu\n", time.total_time);

	/*-------------Pi Calculation Sequential---------*/
	calcPiSequential(result_seq);

	/*Last step of the algorithm is to multiply the result value by 4*/
	result[0] *= 4;
	result_seq[0] *= 4;

	/*-------------Printing Result-----------------*/
	printMatrix(result, 1, EXPAND_QUOTE(VALUE), 1, "Result(Parallel)");
	printMatrix(result_seq, 1, EXPAND_QUOTE(VALUE), 1, "Result(Sequential)");

	//free host resources
	status = clReleaseContext(clData.context);
	status |= clReleaseCommandQueue(clData.command_queue);
	status |= clReleaseProgram(clData.clProgram);
	status |= clReleaseKernel(clData.clKernel);
	status |= clReleaseKernel(clData.clKernel_2);
	status |= clReleaseMemObject(mem_output);
	status |= clReleaseMemObject(mem_result);
	CLU_ERRCHECK(status, "Error while releasing ocl data");
}

int roundUp(int globalSize, int localSize) {
	if (globalSize%localSize == 0)return globalSize;
	else return globalSize + (localSize - (globalSize%localSize));
}

void reductionStep(_ocl_* cl, VALUE* in, VALUE* out, int numberOfElements, _time_* time) {

	int numberOfWorkgroups = GLOBALWORKSIZE(numberOfElements) / LOCALWORKSIZE;
	size_t localWorkSize[1] = { LOCALWORKSIZE };
	size_t  globalWorkSize_SecondStep[1] = {(size_t) GLOBALWORKSIZE(numberOfElements) };
	cl_event event;
	cl_int status;
	int boundary = numberOfElements;
	
	//printf("ElementsToProcess: %d\tGlobalWorkSize: %d\tLocalWorkSize: %d\n", numberOfElements,GLOBALWORKSIZE(numberOfElements), LOCALWORKSIZE);
	//printf("NumberOfWorkgroups: %d\n",numberOfWorkgroups);
	
	/*--------------Creating memory objects-------------*/
	cl_mem mem_in = clCreateBuffer(cl->context, CL_MEM_READ_WRITE, numberOfElements*sizeof(VALUE),NULL, &status);
	CLU_ERRCHECK(status, "Error creating input memory buffer");

	cl_mem mem_out = clCreateBuffer(cl->context, CL_MEM_READ_WRITE, numberOfWorkgroups*sizeof(VALUE), NULL, &status);
	CLU_ERRCHECK(status, "Error creating output memory buffer");

	CLU_ERRCHECK(clEnqueueWriteBuffer(cl->command_queue, mem_in, CL_TRUE, 0, numberOfElements*sizeof(VALUE), in, 0, NULL, &event), "Error reading buffer A");
	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

	/*------Settting arguments-----------*/
	CLU_ERRCHECK(clSetKernelArg(cl->clKernel_2, 0, sizeof(cl_mem), (void *)&mem_in), "Error while setting argument: mem_in");
	CLU_ERRCHECK(clSetKernelArg(cl->clKernel_2, 1, sizeof(cl_mem), (void *)&mem_out), "Error while setting argument: mem_out");
	CLU_ERRCHECK(clSetKernelArg(cl->clKernel_2, 2, sizeof(VALUE)*LOCALWORKSIZE, NULL), "Error while setting argument: local Memory");
	CLU_ERRCHECK(clSetKernelArg(cl->clKernel_2, 3, sizeof(int), &boundary), "Error while setting argument: boundary");

	CLU_ERRCHECK(clEnqueueNDRangeKernel(cl->command_queue, cl->clKernel_2, 1, NULL, globalWorkSize_SecondStep, localWorkSize, 0, NULL, &event), "Failed to enqueue 2D kernel");

	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

	CLU_ERRCHECK(clEnqueueReadBuffer(cl->command_queue, mem_out, CL_TRUE, 0, numberOfWorkgroups*sizeof(VALUE), out, 0, NULL, &event), "Error reading buffer A");

	time->total_time += retrieveTotalExecutionTimeFromEvent(&event);

	if (numberOfWorkgroups > 1) {
		reductionStep(cl, out, out, numberOfWorkgroups, time);
	}

	/*------------free host resources-----------*/
	status = clReleaseMemObject(mem_out);
	status |= clReleaseMemObject(mem_in);
	CLU_ERRCHECK(status, "Error while releasing ocl data");
}

void calcPiSequential(VALUE* result) {
	for (int i = 0; i < SIZE; i++) {
		*result += (VALUE)pow(-1, i) / (VALUE)(2 * i + 1);
	}
}
