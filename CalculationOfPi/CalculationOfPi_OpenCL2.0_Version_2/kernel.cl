/*
Author: Yakup Ipek
Pi Kernel OpenCL 2.0 Version_2 (used built-in function for reduction)
*/


int roundUp(int globalSize, int localSize) {
	if (globalSize%localSize == 0)return globalSize;
	else return globalSize + (localSize - (globalSize%localSize));
}

__kernel void pi_step_two(__global VALUE* input,__global VALUE *output, int boundary, int counter);

__kernel void continue_pi(__global VALUE* input, __global VALUE *output, int boundary, int counter){
	int global_index = get_global_id(0);
	input[global_index] = output[global_index];
	
	if(global_index==0 && counter<NUMOFREDUCTIONCALLS){
		boundary = roundUp(boundary,LOCALWORKSIZE)/LOCALWORKSIZE;
		ndrange_t ndrange = ndrange_1D(roundUp(boundary,LOCALWORKSIZE), LOCALWORKSIZE);
		//printf("Boundary: %d, NumberThreads: %d\n",boundary,roundUp(boundary,LOCALWORKSIZE));
		void (^my_block_one)(void)=^{pi_step_two(input,output,boundary,counter);};
		enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,ndrange,my_block_one);
	}

}

__kernel void pi_step_two(__global VALUE* input,__global VALUE *output, int boundary, int counter){
	int global_index=get_global_id(0);
	int local_index=get_local_id(0);
	int group_offset = get_group_id(0)*LOCALWORKSIZE;
	VALUE result;
	
	if((group_offset+local_index)>=boundary){
		input[group_offset+local_index]=0;
	}
	
	barrier(CLK_GLOBAL_MEM_FENCE);

	result = work_group_reduce_add(input[group_offset+local_index]);
	
	if(local_index == 0){
		 output[get_group_id(0)] = result;
	}

	if(global_index==0){
		counter++;
		ndrange_t ndrange =ndrange_1D(get_global_size(0));
		void (^my_block_one) (void)=^{continue_pi(input, output,boundary,counter);};
		enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,ndrange,my_block_one);
	}
	
}



__kernel void pi_step_one(__global VALUE *input, __global VALUE* output)
{
	int global_index = get_global_id(0);
	VALUE numerator = (VALUE)pow((VALUE)-1,global_index);
	VALUE denominator = 2*global_index+1;
	VALUE result = numerator/denominator;
	input[global_index]=result;

	if(global_index==0){
		int boundary=get_global_size(0);
		ndrange_t ndrange = ndrange_1D(roundUp(boundary,LOCALWORKSIZE), LOCALWORKSIZE);
		//printf("Boundary: %d, NumberThreads: %d\n",boundary,roundUp(boundary,LOCALWORKSIZE));
		void (^my_block_one)(void)=^{pi_step_two(input,output,boundary,0);};
		enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,ndrange,my_block_one);
	}
	
}

