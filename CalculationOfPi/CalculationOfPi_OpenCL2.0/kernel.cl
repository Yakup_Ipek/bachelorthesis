/*
Author: Yakup Ipek
Pi Kernel OpenCL 2.0
*/


int roundUp(int globalSize, int localSize) {
	if (globalSize%localSize == 0)return globalSize;
	else return globalSize + (localSize - (globalSize%localSize));
}

__kernel void copy_values(__global VALUE* input,__global VALUE *output){
	int global_index = get_global_id(0);
	input[global_index] = output[global_index];
}

__kernel void pi_step_two(__global VALUE* input,__global VALUE *output,__local VALUE* localMem, int boundary)
{
	int global_index=get_global_id(0);
	int local_index=get_local_id(0);
	int group_index = get_group_id(0);
	int number_of_work_groups = get_num_groups(0);
	
	if(global_index < boundary){
		localMem[local_index] = input[global_index];
	}else{ //identity element for sum operation is 0
		localMem[local_index] = (VALUE) 0;
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	for(int offset = get_local_size(0)/2; offset > 0; offset >>= 1) {

		if(local_index < offset) { 
			localMem[local_index] += localMem[local_index+ offset];
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	if(local_index== 0) { 
		output[group_index] = localMem[local_index];
		//printf("Result: %f\n\n",output[get_group_id(0)]);
	}

	if(global_index == 0){
		ndrange_t ndrange = ndrange_1D(number_of_work_groups); 
		void (^my_block_one)(void)=^{copy_values(input,output);};
		enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,ndrange,my_block_one);
	}
	
}

__kernel void pi_step_one(__global VALUE *input, __global VALUE* output)
{
	int global_index = get_global_id(0);
	VALUE numerator = (VALUE)pow((VALUE)-1,global_index);
	VALUE denominator = 2*global_index+1;
	VALUE result = numerator/denominator;
	input[global_index]=result;

	if(global_index==0){
		
		int n=get_global_size(0);
		uint localMemSize = sizeof(VALUE)*LOCALWORKSIZE;
		ndrange_t ndrange; 
		int boundary=0;
		
		//important: since each step has to be performed in order, event based synchronization is mandatory
		clk_event_t evt[NUMOFREDUCTIONCALLS];
		for(int i=0; i<NUMOFREDUCTIONCALLS; i++){
				
			if(i==0){	//the boundary of the first reduction step is the n of sigma 
						
				/*------------number of threads-----------*/
				ndrange =ndrange_1D(roundUp(n,LOCALWORKSIZE), LOCALWORKSIZE);
				
				/*------------block variable: boundary is set to the number of elements-----------*/
				void (^my_block_one)(local void*)=^(local void* local_memory){pi_step_two(input,output,(local VALUE*) local_memory,n);};
		
				enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,ndrange,0,NULL,evt+i,my_block_one,localMemSize);

			}else{	//the boundary is the number of workgroups (since each workgroup reduces problem to one element)
			
				boundary = roundUp(n,LOCALWORKSIZE)/LOCALWORKSIZE;
					
				/*------------number of threads-----------*/
				ndrange =ndrange_1D(roundUp(boundary,LOCALWORKSIZE), LOCALWORKSIZE);

				/*------------boundary is set to number of workgroups-----------*/
				void (^my_block_two)(local void*)=^(local void* local_memory){pi_step_two(input,output,(local VALUE*) local_memory,boundary);};
							
				/*-------------execution has to be carried out in correct order, done by using events*/
				enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,ndrange,1,evt+(i-1),evt+i,my_block_two,localMemSize);
	
				n=boundary;			
			}
			release_event(evt[i]);
		}
	}
	
}

