/*
Author: Yakup Ipek
Pi Computation in Parallel for OpenCL 2.0,
Used Features: Dynamic Parallelism, Shared Virtual Memory
Info: enqueue_kernel call (Device) with on event argument
*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include "myOpenCL_Lib.h"
#include "matrixLib.h"

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

#define VALUE float
#define SIZE 150000000

#define LOCALWORKSIZE 1024
#define GLOBALWORKSIZE(x) roundUp(x,LOCALWORKSIZE)
#define NUMOFREDUCTIONCALLS numberOfReductionCalls()
#define ISPOWEROFTWO(a)\
if(!((a != 0) && ((a & (~a + 1)) == a))) { \
	fprintf(stderr, "The Size Variable needs to be a power of 2\n"); \
	exit(-1); \
}

#define KERNEL_FILE_NAME "./kernel.cl"

typedef struct {
	cl_context context;
	cl_command_queue command_queue;
	cl_command_queue device_queue;
	cl_device_id device_id;
	cl_program clProgram;
	cl_kernel clKernel;
}_ocl_;

typedef struct {
	cl_ulong total_time;
} _time_;

/* used to define globalworksize, remainder of division has to be zero*/
int roundUp(int globalSize, int localSize);
/* calculate number of reduction calls, which is passed to the kernel as a preprocessor option*/
int numberOfReductionCalls();
/* used for comparison of result*/
void calcPiSequential(VALUE* result);


int main(void) {

	ISPOWEROFTWO(LOCALWORKSIZE);

	_ocl_ clData;
	_time_ time;
	cl_int status;
	cl_event event;
	size_t globalWorkSize_FirstStep[1] = { SIZE };
	//clData.device_id = clInitWithDeviceQueue(&clData.context, &clData.command_queue, &clData.device_queue);
	clData.device_id = clInitWithDeviceQueueByParam(&clData.context, &clData.command_queue, &clData.device_queue, CL_PLATFORM_ID, CL_DEVICE_ID);

	/*------------- Build program and define kernel--------------*/
	char option[128];
	sprintf(option, "-DVALUE=%s -DLOCALWORKSIZE=%i -DNUMOFREDUCTIONCALLS=%i", EXPAND_QUOTE(VALUE), LOCALWORKSIZE, NUMOFREDUCTIONCALLS);
	myCLBuildProgram(clData.context, &clData.clProgram, clData.device_id, KERNEL_FILE_NAME, option);
	clData.clKernel = clCreateKernel(clData.clProgram, "pi_step_one", &status);
	CLU_ERRCHECK(status, "Error creating kernel_1 for pi calculation");

	/*------------- Shared virtual memory (instead of memObject)--------------*/
	VALUE* input = (VALUE*)clSVMAlloc(clData.context, CL_MEM_READ_WRITE | CL_MEM_SVM_FINE_GRAIN_BUFFER, GLOBALWORKSIZE(SIZE)*sizeof(VALUE), 0);
	VALUE* result = (VALUE*)clSVMAlloc(clData.context, CL_MEM_READ_WRITE | CL_MEM_SVM_FINE_GRAIN_BUFFER, GLOBALWORKSIZE(SIZE)*sizeof(VALUE), 0);
	VALUE* result_seq = (VALUE*)calloc(1, sizeof(VALUE));
	
	/*------------- Setting Arguments--------------*/
	clSetKernelArgSVMPointer(clData.clKernel, 0, input);
	clSetKernelArgSVMPointer(clData.clKernel, 1, result);

	CLU_ERRCHECK(clEnqueueNDRangeKernel(clData.command_queue, clData.clKernel, 1, NULL, globalWorkSize_FirstStep, NULL, 0, NULL, &event), "Failed to enqueue 2D kernel");

	time.total_time = retrieveTotalExecutionTimeFromEvent(&event);

	printf("\nTotal time in nanoseconds = %llu\n", time.total_time);

	/*-------------Pi Calculation Sequential---------*/
	calcPiSequential(result_seq);

	/*Last step of the algorithm is to multiply the result value by 4*/
	result[0] *= (VALUE)4;
	result_seq[0] *= (VALUE)4;

	/*------------- Print out result --------------*/
	printMatrix(result, 1, EXPAND_QUOTE(VALUE), 1, "Result(Parallel)");
	printMatrix(result_seq, 1, EXPAND_QUOTE(VALUE), 1, "Result(Sequential)");

	//free host resources
	clSVMFree(clData.context, input);
	clSVMFree(clData.context, result);
	status = clReleaseContext(clData.context);
	status |= clReleaseCommandQueue(clData.command_queue);
	status |= clReleaseCommandQueue(clData.device_queue);
	status |= clReleaseProgram(clData.clProgram);
	status |= clReleaseKernel(clData.clKernel);
	CLU_ERRCHECK(status, "Error while releasing ocl data");
}

int roundUp(int globalSize, int localSize) {
	if (globalSize%localSize == 0)return globalSize;
	else return globalSize + (localSize - (globalSize%localSize));
}

int numberOfReductionCalls() {
	int count = 1;
	int numberOfWorkGroup = SIZE;
	do {
		numberOfWorkGroup = GLOBALWORKSIZE(numberOfWorkGroup) / LOCALWORKSIZE;
		if (numberOfWorkGroup == 1) {	//problem fits into one workgroup
			break;
		}
		else {
			count++;
			continue;
		}
	} while (true);

	return count;
}

void calcPiSequential(VALUE* result) {
	for (int i = 0; i < SIZE; i++) {
		*result += (VALUE)pow(-1, i) / (VALUE)(2 * i + 1);
	}
}
