/*
Author: Yakup Ipek
Pi Kernel OpenCL 2.0, Enqueue_Kernel with no Events
*/

int roundUp(int globalSize, int localSize) {
	if (globalSize%localSize == 0)return globalSize;
	else return globalSize + (localSize - (globalSize%localSize));
}

__kernel void pi_step_two(__global VALUE* input,__global VALUE *output,__local VALUE* localMem , int boundary, int counter);

__kernel void continue_pi(__global VALUE* input, __global VALUE *output, int boundary, int counter){
	int global_index = get_global_id(0);
	input[global_index] = output[global_index];
	
	if(global_index==0 && counter<NUMOFREDUCTIONCALLS){
		boundary = roundUp(boundary,LOCALWORKSIZE)/LOCALWORKSIZE;
		ndrange_t ndrange = ndrange_1D(roundUp(boundary,LOCALWORKSIZE), LOCALWORKSIZE);
		uint localMemSize = sizeof(VALUE)*LOCALWORKSIZE;
		void (^my_block_one)(local void*)=^(local void* local_memory){pi_step_two(input,output,(local VALUE*) local_memory,boundary,counter);};
		enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,ndrange,my_block_one,localMemSize);
	}

}

__kernel void pi_step_two(__global VALUE* input,__global VALUE *output,__local VALUE* localMem, int boundary, int counter)
{
	int global_index=get_global_id(0);
	int local_index=get_local_id(0);
	
	if(global_index < boundary){
		localMem[local_index] = input[global_index];
	}else{ //identity element for sum operation is 0
		localMem[local_index] = (VALUE) 0;
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	for(int offset = get_local_size(0)/2; offset > 0; offset >>= 1) {

		if(local_index < offset) { 
			localMem[local_index] += localMem[local_index + offset];
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	if(local_index== 0) { 
		output[get_group_id(0)] = localMem[0];
		//printf("Result: %f\n\n",output[get_group_id(0)]);
	}
	
	if(global_index==0){
		counter++;
		ndrange_t ndrange =ndrange_1D(get_global_size(0));
		void (^my_block_one) (void)=^{continue_pi(input, output,boundary,counter);};
		enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,ndrange,my_block_one);
	}
}


__kernel void pi_step_one(__global VALUE *input, __global VALUE* output)
{
	int global_index = get_global_id(0);
	VALUE numerator = (VALUE)pow((VALUE)-1,global_index);
	VALUE denominator = 2*global_index+1;
	VALUE result = numerator/denominator;
	input[global_index]=result;

	if(global_index==0){
		int boundary=get_global_size(0);
		ndrange_t ndrange = ndrange_1D(roundUp(boundary,LOCALWORKSIZE), LOCALWORKSIZE);
		uint localMemSize = sizeof(VALUE)*LOCALWORKSIZE;
		void (^my_block_one)(local void*)=^(local void* local_memory){pi_step_two(input,output,(local VALUE*) local_memory,boundary,0);};
		enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,ndrange,my_block_one,localMemSize);
	}
	
}

