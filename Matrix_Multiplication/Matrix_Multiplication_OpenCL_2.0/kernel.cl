/*
Author: Yakup Ipek
Matrix Multiplication Kernel for OpenCL 2.0
*/

__kernel void matrix_mul(__global VALUE *matrix_A, __global VALUE *matrix_B, __global VALUE *matrix_C)
{
	// two dim parallel
	int row = get_global_id(0);
	int col = get_global_id(1);
	
	//printf("Col: %d\t Row: %d\n",col, row);
	VALUE sum = 0;
	for(int k=0; k<M; ++k) {
		sum += matrix_A[M*row+k] * matrix_B[M*col+k];
	}
	matrix_C[K*row+col] = sum;

	/*
	if(row == 0 && col == 0){
		printf("Child_Get_global_size(0): %d\n",get_global_size(0));
		printf("Child_Get_global_size(1): %d\n",get_global_size(1));
	}
	*/
}

__kernel void matrix_transpose_and_multiplicate(__global VALUE* matrix_A,__global VALUE* transpose_matrix_B,__global VALUE* matrix_C,__global VALUE* temp_matrix){

	
	int row = get_global_id(0);
	int col = get_global_id(1);

	temp_matrix[M*col+row] = transpose_matrix_B[K*row+col]; 
	
	if(row == 0 && col == 0){
	
		//printf("Parent_Get_global_size(0): %d\n",get_global_size(0));
		//printf("Parent_Get_global_size(1): %d\n",get_global_size(1));

		const size_t global_work_size[2]={N,K};
		ndrange_t ndrange = ndrange_2D(global_work_size);

		void(^my_block)(void) = ^{matrix_mul(matrix_A,temp_matrix,matrix_C);};

		enqueue_kernel(get_default_queue(),CLK_ENQUEUE_FLAGS_WAIT_KERNEL,ndrange,my_block);
	}
	
}


