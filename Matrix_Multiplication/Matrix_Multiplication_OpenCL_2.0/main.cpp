/*
Author: Yakup Ipek
Matrix Multiplication in Parallel for OpenCL 2.0
*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include "MyTime.h"
#include "matrixLib.h"
#include "myOpenCL_Lib.h"

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

#ifndef N
#define N 200
#endif

#define M N
#define K N

#define VALUE float

#define KERNEL_FILE_NAME "./kernel.cl"

int main(void) {

	cl_context context;
	cl_command_queue command_queue;
	cl_command_queue device_queue;
	//cl_device_id device_id = clInitWithDeviceQueue(&context, &command_queue,&device_queue);
	cl_device_id device_id = clInitWithDeviceQueueByParam(&context, &command_queue,&device_queue, CL_PLATFORM_ID, CL_DEVICE_ID);
	cl_program clProgram;
	cl_kernel clKernel;
	cl_int status;
	cl_ulong total_time=0;
	cl_event event;
	MyTimeData time;

	char option[128];
	sprintf(option, "-DM=%i -DN=%i -DK=%i -DVALUE=%s", M, N, K, EXPAND_QUOTE(VALUE));
	myCLBuildProgram(context, &clProgram, device_id, KERNEL_FILE_NAME, option);
	clKernel = clCreateKernel(clProgram, "matrix_transpose_and_multiplicate", &status);
	CLU_ERRCHECK(status, "Error creating kernel for matrix multiplication");
	
	VALUE* A = (VALUE*)clSVMAlloc(context, CL_MEM_READ_ONLY, N*M*sizeof(VALUE), 0);
	VALUE* B = (VALUE*)clSVMAlloc(context, CL_MEM_READ_ONLY, M*K*sizeof(VALUE), 0);
	VALUE* B_T = (VALUE*)clSVMAlloc(context, CL_MEM_READ_WRITE, M*K*sizeof(VALUE), 0);
	VALUE* C = (VALUE*)clSVMAlloc(context, CL_MEM_READ_WRITE, N*K*sizeof(VALUE), 0);
	VALUE* T = (VALUE*)malloc(sizeof(VALUE)*N*K);
	VALUE* TMP_A = (VALUE*)malloc(sizeof(VALUE)*N*M);
	VALUE* TMP_B = (VALUE*)malloc(sizeof(VALUE)*M*K);
	fillMatrixRandomly(TMP_A, N*M, EXPAND_QUOTE(VALUE));
	fillMatrixRandomly(TMP_B, M*K, EXPAND_QUOTE(VALUE));
	
	start_timer(&time);
	memcpy(A,TMP_A,sizeof(VALUE)*N*M);
	memcpy(B,TMP_B,sizeof(VALUE)*M*K);
	end_timer(&time);
	total_time += time.Result;

	/*Printing Input Matrices*/
	//printMatrix(A, N*M, EXPAND_QUOTE(VALUE), M, "Matrix A");
	//printMatrix(B, M*K, EXPAND_QUOTE(VALUE), K, "Matrix B");

	/*--------------------Set Arguments---------------------*/
	clSetKernelArgSVMPointer(clKernel, 0, A);
	clSetKernelArgSVMPointer(clKernel, 1, B);
	clSetKernelArgSVMPointer(clKernel, 2, C);
	clSetKernelArgSVMPointer(clKernel, 3, B_T);


	/*-----------------------Enqueue Kernel----------------*/
	size_t  globalWorkSize[2] = { M,K };

	CLU_ERRCHECK(clEnqueueNDRangeKernel(command_queue, clKernel, 2, NULL, globalWorkSize, NULL, 0, NULL, &event), "Failed to enqueue 2D kernel");
	total_time += retrieveTotalExecutionTimeFromEvent(&event);
	
	printf("\nTotal time in nanoseconds = %llu\n\n", total_time);

	matrixMult(A, N, M, B, M, K, T, EXPAND_QUOTE(VALUE));
	verifyTwoMatrices(C, T, N*K, EXPAND_QUOTE(VALUE));
	/*Printing output matrices*/
	//printMatrix(C,N*K, EXPAND_QUOTE(VALUE),K,"Result(Parallel)");
	//printMatrix(T, N*K, EXPAND_QUOTE(VALUE), K, "Result(Sequential)");

	

	//free host resources
	free(T);
	free(TMP_A);
	free(TMP_B);
	clSVMFree(context, A);
	clSVMFree(context, B);
	clSVMFree(context, B_T);
	clSVMFree(context, C);
	clReleaseContext(context);
	clReleaseCommandQueue(command_queue);
	clReleaseCommandQueue(device_queue);
	clReleaseProgram(clProgram);
	clReleaseKernel(clKernel);
}
