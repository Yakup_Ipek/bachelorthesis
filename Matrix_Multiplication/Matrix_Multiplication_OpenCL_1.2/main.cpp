/*
Author: Yakup Ipek
Matrix Multiplication in Parallel for OpenCL 1.X
*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <assert.h>

#include "matrixLib.h"
#include "myOpenCL_Lib.h"
#include "MyTime.h"

#ifndef CL_PLATFORM_ID
	#define CL_PLATFORM_ID 2
#endif

#ifndef CL_DEVICE_ID
	#define CL_DEVICE_ID 0
#endif

#ifndef N
	#define N 200
#endif

#define M N
#define K N

#define VALUE float

#define KERNEL_FILE_NAME "./kernel.cl"

VALUE A[N][M];
VALUE B[M][K];
VALUE C[N][K];

int main(void) {
	
	cl_context context;
	cl_command_queue command_queue;
	//cl_device_id device_id = clInit(&context,&command_queue);
	cl_device_id device_id = clInitByParam(&context,&command_queue, CL_PLATFORM_ID, CL_DEVICE_ID);
	cl_program clProgram;
	cl_kernel clKernel, clKernel_2;
	cl_int status;
	cl_event event;
	cl_ulong total_time=0;
	
	/*---------------Initialize CL_DEVICE---------------*/
	char option[128];
	sprintf(option,"-DM=%i -DN=%i -DK=%i -DVALUE=%s",M,N,K,EXPAND_QUOTE(VALUE));
	myCLBuildProgram(context, &clProgram, device_id, KERNEL_FILE_NAME, option);
	clKernel = clCreateKernel(clProgram, "matrix_mul", &status);
	CLU_ERRCHECK(status, "Error creating kernel for matrix multiplication");
	clKernel_2 = clCreateKernel(clProgram, "matrix_transpose", &status);
	CLU_ERRCHECK(status, "Error creating kernel for matrix transpose");


	fillMatrixRandomly(A, N*M, EXPAND_QUOTE(VALUE));
	fillMatrixRandomly(B, M*K, EXPAND_QUOTE(VALUE));

	/*Printing Input Matrices*/
//	printMatrix(A, N*M, EXPAND_QUOTE(VALUE), M, "Matrix A");
//	printMatrix(B, M*K, EXPAND_QUOTE(VALUE), K, "Matrix B");

	/*--------------Create Memory Objects----------------*/
	cl_mem mem_A = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(VALUE)*N*M,NULL, &status);
	CLU_ERRCHECK(status, "Error creating Buffer_A");
	cl_mem mem_B = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(VALUE)*M*K,NULL, &status);
	CLU_ERRCHECK(status, "Error creating Buffer_B");
	cl_mem mem_temp = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(VALUE)*M*K, NULL, &status);
	CLU_ERRCHECK(status, "Error creating Buffer_temp");
	cl_mem mem_C = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(VALUE)*N*K, NULL, &status);
	CLU_ERRCHECK(status, "Error creating Buffer_C");

	VALUE* T = (VALUE*)malloc(sizeof(VALUE)*N*K);
	
	CLU_ERRCHECK(clEnqueueWriteBuffer(command_queue, mem_A, CL_TRUE, 0, sizeof(VALUE)*N*M, A, 0, NULL, &event),"Failed writing buffer: A");
	total_time += retrieveTotalExecutionTimeFromEvent(&event);
	
	CLU_ERRCHECK(clEnqueueWriteBuffer(command_queue, mem_B, CL_TRUE, 0, sizeof(VALUE)*M*K, B, 0, NULL, &event),"Failed writing buffer: B");
	total_time += retrieveTotalExecutionTimeFromEvent(&event);
	
	/*--------------------Set Arguments---------------------*/
	clSetKernelArg(clKernel_2, 0, sizeof(cl_mem), (void*)&mem_B);
	clSetKernelArg(clKernel_2, 1, sizeof(cl_mem), (void*)&mem_temp);

	clSetKernelArg(clKernel, 0, sizeof(cl_mem), (void*)&mem_A);
	clSetKernelArg(clKernel, 1, sizeof(cl_mem), (void*)&mem_temp);
	clSetKernelArg(clKernel, 2, sizeof(cl_mem), (void*)&mem_C);

	/*-----------------------Enqueue First Kernel----------------*/
	
	size_t  globalWorkSize[2] = { M,K };
	
	CLU_ERRCHECK(clEnqueueNDRangeKernel(command_queue, clKernel_2, 2, NULL, globalWorkSize, NULL, 0, NULL, &event), "Failed to enqueue 2D kernel");
	total_time += retrieveTotalExecutionTimeFromEvent(&event);
	
	/*-----------------------Enqueue Second Kernel----------------*/
	globalWorkSize[0] = N;
	globalWorkSize[1] = K;
	
	CLU_ERRCHECK(clEnqueueNDRangeKernel(command_queue, clKernel, 2, NULL, globalWorkSize, NULL, 0, NULL, &event), "Failed to enqueue 2D kernel");
	total_time += retrieveTotalExecutionTimeFromEvent(&event);
	
	CLU_ERRCHECK(clEnqueueReadBuffer(command_queue, mem_C, CL_TRUE, 0, sizeof(VALUE)*N*K, C, 0, NULL, &event),"Failed reading buffer");
	total_time += retrieveTotalExecutionTimeFromEvent(&event);
	
	printf("\nTotal time in nanoseconds = %llu\n\n", total_time);

	matrixMult(A, N, M, B, M, K, T, EXPAND_QUOTE(VALUE));
	verifyTwoMatrices(C, T, N*K, EXPAND_QUOTE(VALUE));

	/*Printing output matrices*/
	//printMatrix(C,N*K, EXPAND_QUOTE(VALUE),K,"Result(Parallel)");
	//printMatrix(T, N*K, EXPAND_QUOTE(VALUE), K, "Result(Sequential)");

	//free host resources
	free(T);
	clReleaseMemObject(mem_A);
	clReleaseMemObject(mem_B);
	clReleaseMemObject(mem_C);
	clReleaseMemObject(mem_temp);
	clReleaseContext(context);
	clReleaseCommandQueue(command_queue);
	clReleaseProgram(clProgram);
	clReleaseKernel(clKernel);
}
