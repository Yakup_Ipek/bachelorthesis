/*
Author: Yakup Ipek
Matrix Multiplication Kernel for OpenCL 1.X
*/

__kernel void matrix_mul(__global VALUE *matrix_A, __global VALUE *matrix_B, __global VALUE *matrix_C)
{
	// two dim parallel
	int row = get_global_id(0);
	int col = get_global_id(1);
	//printf("Col: %d\t Row: %d\n",col, row);
	
	VALUE sum = 0;
	for(int k=0; k<M; ++k) {
		sum += matrix_A[M*row+k] * matrix_B[M*col+k];
	}
	matrix_C[K*row+col] = sum;

	/*
	if(col ==0 && row ==0){
		printf("Get_global_size(0): %d\n",get_global_size(0));
		printf("Get_global_size(1): %d\n",get_global_size(1));
	}
	*/
}

__kernel void matrix_transpose(__global VALUE* transpose_matrix_B, __global VALUE* temp_matrix){

	int row = get_global_id(0);
	int col = get_global_id(1);
	
	temp_matrix[M*col+row] = transpose_matrix_B[K*row+col]; 
}