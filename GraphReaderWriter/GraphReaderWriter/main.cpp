/*
Author: Yakup Ipek
Generating some graphs for dijkstra
*/
#include <stdio.h>
#include <stdlib.h>
#include "Graph.h"
#include "GraphReader.h"
#include "GraphWriter.h"

#define NODES 200

int main(void) {
	Graph graph;

	generate_random_directed_graph(&graph, NODES, 100, 100, 1000);
	write_graph_into_file(graph, "[NORMAL]Nodes200_Neighbours100.txt");
	//read_graph_into_file(&graph, "N1K_Neighbour0.5K.txt");

	//write_weighted_graph_into_file(graph, "test.txt");
	//read_weighted_graph_into_file(&graph, "test.txt");
	
	clean_graph(&graph);
	return EXIT_SUCCESS;
}